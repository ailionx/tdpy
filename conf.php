<?php
error_reporting(E_ALL & ~E_NOTICE ^ E_DEPRECATED);
define ('POST_PRO', "frank");
define ('root_dir',$_SERVER['DOCUMENT_ROOT']);
define ('project_dir',root_dir."/project");
define ('frame_dir', project_dir."/frame");
define ('proc_dir', project_dir."/proc");
define ('table_dir', project_dir."/table");
define ('js_dir', project_dir."/js");


$statusMappingArr = array(
        '个人待上报项目'=>'1',
        '待企业审核项目'=>'2',
        '待上报项目'=>'3',
        '待修改项目'=>'4',
        '待审核项目'=>'5',
        '初审通过项目'=>'5',
        '已审批项目'=>'6',
        '已立项项目'=>'7',
        '落选项目'=>'8',
        '待提交项目'=>'3',
        '待提交合同'=>'3',
        '待提交难题'=>'3',
        '待评审项目'=>'5',
        '已提交难题'=>'9',
        '已提交合同'=>'9',
        '通过初审项目'=>'6',
        '未通过初审项目'=>'8',
        '已解决难题'=>'6',
        '待初审项目'=>'9'
    );



//echo root_dir;
//echo $_SERVER['DOCUMENT_ROOT'];
//var_dump(dirname(_FILE_));
//var_dump(file_exists("/wwwroot/tdpy/project/proc/myphp_class.php"));
//var_dump(file_exists(proc_dir."/collector_class.php"));
//var_dump(file_exists("/myphp_class.php"));

include '../proc/mysecret.php';
include "../proc/myphp_class.php";
include "../proc/collector_class.php";
include "../proc/changeProfile.php";
include '../proc/accountManager.php';
include '../proc/Member.php';
include '../dataobjects/DataObject.php';
include '../dataobjects/CompanyInfo.php';

//foreach(glob('../dataobjects/*.php') as $filename) {
//        var_dump($filename);
//    include $filename;
//
//}


function configMenu ($table,$pems,$statusMappingArr) {
	switch ($table){
		case 'project_application':
			switch ($pems){
				//不同的用户权限有不一样的菜单
				case 1:
					$status_list = array('待审核项目','待修改项目','已审批项目','已立项项目','落选项目');
					break;
				case 2:
					$status_list = array('待企业审核项目', '待上报项目', '待初审项目', '待修改项目','待审核项目','已审批项目','已立项项目','落选项目');
					break;
				case 3:
					$status_list = array('个人待上报项目','待企业审核项目','待修改项目','待审核项目','已审批项目','已立项项目','落选项目');
					break;
				case 11:
					$status_list = array('待初审项目','待修改项目');
					break;
				case 12:
					$status_list = array('待初审项目','待修改项目', '初审通过项目');
					break;
				case 13:
					$status_list = array('待初审项目','待修改项目', '初审通过项目');
					break;
				default:
					echo "<script>alert('非法用户')</script>";
					exit();
			}
			break;
		case 'project_contract':
		case 'project_task':
		case 'project_inprogress':
			switch ($pems){
				//管理员用户和普通用户暂时是一样的，先分开写以防以后有不一样的需求
				case 1:
					$status_list = array('待审核项目','待修改项目','已审批项目');
					break;
				case 2:
					$status_list = array('待上报项目','待修改项目','待审核项目','已审批项目');
					break;
				case 3:
					$status_list = array('个人待上报项目','待企业审核项目','待修改项目','待审核项目','已审批项目');
					break;
				case 13:
					$status_list = array('待审核项目','待修改项目','已审批项目');
					break;
				default:
					echo "<script>alert('非法用户')</script>";
					exit();
			}
			break;
		case 'project_conclusion':
			switch ($pems){
				//管理员用户和普通用户暂时是一样的，先分开写以防以后有不一样的需求
				case 1:
					$status_list = array('待审核项目','待修改项目','已审批项目');
					break;
				case 2:
					$status_list = array('待上报项目','待修改项目','待审核项目','已审批项目');
					break;
				case 3:
					$status_list = array('个人待上报项目','待企业审核项目','待修改项目','待审核项目','已审批项目');
					break;
				case 13:
					$status_list = array('待审核项目','待修改项目','已审批项目');
					break;
				default:
					echo "<script>alert('非法用户')</script>";
					exit();
			}
			break;
		case 'tech_awards':
			switch ($pems){
				//管理员用户和普通用户暂时是一样的，先分开写以防以后有不一样的需求
				case 1:
					$status_list = array('待评审项目','通过初审项目','未通过初审项目');
					break;
				case 2:
					$status_list = array('待提交项目','待评审项目','待修改项目','通过初审项目','未通过初审项目');
					break;
				case 3:
					$status_list = array('待提交项目','待评审项目','待修改项目','通过初审项目','未通过初审项目');
					break;
				case 13:
					$status_list = array('待评审项目','通过初审项目','未通过初审项目');
					break;
				default:
					echo "<script>alert('非法用户')</script>";
					exit();
			}
			break;
		case 'tech_challenge':
			switch ($pems){
				//管理员用户和普通用户暂时是一样的，先分开写以防以后有不一样的需求
				case 1:
					$status_list = array('已提交难题','已解决难题');
					break;
				case 2:
					$status_list = array('待提交难题','已提交难题','已解决难题');
					break;
				case 3:
					$status_list = array('待提交难题','已提交难题','已解决难题');
					break;
				case 12:
					$status_list = array('已提交难题','已解决难题');
					break;
				default:
					echo "<script>alert('非法用户')</script>";
					exit();
			}
			break;
		case 'tech_contract':
			switch ($pems){
				//管理员用户和普通用户暂时是一样的，先分开写以防以后有不一样的需求
				case 1:
					$status_list = array('已提交合同');
					break;
				case 2:
					$status_list = array('待提交合同','已提交合同');
					break;
				case 3:
					$status_list = array('待提交合同','已提交合同');
					break;
				case 12:
					$status_list = array('已提交合同');
					break;
				default:
					echo "<script>alert('非法用户')</script>";
					exit();
			}
			break;
		default:
			echo "<script>alert('未知执行目标')</script>";
			exit ();
		}
	foreach ($status_list as $statusName) {
		foreach ($statusMappingArr as $statusNameMapping=>$statusMapping) {
			if ($statusName === $statusNameMapping) {
				$statusListArr[$statusMapping] = $statusName;
			}
		}
	}

	return $statusListArr;
	
}

//用于在表单文件中控制访问权限，以防有人意外由id号和文件名进入表单页面

function permissionBlocker ($type,$status) {
	$user = $_SESSION[user];
	$pems = $_SESSION[pems];
	$company_name = $_SESSION[company_name];
	$bloker = '';
	switch ($pems) {
		case 1:
			switch ($type) {
				case 'apply':
					$pemsBloker = '1';	//$pemsBloker 表示被阻挡
					break;
				case 'edit':
					$pemsBloker = '1';
					$passStatus = array(); //$psssStatus 表示可以放行的项目状态
					break;
				case 'show':
					$pemsBloker = '0';
					$passStatus = array('3','4','5','6','7','8');
					break;				
			}
			break;
		case 2:
			switch ($type) {
				case 'apply':
					$pemsBloker = '0';	//$pemsBloker 表示被阻挡
					break;
				case 'edit':
					$pemsBloker = '$value[company_name]!==$company_name';
					$passStatus = array('2','3','4');
					break;
				case 'show':
					$pemsBloker = '$value[company_name]!==$company_name';
					$passStatus = array('2','3','4','5','6','7','8');
					break;				
			}
			break;
		case 3:
			switch ($type) {
				case 'apply':
					$pemsBloker = '0';	//$pemsBloker 表示被阻挡
					break;
				case 'edit':
					$pemsBloker = '$value[user]!==$user';
					$passStatus = array('1','2','4');
					break;
				case 'show':
					$pemsBloker = '$value[user]!==$user';
					$passStatus = array('1','3','4','5','6','7','8');
					break;				
			}
			break;	
		default:
			$bloker = '1';
	}
	
	if ($_GET[status]) {
		if (in_array($status,$passStatus)) {
			$statusBlocker = 0; 
		} else {
			$statusBlocker = 1;
		}
	} else if ($type === 'apply') {
		$statusBlocker = 0;
	} else {
		$statusBlocker = 1;
	}
	
	$bloker = '$pemsBloker'.' || '.'$statusBlocker';
	
	if ($blocker) {
		echo "<script>alert('对不起，你没有权限查看该页面')</script>";
		exit();
	}	
}
 
?>
