<?php
session_start();
include ("../../conf.php");
$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">

body {
	width:80%;
	margin:20px auto;
}

li {
	list-style:none;
	display:block;
	width:200px;
	float:left;
}


div.reserch {
	margin:90px 0px 60px 0px;
}

div.selector {
	font-size:0.75em;
	border: 1px solid #999;
	padding:10px 10px 10px 20px;
}

table {
	border-collapse:collapse;
}

td,th {
	text-align:center;
	padding:0px;
	margin:0;
}

.center_content {
	margin-top:40px;
	line-height:30px;
}

</style>
<title>企业信息汇总</title>
</head>

<body>

<div class="selector">
<?php 

	$order_name_array = array('company_name'=>'企业名称',
							'hightech_revenue'=>'高新技术产品、新产品等销售收入',
							'tech_member'=>'科技人员数',
							'dengjichengguo'=>'登记成果',
							'invent_patent'=>'发明专利数（最近一年）',
							'invent_patent_all'=>'发明专利数（累计）',
							'practical_patent'=>'实用新型专利（最近一年）',
							'practical_patent_all'=>'实用新型专利（累计）',
							'facade_patent'=>'外观设计专利数（最近一年）',
							'facade_patent_all'=>'外观设计专利数（累计）',
							'firmly_new'=>'认定的新产品（最近一年）',
							'firmly_new_all'=>'认定的新产品（累计）');

?>
<form method="get" action="./company_info_summarize.php" name="order_form">
请选择：按&nbsp;<select name="order_item" >
<?php
	foreach ($order_name_array as $order_key=>$order_name) {
		if ($_GET['order_item'] == $order_key) {
			echo "<option name=array_company_class[$order_key] value=$order_key selected>$order_name</option>";
		} else {
			echo "<option name=array_company_class[$order_key] value=$order_key>$order_name</option>";
		}
	}


?>
	</select>
    <select name="order_meth">
<?php 
	$order_meth_array = array('asc'=>'升序',
							'desc'=>'降序');		
	foreach ($order_meth_array as $order_key=>$order_name) {
		if ($_GET['order_meth'] == $order_key) {
			echo "<option value=$order_key selected>$order_name</option>";
		} else {
			echo "<option value=$order_key>$order_name</option>";
		}
	}							
?>
    </select>
    &nbsp;排序
<br />
<br />
<div class="company">企业类别筛选:
<ul class="check">
<?php
	if ($_GET[array_company_class][$order_key] == $order_name) {
		$order_key = $order_key." selected";
	} 
	$array_company_class = array('hightech_country'=>'高新技术企业：国家',
								'hightech_city'=>'高新技术企业：市',
								'shengchuangxinshifan'=>'省级创新型示范企业',
								'shengchuangxinxingshidian'=>'省级创新型试点企业',
								'shengkejixingzhongxiao'=>'省级科技型中小企业',
								'shengzhuanlishifan'=>'省级专利示范企业',
								'shengnongyekeji'=>'省级农业科技型企业',
								'shizhuanlishifan'=>'市级专利示范企业',
								'shichuangxinxing'=>'市级创新型企业',
								'xiankejixing'=>'县级科技型企业',
								'class_other'=>'其他',							
								);
	foreach ($array_company_class as $company_class_key=>$company_class) {
		if ($_GET[array_company_class][$company_class_key]==$company_class_key) {
			echo "<li><input type='checkbox' name=array_company_class[$company_class_key] value=$company_class_key checked>$company_class</li>";
		} else {
			echo "<li><input type='checkbox' name=array_company_class[$company_class_key] value=$company_class_key>$company_class</li>";
		}
	}
?>
</ul>
</div>

<div class="reserch">企业研发中心类别筛选:
<ul class="check">
<?php
	$array_reserch_class = array('shengjiqiyeyanjiuyuan'=>'省级企业研究院',
								'shengjigaoxinjishuqiyefazhanzhongxin'=>'省级高新技术企业研发中心',
								'shengjinongyeqiyekejiyanfazhongxin'=>'省级农业企业科技研发中心',
								'shengjiquyukejichuangxinfuwuzhongxin'=>'省级区域科技创新服务中心',
								'shengjidayuanmingxiaogongjianchuangxinzaiti'=>'省级大院名校共建创新载体',
								'shijigongchengjishuyanjiukaifazhongxin'=>'市级工程技术研究开发中心',
								'qiyeshuyanfajigou'=>'企业所属研发机构',
								'rd_other'=>'其他',							
								);
							
	foreach ($array_reserch_class as $reserch_class_key=>$reserch_class) {
		if($_GET[array_reserch_class][$reserch_class_key] == $reserch_class_key) {
			echo "<li><input type='checkbox' name=array_reserch_class[$reserch_class_key] value=$reserch_class_key checked>$reserch_class</li>";
		} else {
			echo "<li><input type='checkbox' name=array_reserch_class[$reserch_class_key] value=$reserch_class_key >$reserch_class</li>";
		}
	}
?>
</ul>
</div>
<input type="submit" value="开始汇总"  />
</form>
</div>
<?php
	if($_GET){
		$order_item = $_GET[order_item];
		$order_meth = $_GET[order_meth];
		$array_company_class = $_GET[array_company_class];
		$array_reserch_class = $_GET[array_reserch_class];
		if($_GET[page]) {
			$page = $_GET[page];	
		} else {
			$page = 1;
		}
		$perPage = 15;
		$limit = "limit $page,$perPage";
		$show_array = array('company_name'=>'企业名称',$order_item=>$order_name_array[$order_item],'company_address'=>'企业地址','corporate_rep'=>'法人代表');
		$show_list = implode(',',array_keys($show_array));
		
		
		if(count($array_company_class)) {
			$list_company_class = implode("%' and array_company_class like '%",$array_company_class);
			$company_class_condition = "array_company_class like '%".$list_company_class."%'";
		} else {
			$company_class_condition = "";
		}
		
		if(count($array_reserch_class)) {
			$list_reserch_class = implode("%' and array_reserch_class like '%",$array_reserch_class);
			$reserch_class_condition = "array_reserch_class like '%".$list_reserch_class."%'";
		} else {
			$reserch_class_condition = "";
		}
		if ($company_class_condition && $reserch_class_condition) {
			$condition = $company_class_condition.' and '.$reserch_class_condition;
		} else {
			$condition = $company_class_condition.$reserch_class_condition;
		}
		if($condition) {
			$query="select $show_list from company_info where $condition ORDER BY $order_item $order_meth";
		} else {
			$query="select $show_list from company_info where 1 ORDER BY $order_item $order_meth";
		}
//		echo $condition;
//		echo $query;
		$result = mysql_query($query);
		$result_num = mysql_num_rows($result);
		$pages=intval($result_num/$perPage)+1;
		$lo_num = ($page-1)*$perPage+1;
		if ($page<$pages) {
			$hi_num=$page*$perPage;
			$showPage = $perPage;
		} else {
			$hi_num = $result_num%$perPage+$lo_num-1;
			$showPage = $result_num%$perPage;
		}
		foreach ($_GET as $get_key=>$get_val) {
			if (is_array($get_val)){
				foreach ($get_val as $each_val) {
					$get_arr[] = $get_key."%5B".$each_val."%5D=".$each_val;
				}
			} else if ($get_key == 'page'){
				continue;
			} else {
				$get_arr[] = "$get_key=$get_val";
			}
		}
		if ($result_num === 0) {
			$lo_num = 0;
		}
		$get_string = implode('&',$get_arr);
		echo "<center class='center_content'>";
		echo "总共".$result_num."条结果,当前显示第".$lo_num."-".$hi_num."条&nbsp;&nbsp;";
		$url = $_SERVER["REQUEST_URI"];
		$url = parse_url($url);
		$url = $url[path];
		if ($page>1) {
			echo "<a href=$url?page=".($page-1)."&".$get_string."> 上一页</a> |";
		}
		if($page<$pages){
			 echo "| <a href=$url?page=".($page+1)."&".$get_string.">下一页</a>";
		}
		echo "<table class='content' border='1' width=90%>";
		echo "<thead>";
		foreach($show_array as $show_item=>$show_name) {
			echo "<th>$show_name</th>";
		}
		echo "</thead>";
		$query_sep = $query." limit ".($page-1)*$perPage.",$showPage";
		$result_sep = mysql_query($query_sep);
		while ($row = mysql_fetch_array($result_sep)) {
			echo "<tr>";
			foreach(array_keys($show_array) as $show_item) {
				if($show_item=='company_name'){
					echo "<td><a href=../show/show_company_info.php?company_name=$row[company_name] target='_blank'>$row[$show_item]</a></td>";
				} else {
					echo "<td>$row[$show_item]</td>";
				}
			}
			echo "</tr>";
		}
		echo "</table>";
		

		echo "</center>";		
	}

	

?>

</body>
</html>
