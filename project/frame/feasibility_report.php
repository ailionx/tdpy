<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>可行性报告及经费概算编写提纲</title>
</head>

<body>
<pre>
龙游县重点科技项目可行性报告及经费概算编写提纲
一、项目可行性报告
（一）立项的背景和意义。
（二）国内外研究现状和发展趋势。
（三）项目主要研究开发内容、技术关键及主要创新点。
（四）项目预期目标（主要技术经济指标、社会效益、技术应用和产业化前景以及获取自主知识产权的情况）。
（五）项目实施方案、技术路线、组织方式与课题分解。
（六）计划进度安排。
（七）现有工作基础和条件。
二、经费概算
凡申请财政分期补助、事先立项事后补助的项目，均应当编制科研项目经费概算。
经费概算包括两部分：一是经费概算列表，二是经费概算说明。经费概算列表的表式见该提纲附表“县级科技计划项目经费概算表”。经费概算说明包括： 对承担单位和相关部门承诺提供的支撑条件进行说明；对各科目支出的主要用途、与项目研究的相关性、概算方法、概算依据进行分析说明；对其他来源经费进行说明。
项目可行性报告及经费概算编写应当回避项目申报单位和项目负责人、成员相关的信息，否则作无效申报处置。
</pre>
请下载以下word文档，完成项目可行性报告及经费概算编写提纲，并将完成的文档上传，<br />
<a href="../file/龙游县重点科技项目可行性报告及经费概算编写提纲.DOC" style="font-size:12px;line-height:30px">龙游县重点科技项目可行性报告及经费概算编写提纲.DOC</a><br />
<br />

上传已完成的项目可行性报告及经费概算编写提纲：<br />
<form action="../proc/upload_file.php" method="post">
<input type="file" name="feasibility_report" id='upload' enctype="multipart/form-data" />
<input type="submit" name="submit" value="上传" />
</body>
</html>