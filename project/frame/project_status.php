﻿<?php
session_start();
include ("../../conf.php");
$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
$user = $ident[user];
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<style type="text/css">
@media screen {
	.container {
		position:relative;
		top:15px;
	}
	
}

td,th {
	text-align:center;
	padding:8px;
	font-size:12px;
}

table {
	border-collapse:collapse;
	border:1px;
	width: 750px;
}

</style>
</head>


<div class="container">


<?php
class genStatusTable {
	public $table; // 表格所要显示的类型，比如项目/合同/科技进步奖/技术难题 等
	public $status; //项目状态
	public $pems; //当前用户的权限状态，直接由用户类型决定
	public $tableHeaderArr; //中文名与mysql字段名（英文）对应的关联数组，决定表格所要生成的内容
	public $query; //到mysql中查询内容的语句
	public $actionArr;
//	public $valueTrans_list; //需要转换中英文名字的字段值
	public $nameExchangeArrList; //转换中英文名字的对照表
//	public $company_info_loc; //公司名字所在表格的列位置
//	public $project_class_loc; //项目类型所在表格的列位置
	
	function __construct ($table,$status,$pems,$tableHeaderArr,$query,$actionArr,$valueTrans_list,$nameExchangeArrList) {
		$this->table = $table;
		$this->status = $status;
		$this->pems = $pems;
		$this->tableHeaderArr = $tableHeaderArr;
		$this->query = $query;
		$this->actionArr = $actionArr;
//		$this->valueTrans_list = $valueTrans_list;
		$this->nameExchangeArrList = $nameExchangeArrList;
//		$this->company_info_loc = $company_info_loc;
//		$this->project_class_loc = $project_class_loc;
//		var_dump($actionArr);
		if 	(is_array($actionArr)) {
			$this->table_header_string = '<thead><tr><th>'.implode('</th><th>',$this->tableHeaderArr).'</th><th>申请单位</th><th colspan='.count($actionArr).'>可执行操作</th></tr></thead>';
		} else {
			$this->table_header_string = '<thead><tr><th>'.implode('</th><th>',$this->tableHeaderArr).'</th><th>申请单位</th></tr></thead>';
		}
	}	

	
	//生成显示状态的内容表格，此为页面的主题内容
	function createTable () {
//		echo $this->query;
		$result = mysql_query($this->query);
		$tableMappingArr = array('project_application'=>'项目申请','project_contract'=>'合同书','project_task'=>'任务书','project_inprogress'=>'中期检查','project_conclusion'=>'结题','tech_awards'=>'科技进步奖','tech_challenge'=>'技术难题','tech_contract'=>'技术难题合同');
		foreach ($tableMappingArr as $perTable=>$perTitle) {
			if ($this->table == $perTable) {
				$tableTitle = $perTitle;
			}
		}
		echo "<center><span style='line-height:40px;font-weight:bold;font-size:23px'>$tableTitle".'状态</span><br><br>';
		if (mysql_num_rows($result)==0) {
			echo "没有可显示的项目";
		} else {
			echo "<table border=1  width=90%>";
			echo $this->table_header_string;
			while($value_array = mysql_fetch_array($result)) {
//				print_r($value_array);echo '<br>';	
				echo "<tr>";
				$project_id = $value_array[project_id];
				for ($j=2;$j<count($value_array)/2;$j++) {
					$value_array[$j] = $value_array[$j];
					// 处理表单中包含的一些下拉单选，因为mysql中存的是英文，显示的时候要转换为中文
						if ($j == $this->getNumberKey('project_class',$value_array) && $this->nameExchangeArrList){
							$value_array[$j] = $this->nameTransform($value_array[$j],$this->nameExchangeArrList);
						}
					$arrayKeys = array_keys($value_array);
					if ($j == $this->getNumberKey('project_name',$value_array)) {
						echo "<td>"."<a href='../show/show_$this->table.php?project_id=$project_id' target='_blank'>".$value_array[$j]."</a>"."</td>";
					} else if ($j == $this->getNumberKey('company_name',$value_array) && $_SESSION[pems]==1){
						echo "<td>"."<a href=../show/show_company_info.php?company_name=$value_array[$j] target='_blank'>".$value_array[$j]."</a>"."</td>";
					} else {
						echo "<td>".$value_array[$j]."</td>";
					}
				}
				$queryApplicant = "select account_type,company_name,person_name from account where user='$value_array[user]'";
				$resultApplicant = mysql_query($queryApplicant);
				$rowApplicant = mysql_fetch_array($resultApplicant);
				if ($rowApplicant[account_type]=='company') {
					echo "<td>$rowApplicant[company_name]</td>";
				} else if ($rowApplicant[account_type]=='personal') {
					echo "<td>$rowApplicant[person_name]</td>";
				} else {
					echo "<script>alert('未知的账户类型')</script>";
					exit();
				}
				echo $this->showActionBar($project_id);
				echo "</tr>";	
			}
			echo "</table>";
			echo "</center>";
		}		
	}
	
	// 处理表单中包含的一些下拉单选，因为mysql中存的是英文，显示的时候要转换为中文
	function nameTransform ($value,$value_arr) {
		if(in_array($value,array_keys($value_arr))) {
				$valueTrans = $value_arr[$value];
		} else {
			$valueTrans = $value_arr['default'];
		}
		return $valueTrans;
	}
	
	
	
	// 找到返回数组中键名是字段名的对应的数字键名 （我觉得应该有比这个函数更好的方法，先放着以后看要不要优化）
	//已优化
	function getNumberKey($string,$arr) {
		$numKey='';
		while (($key = key($arr))!== NULL) {
			if ($key === $string) {
				prev($arr);
				$numKey = key($arr);
				next($arr);
			}
			next($arr);
		}
		return $numKey;
	}

	
	//定义所有操作的操作内容
	function showActionBar ($project_id) {
		$contentString = '';
                if ($this->actionArr) {
                    foreach ($this->actionArr as $action) {
                             switch($action){
                                     case '编辑':
                                             $tdString = "<td><a href=../edit/edit_$this->table.php?project_id=$project_id target='content'>$action</a></td>";
                                             break;
                                     case '删除':
                                             $tdString = "<td><a href=../proc/change_project_status.php?table=$this->table&id=$project_id&status=delete&pre_status=$this->status onClick=\"return confirm('确定要将此项目删除吗？')\">$action</a></td>";
                                             break;
                                     case '上报':
                                             if ($this->table === 'tech_awards' || $this->table === 'tech_challenge' || $this->table === 'tech_contract') {
                                                     $action = '提交';
                                             } //科技进步奖与技术难题用“提交” 
                                             if 	($this->table === 'project_application') {
                                                     $tdString = "<td><a href=../proc/change_project_status.php?table=$this->table&id=$project_id&status=9&pre_status=$this->status onClick=\"return confirm('上报以后将不可修改，确定要上报吗？')\">$action</a></td>";
                                             } else {
                                                     $tdString = "<td><a href=../proc/change_project_status.php?table=$this->table&id=$project_id&status=5&pre_status=$this->status onClick=\"return confirm('上报以后将不可修改，确定要上报吗？')\">$action</a></td>";
                                             }
                                             break;
                                     case '上报企业':
                                             $tdString = "<td><a href=../proc/change_project_status.php?table=$this->table&id=$project_id&status=2&pre_status=$this->status onClick=\"return confirm('上报以后将不可修改，确定要上报吗？')\">$action</a></td>";
                                             break;
                                     case '退回修改':
                                             $tdString = "<td><a href=../proc/change_project_status.php?table=$this->table&id=$project_id&status=4&pre_status=$this->status onClick=\"return confirm('确定要将此项目退回修改吗？')\">$action</a></td>";
                                             break;
                                     case '审核落选':
                                             $tdString = "<td><a href=../proc/change_project_status.php?table=$this->table&id=$project_id&status=8&pre_status=$this->status onClick=\"return confirm('确定要此项目审核不通过吗，此操作将项目归入落选项目？')\">$action</a></td>";
                                             break;
                                     case '审核通过':
                                             if ($this->table === 'tech_challenge') {
                                                     $action = '难题解决';
                                             }
                                             $tdString = "<td><a href=../proc/change_project_status.php?table=$this->table&id=$project_id&status=6&pre_status=$this->status onClick=\"return confirm('确定此项目审核通过吗？')\">$action</a></td>";
                                             break;
                                     case '初审通过':
                                             if ($this->table === 'tech_challenge') {
                                                     $action = '难题解决';
                                             }
                                             $tdString = "<td><a href=../proc/change_project_status.php?table=$this->table&id=$project_id&status=5&pre_status=$this->status onClick=\"return confirm('确定此项目初审通过吗？')\">$action</a></td>";
                                             break;
                                     case '立项':
                                             $tdString = "<td><a href=../proc/change_project_status.php?table=$this->table&id=$project_id&status=7&pre_status=$this->status onClick=\"return confirm('立项后将进入填写合同状态，确定要立项吗？')\">$action</a></td>";
                                             break;
                                     case '签订合同';
                                             if ($this->table === 'tech_challenge') {
                                                     $queryContract = "select * from tech_contract where project_id='$project_id'";
                                                     $numContract = mysql_num_rows(mysql_query($queryContract));
                                                     if ($numContract === 0){
                                                     $tdString = "<td><a href=../table/tech_contract_table.php?project_id=$project_id target='content'>".$action."</a></td>";
                                                     } else if ($numContract > 0) {
                                                             $tdString = "<td>已填写合同<br>请进入合同状态查看</td>";
                                                     }
                                                     break;
                                             }
                                             $queryApplication = "select project_class from project_application where project_id='$project_id'";
                                             $rowApplication = mysql_fetch_array(mysql_query($queryApplication));
                                             if ($rowApplication[project_class] === 'general_project') {
                                                     $action = '任务书';
                                                     $contractType = 'task';
                                             } else if ($rowApplication[project_class] === 'key_project') {
                                                     $action = '合同书';
                                                     $contractType = 'contract';
                                             } else {
                                                     $action = NULL;
                                             }
                                             if ($action) {
                                                     $queryContract = "select * from project_$contractType where project_id='$project_id'";
                                                     $numContract = mysql_num_rows(mysql_query($queryContract));
                                                     if ($numContract === 0){
                                                             $tdString = "<td><a href=../table/project_".$contractType."_table.php?project_id=$project_id target='content'>填写".$action."</a></td>";
                                                     } else if ($numContract > 0) {
                                                             $tdString = "<td>已填写".$action."，<br>请进入".$action."状态查看</td>";
                                                     }
                                             } else {
                                                     $tdString = "<td>项目计划分类未知，<br>不能签订合同</td>";
                                             }
                                             break;
                                     case '填写中期检查报告':
                                                     $queryInprogress = "select * from project_inprogress where project_id='$project_id'";
                                                     $numInprogress = mysql_num_rows(mysql_query($queryInprogress));
                                                     if ($numInprogress === 0){
                                                             $tdString = "<td><a href=../table/project_inprogress_table.php?project_id=$project_id target='content'>".$action."</a></td>";
                                                     } else if ($numInprogress > 0) {
                                                             $tdString = "<td>已填写".$action."，<br>请进入".$action."状态查看</td>";
                                                     }
                                             break;
                                     case '填写结题报告':
                                                     $queryConclusion = "select * from project_conclusion where project_id='$project_id'";
                                                     echo $numConclusion = mysql_num_rows(mysql_query($queryConclusion));
                                                     if ($numConclusion === 0){
                                                             $tdString = "<td><a href=../table/project_conclusion_table.php?project_id=$project_id target='content'>".$action."</a></td>";
                                                     } else if ($numConclusion > 0) {
                                                             $tdString = "<td>已填写".$action."，<br>请进入".$action."状态查看</td>";
                                                     }
                                             break;
                                     fault:
                                             echo "<script>alert('未知的操作名')</script>";		
                             }
                             $contentString = $contentString.$tdString;			
                    } 
                }
		
		return $contentString;
	}
}



//开始分不同条件调用类来生成状态表

if($_GET) {
	$table = $_GET[table];
	$status = $_GET[status];
	$pems = $ident[pems];
	$user = $ident[user];
	$company_name = $ident[company_name];

// 查找条件由用户权限决定。查找的时候，管理员用户可以查找所有的项目，企业用户可以查找企业名字是当前企业的项目，而个人账户只能查找当前账户申请的项目	
	switch($pems) {
		case 1:
			$queryCondition = "where  project_status='$status'";
			break;
		case 11:
			if ($table === 'project_application') {
				$queryCondition = "where  project_status='$status' and tech_resource='专利技术产业化'";
			} else {
				$queryCondition = "where  project_status='$status'";
			}
			break;
		case 12:
			if ($table === 'project_application') {
				$queryCondition = "where  project_status='$status' and tech_resource='引进省外、国外技术消化创新'";
			} else {
				$queryCondition = "where  project_status='$status'";
			}
			break;
		case 13:
			if ($table === 'project_application') {
				$queryCondition = "where  project_status='$status' and (tech_resource='自主开发' or tech_resource='产学研联合攻关' or tech_resource='省内其他单位技术')";
			} else {
				$queryCondition = "where  project_status='$status'";
			}
			break;
		case 2:
			$queryCondition = "where company_name='$company_name' and  project_status='$status'";
			break;
		case 3:
			$queryCondition = "where user='$user' and  project_status='$status'";
			break;
		default:
			echo "<script>alert('非法用户')</script>";
			exit();
	}
	
// 查找条件由用户权限决定。查找的时候，管理员用户可以查找所有的项目，企业用户可以查找企业名字是当前企业的项目，而个人账户只能查找当前账户申请的项目
//	$statusMappingArr = array('1'=>'个人待上报项目','2'=>'个人提交项目','3'=>'企业待上报项目','4'=>'待修改项目','5'=>'待审核项目','6'=>'已受理项目','7'=>'已立项项目','8'=>'落选项目');
switch($status) {
		case 1:
			switch ($pems) {
				case 1:
					$actionArr = NULL; // 管理员用户操作项
					break;
				case 2:
					$actionArr = NULL; // 企业用户操作项
					break;
				case 3:
					$actionArr = array('编辑','删除','上报企业'); // 个人用户操作项
					break;
				default:
					echo "<script>alert('非法用户')</script>";exit();
			}
			break;
		case 2:
			switch ($pems) {
				case 1:
					$actionArr = NULL; // 管理员用户操作项
					break;
				case 2:
					$actionArr = array('编辑','退回修改','上报'); // 企业用户操作项
					break;
				case 3:
					$actionArr = NULL; // 个人用户操作项
					break;
				default:
					echo "<script>alert('非法用户')</script>";exit();
			}
			break;
		case 3:
			switch ($pems) {
				case 1:
					$actionArr = NULL; // 管理员用户操作项
					break;
				case 2:
					$actionArr = array('编辑','删除','上报'); // 企业用户操作项
					break;
				case 3:
					$actionArr = NULL; // 个人用户操作项
					break;
				default:
					echo "<script>alert('非法用户')</script>";exit();
			}
			break;
		case 4:
			switch ($pems) {
				case 1:
					$actionArr = NULL; // 管理员用户操作项
					break;
				case 11:
					$actionArr = NULL; // 管理员用户操作项
					break;
				case 12:
					$actionArr = NULL; // 管理员用户操作项
					break;
				case 13:
					$actionArr = NULL; // 管理员用户操作项
					break;
				case 2:
					$actionArr = array('编辑','上报'); // 企业用户操作项
					break;
				case 3:
					$actionArr = array('编辑','上报企业'); // 个人用户操作项
					break;
				default:
					echo "<script>alert('非法用户')</script>";exit();
			}
			break;
		case 5:
			if ($pems != 1 && $pems != 13) {
				$actionArr = NULL;
			} else {
				if ($table == 'project_application') {
					$actionArr = array('审核通过','审核落选','退回修改');
				} else if ($table == 'tech_awards') {
					$actionArr = array('审核通过','审核落选','退回修改');
				} else if ($table == 'tech_challenge') {
					$actionArr = array('审核通过');
				} else {
					$actionArr = array('审核通过','退回修改');
				}
			}
			break;
		case 6:
			if ($pems == 1 || $pems == 12 || $pems ==13) {
				if ($table == 'project_application') {
					$actionArr = array('立项');
				} else {
					$actionArr = NULL;
				}
			} else {
				if ($table == 'project_application') {
				} else if ($table == 'project_contract' || $table == 'project_task'){
					$actionArr = array('填写中期检查报告');
				} else if ($table == 'project_inprogress'){
					$actionArr = array('填写结题报告');
				} else if ($table == 'tech_challenge') {
					$actionArr = array('签订合同');
				} else {
					$actionArr = NULL;
				}
			}
			break;
		case 7:
			if ($pems == 1 || $pems == 13) {
				$actionArr = NULL;
			} else {
					$actionArr = array('签订合同');
			}
			break;
		case 9:
			if ($pems == 1 || $pems == 11 || $pems == 12 || $pems == 13) {
				if ($table == 'project_application') {
					$actionArr = array('初审通过','审核落选','退回修改');
				} else if ($table == 'tech_awards') {
					$actionArr = array('审核通过','审核落选');
				} else if ($table == 'tech_challenge') {
					$actionArr = array('审核通过');
				} else if ($table == 'tech_contract') {
					$actionArr = NULL;
				} else {
					$actionArr = array('审核通过','退回修改');
				}
			} else {
				$actionArr = NULL;
			}
			break;
		default:
			$actionArr = NULL;
	}

// 显示内容由表格类型决定	
	switch($table) {
		case 'project_application':
			$tableHeaderArr = array('project_name'=>'项目名称','project_class'=>'计划类别','company_name'=>'所属单位','fuzeren_name'=>'负责人','apply_date'=>'申请日期'); 
			$queryContent = 'project_id,user,'.implode(',',array_keys($tableHeaderArr));
			$query = "select $queryContent from $table $queryCondition";
			$nameExchangeArrList = array('general_project'=>'一般科技计划项目','key_project'=>'重点科技计划项目','default'=>'未知的项目类别');
			break;
		case 'project_contract':
			$tableHeaderArr = array('project_name'=>'项目名称','project_class'=>'计划类别','company_name'=>'所属单位','fuzeren_name'=>'负责人','apply_date'=>'申请日期'); 
			$queryContent = 'project_id,user,'.implode(',',array_keys($tableHeaderArr));
			$query = "select $queryContent from $table $queryCondition";
			$nameExchangeArrList = array('key_industry'=>'重点工业项目','key_agriculture'=>'重点农业项目','key_society'=>'重点社会发展项目','default'=>'未知的项目类别');
			break;
		case 'project_task':
			$tableHeaderArr = array('project_name'=>'项目名称','company_name'=>'所属单位','fuzeren_name'=>'负责人','apply_date'=>'申请日期'); 
			$queryContent = 'project_id,user,'.implode(',',array_keys($tableHeaderArr));
			$query = "select $queryContent from $table $queryCondition";
			break;
		case 'project_inprogress':
			$tableHeaderArr = array('project_name'=>'项目名称','company_name'=>'所属单位','project_manager'=>'负责人','apply_date'=>'申请日期'); 
			$queryContent = 'project_id,user,'.implode(',',array_keys($tableHeaderArr));
			$query = "select $queryContent from $table $queryCondition";
			break;
		case 'project_conclusion':
			$tableHeaderArr = array('project_name'=>'项目名称','company_name'=>'所属单位','primary_member'=>'负责人','apply_date'=>'申请日期'); 
			$queryContent = 'project_id,user,'.implode(',',array_keys($tableHeaderArr));
			$query = "select $queryContent from $table $queryCondition";
			break;
		case 'tech_awards':
			$tableHeaderArr = array('project_name'=>'项目名称','company_name'=>'所属单位','primary_member'=>'主要完成人','apply_date'=>'申请日期'); 
			$queryContent = 'project_id,user,'.implode(',',array_keys($tableHeaderArr));
			$query = "select $queryContent from $table $queryCondition";
			break;
		case 'tech_challenge':
			$tableHeaderArr = array('project_name'=>'项目名称','company_name'=>'所属单位','contact_name'=>'联系人','apply_date'=>'申请日期'); 
			$queryContent = 'project_id,user,'.implode(',',array_keys($tableHeaderArr));
			$query = "select $queryContent from $table $queryCondition";
			break;
		case 'tech_contract':
			$tableHeaderArr = array('project_name'=>'项目名称','company_name'=>'所属单位','manager_name'=>'负责人','apply_date'=>'申请日期'); 
			$queryContent = 'project_id,user,'.implode(',',array_keys($tableHeaderArr));
			$query = "select $queryContent from $table $queryCondition";
			break;
		default:
			echo "<script>alert('未知的执行目标')</script>";
			exit();		
	}
	$projectApplication2 = new genStatusTable($table,$status,$pems,$tableHeaderArr,$query,$actionArr,$nameExchangeArrList,$nameExchangeArrList);
	$projectApplication2->createTable();
	
}

?>

</div>
</html>

