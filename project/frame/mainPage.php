<?php
	session_start();
        include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<script type="text/javascript" src="../../conf.js"></script>
<link rel="stylesheet" href="../css/superfish.css" media="screen">
<script src="../js/jquery-1.10.2.js"></script>
<script src="../js/hoverIntent.js"></script>
<script src="../js/superfish.js"></script>
<script>
(function($){ //create closure so we can safely use $ as alias for jQuery

	$(document).ready(function(){

		// initialise plugin
		var example = $('#example').superfish({
			//add options here if required
		});

		// buttons to demonstrate Superfish's public methods
		$('.destroy').on('click', function(){
			example.superfish('destroy');
		});

		$('.init').on('click', function(){
			example.superfish();
		});

		$('.open').on('click', function(){
			example.children('li:first').superfish('show');
		});

		$('.close').on('click', function(){
			example.children('li:first').superfish('hide');
		});
	});

})(jQuery);

function loadIframe(page) {
	window.frames[1].location.href=page;
}

</script>
<style type="text/css">
@media print {
	#title_id,#menu_id {
		display:none;
	}
	#content_id {
	position: fixed;
	left: -8px;
	top: -100px;
	width: 100%;
	z-index: 1;
	}
}

body {
	margin:0px;
}

.headBar {
	background-image:url('../pic/titlebg.gif');
	background-repeat:repeat-x;
}

#title_id {
	top:0;
	width:980px;
	height:50px;
	margin:auto;	
}

.menuBar {
	background-color:#bdd2ff;
	border-left: 1px solid #fff;
	border-top: 1px solid rgba(255,255,255,.5);
	border-bottom:1px solid #009;
}

#menu_id {
	font-size:14px;
	line-height:18px;
	top:50px;
	left:-1px;
	width:980px;
	margin:auto;
	height:30px;
	border-left: 1px solid #fff;

}
.content_bg {
	background-color:#999;
}

#content_id {
	position:relative;
	top:0px;
	width:980px;
	height:1100px;
	margin:0 auto;
	z-index:1;
	background-color:#FFF;
}
</style>
<title>龙游县科技型企业梯度培育平台</title>
</head>

<body>
<div class="headBar">
<div id="title_id">
<iframe id='frame' name='title' src='../frame/title.php' width="100%" height="50px" frameborder="0"></iframe>
</div>
</div>
<div class="menuBar">
<div id="menu_id">
<ul class="sf-menu" id="example">
	<li><a href="./welcome.php" target='content'>首页</a></li>
<?php
$company_info_status = check_company_info();
switch ($_SESSION[pems]) {
	case (1): 
		echo "
			<li><a href='../frame/company_info_summarize.php' target='content'>企业汇总</a></li>
			<li><a href='../proc/verify_account.php?type=company' target='content'>审核企业账号</a></li>
		<li><a>县科技计划项目</a>
			<ul>
				<li><a href='../frame/project_status_frame.php?table=project_application' target='content'>项目状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=project_contract' target='content'>合同书状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=project_task' target='content'>任务书状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=project_inprogress' target='content'>中期检查状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=project_conclusion' target='content'>结题状态</a></li>
			</ul>
			<li><a href='../frame/project_status_frame.php?table=tech_awards' target='content'>科技进步奖</a></li>
  		<li><a>技术难题</a>
			<ul>
				<li><a href='../frame/project_status_frame.php?table=tech_challenge' target='content'>技术难题状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=tech_contract' target='content'>合同管理</a></li>
			</ul>";
		break;
	case (11): 
		echo "
			<li><a href='../frame/company_info_summarize.php' target='content'>企业汇总</a></li>
		<li><a>县科技计划项目</a>
			<ul>
				<li><a href='../frame/project_status_frame.php?table=project_application' target='content'>项目状态</a></li>
			</ul>";
		break;
	case (12): 
		echo "
			<li><a href='../frame/company_info_summarize.php' target='content'>企业汇总</a></li>
			<li><a>县科技计划项目</a>
			<ul>
				<li><a href='../frame/project_status_frame.php?table=project_application' target='content'>项目状态</a></li>
			</ul>
  			<li><a>技术难题</a>
			<ul>
				<li><a href='../frame/project_status_frame.php?table=tech_challenge' target='content'>技术难题状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=tech_contract' target='content'>合同管理</a></li>
			</ul>";
		break;
	case (13): 
		echo "
			<li><a href='../frame/company_info_summarize.php' target='content'>企业汇总</a></li>
		<li><a>县科技计划项目</a>
			<ul>
				<li><a href='../frame/project_status_frame.php?table=project_application' target='content'>项目状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=project_contract' target='content'>合同书状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=project_task' target='content'>任务书状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=project_inprogress' target='content'>中期检查状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=project_conclusion' target='content'>结题状态</a></li>
			</ul>
			<li><a href='../frame/project_status_frame.php?table=tech_awards' target='content'>科技进步奖</a></li>
			";
		break;
	case (2):
		echo "<li>";
		switch ($company_info_status){
			case(1):
				echo "<a href='../table/company_info.php' target='content' onClick=\"alert('即将进入填写企业资料页面')\">企业资料</a>";
				break;
			case(2):
				echo "<a href='../edit/edit_company_info.php' target='content' onClick=\"alert('需要进一步完善您的企业资料')\">企业资料</a>";
				break;
			case(3):
				echo "<a href='../show/show_company_info.php' target='content'>企业资料</a>";
				break;
			default:
				echo "<a href='../show/edit_company_info.php' target='content'>企业资料</a>";
		}
		echo "</li>
		<li><a href='../proc/verify_account.php?type=person' target='content'>下属用户管理</a></li>
		<li><a>县科技计划项目</a>
			<ul>
				<li>";
		
		switch ($company_info_status){
			case(1):
				echo "<a href='../table/company_info.php' target='content' onClick=\"alert('您还未填写企业资料,请填写企业资料才能进行项目的申请')\">申请项目</a>";
				break;
			case(2):
				echo "<a href='../edit/edit_company_info.php' target='content' onClick=\"alert('需要进一步完善您的企业资料才能进行项目的申请')\">申请项目</a>";
				break;
			case(3):
				echo "<a href='../table/project_application_table.php' target='content'>申请项目</a>";
				break;
			default:
				echo "未知的企业资料状态";
				exit();
		}
		echo "</li>";
		echo "
			<li><a href='../frame/project_status_frame.php?table=project_application' target='content'>项目状态</a></li>
			<li><a href='../frame/project_status_frame.php?table=project_contract' target='content'>合同书状态</a></li>
			<li><a href='../frame/project_status_frame.php?table=project_task' target='content'>任务书状态</a></li>
			<li><a href='../frame/project_status_frame.php?table=project_inprogress' target='content'>中期检查状态</a></li>
			<li><a href='../frame/project_status_frame.php?table=project_conclusion' target='content'>结题状态</a></li>
			</ul></li>
  			<li><a>科技进步奖</a>
				<ul>
				<li><a href='../table/tech_awards_table.php' target='content'>申请科技进步奖</a></li>
				<li><a href='../frame/project_status_frame.php?table=tech_awards' target='content'>获奖状态</a></li>
				</ul>
			</li>
			
  			<li><a>技术难题</a>
				<ul>
				<li><a href='../table/tech_challenge_table.php' target='content'>申请技术难题</a></li>
				<li><a href='../frame/project_status_frame.php?table=tech_challenge' target='content'>技术难题状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=tech_contract' target='content'>合同管理</a></li>
				</ul>
			</li>
			";
		break;
	case (3):
		echo "<li>";
		switch ($company_info_status){
			case(1):
				echo "<a href='../table/company_info.php' target='content' onClick=\"alert('即将进入填写企业资料页面')\">企业资料</a>";
				break;
			case(2):
				echo "<a href='../edit/edit_company_info.php' target='content' onClick=\"alert('需要进一步完善您的企业资料')\">企业资料</a>";
				break;
			case(3):
				echo "<a href='../show/show_company_info.php' target='content'>企业资料</a>";
				break;
			default:
				echo "<a href='../show/edit_company_info.php' target='content'>企业资料</a>";
		}
		echo "</li>
		<li><a>县科技计划项目</a>
			<ul>
				<li>";
		
		switch ($company_info_status){
			case(1):
				echo "<a href='../table/company_info.php' target='content' onClick=\"alert('您还未填写企业资料,请填写企业资料才能进行项目的申请')\">申请项目</a>";
				break;
			case(2):
				echo "<a href='../edit/edit_company_info.php' target='content' onClick=\"alert('企业资料填写不完整，请完善您的企业资料才能进行项目的申请')\">申请项目</a>";
				break;
			case(3):
				echo "<a href='../table/project_application_table.php' target='content'>申请项目</a>";
				break;
			default:
				echo "未知的企业资料状态";
				exit();
		}
		echo "</li>";
		echo "
			<li><a href='../frame/project_status_frame.php?table=project_application' target='content'>项目状态</a></li>
			<li><a href='../frame/project_status_frame.php?table=project_contract' target='content'>合同书状态</a></li>
			<li><a href='../frame/project_status_frame.php?table=project_task' target='content'>任务书状态</a></li>
			<li><a href='../frame/project_status_frame.php?table=project_inprogress' target='content'>中期检查状态</a></li>
			<li><a href='../frame/project_status_frame.php?table=project_conclusion' target='content'>结题状态</a></li>
			</ul></li>
  			<li><a>科技进步奖</a>
				<ul>
				<li><a href='../table/tech_awards_table.php' target='content'>申请科技进步奖</a></li>
				<li><a href='../frame/project_status_frame.php?table=tech_awards' target='content'>获奖状态</a></li>
				</ul>
			</li>
			
  			<li><a>技术难题</a>
				<ul>
				<li><a href='../table/tech_challenge_table.php' target='content'>申请技术难题</a></li>
				<li><a href='../frame/project_status_frame.php?table=tech_challenge' target='content'>技术难题状态</a></li>
				<li><a href='../frame/project_status_frame.php?table=tech_contract' target='content'>合同管理</a></li>
				</ul>
			</li>
			";
		break;
	default:
		session_destroy();
		echo "<script>alert('非法用户！');top.location.href= '../register/login.php';</script>";;
		exit();
}
?>
</ul>
</div>
</div>
<div class="content_bg">
<div id="content_id">
<iframe id='welcome' src=<?php
	if($_GET[iframe_page]) {
		echo '../show/'.$_GET[iframe_page].'.php';
	} else {
		echo 'welcome.php';
	}
?>
 name="content" width="100%" height="100%" frameborder="auto" scrolling="auto" style="position:relative;top:-12px;"></iframe>
</div>
</div>
</body>
</html>
