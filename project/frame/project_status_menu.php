<?php
session_start();
include ("../../conf.php");
$ident = identify_user($_SESSION['id'],$_SESSION['cookshell']);
$user = $ident['user'];
$company_name = $ident['company_name'];
$pems = $ident['pems'];
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
</head>
<style type="text/css">
li {
	font-family:"宋体";
	font-weight:bold;
	float:left;
	font-size:12px;
	border-bottom: 1px solid #666;
	background:#d0d0d0;
	line-height:30px;
}

ul.nav {
	margin:0;
	list-style: none; /* 这将删除列表标记 */
	position:absolute;
	left:-40px;
	top: 0px;
}

ul.nav a, ul.nav a:visited { /* 对这些选择器进行分组可确保链接即使在访问之后也能保持其按钮外观 */
	padding: 2px 2px 2px 10px;
	display: block; /* 这将为链接赋予块属性，使其填满包含它的整个 LI。这样，整个区域都可以响应鼠标单击操作。 */
	width: 132px;  /*此宽度使整个按钮在 IE6 中可单击。如果您不需要支持 IE6，可以删除它。请用侧栏容器的宽度减去此链接的填充来计算正确的宽度。 */
	height:30px;
	text-decoration: none;
	background-color: lightskyblue;
	padding-top:5px;

}

ul.nav a:hover, ul.nav a:active, ul.nav a:focus { /* 这将更改鼠标和键盘导航的背景和文本颜色 */
	background-color: #3D7878;
	color: #FFF;
}

body {
	background:#CCC;

}

</style>
<body>
<div class="status_menu">
<?php
if ($_GET[table]) {
	$table = $_GET[table];
	$statusListArr = configMenu($table,$pems,$statusMappingArr);
} else {
	echo "<script>alert('未定义执行目标')</script>";
	exit ();
}



//print_r($statusListArr);
echo "<ul class='nav'>";
foreach ($statusListArr as $currentStatus=>$statusName) {
	switch ($pems){
		case 1:
			$condition = "project_status='$currentStatus'";
			break;
		case 2:
			$condition = "project_status='$currentStatus' and company_name='$company_name'";
			break;
		case 3:
			$condition = "project_status='$currentStatus' and user='$user'";
			break;
		case 11:
			if ($table === 'project_application') {
				$condition = "project_status='$currentStatus' and tech_resource='专利技术产业化'";
			} else {
				$condition = "project_status='$currentStatus'";
			}
			break;
		case 12:
			if ($table === 'project_application') {
				$condition = "project_status='$currentStatus' and tech_resource='引进省外、国外技术消化创新'";
			} else {
				$condition = "project_status='$currentStatus'";
			}
			break;
		case 13:
			if ($table === 'project_application') {
				$condition = "project_status='$currentStatus' and (tech_resource='自主开发' or tech_resource='产学研联合攻关' or tech_resource='省内其他单位技术')";
			} else {
				$condition = "project_status='$currentStatus'";
			}
			break;
		default:
			echo "<script>alert('非法用户');</script>";
			exit();
	}
	$statusProjectNum = get_certain_num($table,'project_id',$condition);
	echo "<li><a href=./project_status.php?status=$currentStatus&table=$table target='project_status'>$statusName($statusProjectNum)</a></li>";
}
echo "</ul>";	
?>
</div>
</body>
</html>
