<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$pems = $_SESSION[pems];
	//time_expired(10);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>县科技局-菜单</title>
		<link rel="stylesheet" href="../css/superfish.css" media="screen">
		<style> body { max-width: 100em; } </style>
		<script src="../js/jquery-1.10.2.js"></script>
		<script src="../js/hoverIntent.js"></script>
		<script src="../js/superfish.js"></script>
		<script>

		(function($){ //create closure so we can safely use $ as alias for jQuery

			$(document).ready(function(){

				// initialise plugin
				var example = $('#example').superfish({
					//add options here if required
				});

				// buttons to demonstrate Superfish's public methods
				$('.destroy').on('click', function(){
					example.superfish('destroy');
				});

				$('.init').on('click', function(){
					example.superfish();
				});

				$('.open').on('click', function(){
					example.children('li:first').superfish('show');
				});

				$('.close').on('click', function(){
					example.children('li:first').superfish('hide');
				});
			});

		})(jQuery);


		</script>
	</head>
	<body>
<ul class="sf-menu" id="example">
	<li><a href="./welcome.php" target='content'>首页</a></li>
    <?php
        switch ($_SESSION[pems]) {
	case (2):
		echo "<li>";
		switch (check_company_info()){
			case(1):
				echo "<a href='../table/company_info.php' target='content' onClick=\"alert('即将进入填写企业资料页面')\">企业资料</a>";
				break;
			case(2):
				echo "<a href='../edit/edit_company_info.php' target='content' onClick=\"alert('企业资料还未填写完整，请完善您的企业资料')\">企业资料</a>";
				break;
			case(3):
				echo "<a href='../show/show_company_info.php' target='content'>企业资料</a>";
				break;
			default:
				echo "<a href='../show/edit_company_info.php' target='content'>企业资料</a>";
		}
		echo "</li>
		<li><a>县科技计划项目</a>
			<ul>
				<li>";
		
		switch (check_company_info()){
			case(1):
				echo "<a href='../table/company_info.php' target='content' onClick=\"alert('您还未填写企业资料,请填写企业资料才能进行项目的申请')\">申请项目</a>";
				break;
			case(2):
				echo "<a href='../edit/edit_company_info.php' target='content' onClick=\"alert('企业资料填写不完整，请完善您的企业资料才能进行项目的申请')\">申请项目</a>";
				break;
			case(3):
				echo "<a href='../table/project_application_table.php' target='content'>申请项目</a>";
				break;
			default:
				echo "未知的企业资料状态";
				exit();
		}
		echo "</li>";
		echo "
			<li><a href='../frame/project_status_frame.php?type=project_application' target='content'>现有项目状态</a></li>
			<li><a href='../frame/feasibility_report.php' target='content'>可行性报告</a></li>
			<li><a href='../proc/contract_status.php' target='content'>合同状态</a></li>
			<li><a href='../frame/project_status_frame.php?type=project_inprogress' target='content'>中期检查状态</a></li>
			<li><a href='../table/project_conclusion_table.php' target='content'>结题与统计</a></li>
			</ul></li>
  			<li><a>科技进步奖</a>
				<ul>
				<li><a href='../table/tech_awards_table.php' target='content'>申请科技进步奖</a></li>
				<li><a href='../table/tech_awards_table.php' target='content'>获奖状态</a></li>
				</ul>
			</li>
			
  			<li><a>技术难题</a>
				<ul>
				<li><a href='../table/tech_challenge_table.php' target='content'>申请技术难题</a></li>
				<li><a href='../table/tech_challenge_table.php' target='content'>已申请的难题</a></li>
				</ul>
			</li>
			";
		break;
	case (1): 
		$i = array(7);
		for ($a=1;$a<=6;$a++) {
			$i[$a] = get_certain_num("project_application",'*',"project_status='$a'");
		}
		echo "
			<li><a href='../frame/project_status_frame.php?type=project_application' target='content'>项目状态</a></li>
			<!--<li><a href='../frame/project_status_frame.php?type=contract' target='content'>合同状态</a></li>-->
			<li><a href='../proc/contract_status.php' target='content'>合同状态</a></li>
			<li><a href='../frame/project_status_frame.php?type=project_inprogress' target='content'>中期检查状态</a></li>
			<li><a href='../table/tech_awards_table.php' target='content'>获奖状态</a></li>
			<li><a href='../table/tech_challenge_table.php' target='content'>已申请的难题</a></li>";
		break;
	default:
		echo "<script>alert('非法用户！')";
		exit();
}
?>
	</body>
</html>