<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
include ("../../conf.php");
$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
$id = $_SESSION[id];
//不同用户进入此页面的权限
permissionBlocker('apply','');
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="../js/my_javascript.js"></script>
<script type="text/javascript">
mustFillArr = {'project_name':'项目名称','project_code':'计划编号','start_date':'开始日期','assume_unit':'承担单位','finish_date':'结束日期','project_manager':'项目负责人','manager_phone':'联系电话','manager_email':'电子邮件','adress':'通讯地址','total_budget':'预算总额（按合同）','self_budget':'预算总额（按合同）','fiscal_budget':'预算总额（按合同）','loan_budget':'预算总额（按合同）','total_implemented':'已落实经费','self_implemented':'已落实经费','fiscal_implemented':'已落实经费','loan_implemented':'已落实经费','progress_status':'项目执行情况'}
function checkCompanyForm(){
	return checkForm(mustFillArr);
}
</script>
<title>中期检查</title>
</head>
<h2><center>项目执行情况中期检查表</center></h2>
<form method="post" action="../proc/submit_form.php?project_id=<?php echo $_GET[project_id]; ?>" name="inprogress_form" onSubmit="return checkCompanyForm()"> 
<input type="hidden" value="apply" name="apply_edit">
<input type="hidden" value="project_inprogress" name="submit_type">
<fieldset style="font-size:12px">
<legend>一、项目基本情况</legend>
<table border="0" style="font-size:12px">
  <tr>
    <td>项目名称：<input type="text" name="project_name"></td>
  </tr>
  <tr>
    <td>计划编号：<br><input type="text" name="project_code"></td>
    <td>开始日期：<br><input type="text" name="start_date"></td>
  </tr>
  <tr>
    <td>承担单位：<br><input type="text" name="assume_unit"/></td>
    <td>结束日期：<br><input type="text" name="finish_date"/></td>
  </tr>
  <tr>
    <td>项目负责人：<br><input type="text" name="project_manager"/></td>
    <td>联系电话：<br><input type="text" name="manager_phone"/></td>
  </tr>
  <tr>
    <td>电子邮件：<br><input type="email" name="manager_email"/></td>
    <td>通讯地址：<br><input type="text" name="adress"/></td>
  </tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend>二、项目资金及其他配套条件落实情况</legend>
<table border="0" style="font-size:12px">
  <tr>
    <th>&nbsp;</th><th>项目总经费</th><th>其中：自筹</th><th>财政拨款</th><th>银行贷款</th>
  </tr>
  <tr>
    <th>预算总额（按合同）</th>
    <td><input type="text" name="total_budget"></td>
    <td><input type="text" name="self_budget"></td>
    <td><input type="text" name="fiscal_budget"></td>
    <td><input type="text" name="loan_budget"></td>
  </tr>
  <tr>
    <th>已落实经费</th>
    <td><input type="text" name="total_implemented"/></td>
    <td><input type="text" name="self_implemented"/></td>
    <td><input type="text" name="fiscal_implemented"/></td>
    <td><input type="text" name="loan_implemented"/></td>
  </tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend>项目执行情况</legend>
	<textarea name="progress_status" cols="100" rows="10" placeholder="（项目经费使用情况，项目主要研究内容，技术、经济指标完成情况，成果，问题建议等说明）"></textarea>
</fieldset>
<center><input style="font-size:14px" type="submit" name="submit" value="提交">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" onClick="history.go(-1)" value="返回"></center>
</form>

</html>
