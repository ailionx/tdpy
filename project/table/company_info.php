<?php
session_start();
include ("../../conf.php");
$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
$company_name = $ident[company_name];

//控制下拉表单的预设值
		function process_pd_list($name,$arr,$value) {
			foreach ($arr as $cla) {
				if ($value[$name] === $cla) {
					$mark[] = " selected";
				} else {
					$mark[] = " ";
				}
			}
			return $mark;
		}




?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<title>科技型企业基本信息表</title>
<script type="text/javascript" src="../../conf.js"></script>
<script type="text/javascript">	
mustFillArr = {"company_name":"企业名称","establish_time":"成立时间","company_address":"企业地址","industry_class":"行业类别","register_time":"注册时间","registered_capital":"注册资本","company_code":"组织机构代码","corporate_rep":"法人代表","rep_phone":"联系方式","contact":"联系人","contact_phone":"联系方式","business_scope":"经营范围","revenue":"营业收入","work_force":"职工总数","high_work_force":"大专以上职工数","tech_member":"科技人员数","jishuyituodanwei":"技术依托单位","rd_sale_ratio":"研发费占销售收入比","invent_patent":"发明专利数(当年)","invent_patent_all":"发明专利数(累计)","practical_patent":"实用新型专利(当年)","practical_patent_all":"实用新型专利(累计)","facade_patent":"外观设计专利数(当年)","facade_patent_all":"外观设计专利数(累计)","tech_awards":"科技成果获奖","high_product":"高新技术及产品和新产品","tech_introduce":"技术、产品开发引进","tech_coorperation":"科企合作","orientation_other":"其他","patent_invent":"发明","patent_practical":"实用型","patent_appear":"外观设计"};
function checkCompanyForm(){
	return checkForm(mustFillArr);
}
</script>
<style type="text/css">
.mustFillMark {
	color:red;
}

</style>
</head>

<h1><center>科技型企业基本信息表</center></h1>
<p style="font-size:15px">注:如果你在填写的时候需要额外的说明，请参考填写说明。<span class="mustFillMark">带*号为必填项</span></p>

<h3>填表说明</h3>
<ol style="font-size:12px">
<li>调查范围：对有科技人员、有科技投入、有科技成果（包括专利、软件产权等）、有科技产品和科技服务的工业企业、农业企业进行调查。</li>
<li>行业类别：以工业、农业、其他分类填写。</li>
<li>经营情况：高新技术产品、新产品销售收入包括高新技术产品销售收入，新产品销售收入以及技术性收入。技术性收入指技术开发、技术转让、技术入股、技术服务、技术培训、技术工程设计和承包、技术出口、引进技术消化吸收以及中试产品销售等技术贸易收入。</li>
<li>企业类别：没有获得市级以上高新技术企业、科技型企业、示范企业、试点企业和创新型企业的归入“其他”类。</li>
<li>高新技术产品和新产品：主要填写企业拥有的高新技术产品名称和市级以上认定的新产品名称。</li>
<li>研发中心类别：企业没有建立研发中心的填“其他”类。</li>
<li>科技创新项目意向：“专利申报”分别填写2014年意向申报的三类专利数。“研发中心建设”按梯度填写建设意向。“科技型企业升级”按梯度填写升级意向。“科企合作”填写同科研院校合作意向项目和对象。“技术、产品引进开发”填写意向开发引进技术或开发申报新产品意向。 “其他”填写需对接或引进科研人才、科研机构等方面的意向。</li>
<li>涉及数据事项：所有数据以2013年底为统计口径。</li>
<li>表格电子版下载：<a href='http://www.lyinfo.gov.cn' target='_blank'>http://www.lyinfo.gov.cn</a></li>
</ol>

<form action="../proc/submit_form.php?type=company_info" method="post"  onSubmit="return checkCompanyForm()">
<input type="hidden" value="company_info" name="submit_type">
<fieldset style="font-size:12px">
<table border="0" style="font-size:12px">
  <tr>
    <td><span class="mustFillMark">*</span>企业名称：<br><input type="text" name="company_name" value="<?php echo $company_name; ?>" readonly></td>
    <td><span class="mustFillMark">*</span>成立时间：<br><input type="text" name="establish_time"></td>
  </tr>
  <tr>
    <td><span class="mustFillMark">*</span>企业地址：<br><input type="text" name="company_address"></td>
    <td><span class="mustFillMark">*</span>行业类别：<br>
<?php 
	$industry_class_list = array('工业','农业','社会发展等');
	draw_pd_reset('industry_class',$industry_class_list);	
?>
</td>
  </tr>
  <tr>
    <td><span class="mustFillMark">*</span>注册时间：<br><input type="text" name="register_time"/></td>
    <td><span class="mustFillMark">*</span>注册资本：<br><input type="text" name="registered_capital"/></td>
    <td><span class="mustFillMark">*</span>组织机构代码：<br><input type="text" name="company_code"/></td>
  </tr>
  <tr>
    <td><span class="mustFillMark">*</span>法人代表：<br><input type="text" name="corporate_rep"/></td>
    <td><span class="mustFillMark">*</span>联系方式：<br><input type="text" name="rep_phone"/></td>
    <td>职务/职称：<br><input type="text" name="rep_title"/></td>
  </tr>
  <tr>
    <td><span class="mustFillMark">*</span>联系人：<br><input type="text" name="contact"/></td>
    <td><span class="mustFillMark">*</span>联系方式：<br><input type="text" name="contact_phone"/></td>
    <td>职务/职称：<br><input type="text" name="contact_title"/></td>
  </tr>
   <tr>
    <td>传真：<br><input type="text" name="fax"/></td>
    <td>电子邮件：<br><input type="email" name="email"/></td>
    <td>网址：<br><input type="url" name="website"/></td>
  </tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend><span class="mustFillMark">*</span>经营范围</legend>
<textarea name="business_scope" cols="80px" rows="5px"></textarea>
</fieldset>
<fieldset style="font-size:12px">
<legend>企业规模和占地情况</legend>
<table>
  <tr>
    <td><span class="mustFillMark">*</span>营业收入：<br><input type="text" name="revenue"/></td>
    <td><span class="mustFillMark">*</span>职工总数：<br><input type="text" name="work_force"/></td>
    <td><span class="mustFillMark">*</span>大专以上职工数：<br><input type="text" name="high_work_force"/></td>
  </tr>
  <tr>
    <td>占地面积：<br><input type="text" name="floor_space"/></td>
  	<td>利税收入：<br><input type="text" name="tax_revenue"/></td>
    <td>上缴税额：<br><input type="text" name="pay_tax"/></td>
    <td>高新技术产品、新产品等销售收入：<br><input type="text" name="hightech_revenue"/></td>
  </tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend><span class="mustFillMark">*</span>企业类别</legend>
<table>
	<tr>
		<td>高新技术企业 <input type="checkbox" name="array_company_class[hightech_country]">国家  <input type="checkbox" name="array_company_class[hightech_city]">市</td>
        <td><input type="checkbox" name="array_company_class[shengchuangxinshifan]">省级创新型示范企业</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_company_class[shengchuangxinxingshidian]">省级创新型试点企业</td>
        <td><input type="checkbox" name="array_company_class[shengkejixingzhongxiao]">省级科技型中小企业</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_company_class[shengzhuanlishifan]">省级专利示范企业</td>
        <td><input type="checkbox" name="array_company_class[shengnongyekeji]">省级农业科技型企业</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_company_class[shizhuanlishifan]">市级专利示范企业</td>
        <td><input type="checkbox" name="array_company_class[shichuangxinxing]">市级创新型企业</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_company_class[xiankejixing]">县级科技型企业</td>
        <td><input type="checkbox" name="array_company_class[class_other]">其他</td>
	</tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend><span class="mustFillMark">*</span>研发中心类别</legend>
<table>
	<tr>
		<td><input type="checkbox" name="array_reserch_class[shengjiqiyeyanjiuyuan]">省级企业研究院</td>
        <td><input type="checkbox" name="array_reserch_class[shengjigaoxinjishuqiyefazhanzhongxin]">省级高新技术企业研发中心</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_reserch_class[shengjinongyeqiyekejiyanfazhongxin]">省级农业企业科技研发中心</td>
        <td><input type="checkbox" name="array_reserch_class[shengjiquyukejichuangxinfuwuzhongxin]">省级区域科技创新服务中心</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_reserch_class[shengjidayuanmingxiaogongjianchuangxinzaiti]">省级大院名校共建创新载体</td>
        <td><input type="checkbox" name="array_reserch_class[shijigongchengjishuyanjiukaifazhongxin]">市级工程技术研究开发中心</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_reserch_class[qiyeshuyanfajigou]">企业所属研发机构</td>
        <td><input type="checkbox" name="array_reserch_class[rd_other]">其他</td>
	</tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend>科研活动情况</legend>
<table>
  <tr>
    <td><span class="mustFillMark">*</span>科技人员数：<br><input type="text" name="tech_member"/></td>
    <td><span class="mustFillMark">*</span>技术依托单位：<br><input type="text" name="jishuyituodanwei"/></td>
    <td><span class="mustFillMark">*</span>研发费占销售收入比：<br><input type="text" name="rd_sale_ratio"/></td>
  </tr>
  <tr>
  	<td><span class="mustFillMark">*</span>发明专利数(当年)：<br><input type="text" name="invent_patent"/></td>
    <td><span class="mustFillMark">*</span>发明专利数(累计)：<br><input type="text" name="invent_patent_all"/></td>
    <td><span class="mustFillMark">*</span>实用新型专利(当年)：<br><input type="text" name="practical_patent"/></td>
    <td><span class="mustFillMark">*</span>实用新型专利(累计)：<br><input type="text" name="practical_patent_all"/></td>
  </tr><tr>
    <td><span class="mustFillMark">*</span>外观设计专利数(当年)：<br><input type="text" name="facade_patent"/></td>
    <td><span class="mustFillMark">*</span>外观设计专利数(累计)：<br><input type="text" name="facade_patent_all"/></td>
  </tr>
</table>
<span class="mustFillMark">*</span>科技成果获奖<br>
<textarea name="tech_awards" cols="80px" rows="5px"></textarea><br>
<span class="mustFillMark">*</span>高新技术及产品和新产品<br>
<textarea name="high_product" cols="80px" rows="5px"></textarea>
</fieldset>
<fieldset style="font-size:12px">
<legend>科技创新项目意向</legend>
	<fieldset style="font-size:12px">
	<legend>专利申报</legend>
	<table>
	  <tr>
 	   <td><span class="mustFillMark">*</span>发明(项)：<br><input type="text" name="patent_invent"/></td>
	    <td><span class="mustFillMark">*</span>实用型(项)：<br><input type="text" name="patent_practical"/></td>
 	   <td><span class="mustFillMark">*</span>外观设计(项)：<br><input type="text" name="patent_appear"/></td>
 	 </tr>
	</table>
	</fieldset>
	<fieldset style="font-size:12px">
	<legend><span class="mustFillMark">*</span>研发中心建设</legend>
	<table>
	  <tr>
 	   <td colspan=4 >
<?php
$research_construction_arr = array('construction_country'=>'国家级','construction_province'=>'省级','construction_city'=>'市级','construction_county'=>'县级');
draw_checkbox_reset('array_research_construction',$research_construction_arr);

?>
	   </td>
 	 </tr>
	</table>
	</fieldset>
	<fieldset style="font-size:12px">
	<legend><span class="mustFillMark">*</span>科技型企业升级</legend>
	<table>
	  <tr>
 	   <td>
<?php
$tech_upgrade_arr = array('upgrade_country'=>'国家级','upgrade_province'=>'省级','upgrade_city'=>'市级','upgrade_county'=>'县级');
draw_checkbox_reset('array_tech_upgrade',$tech_upgrade_arr);

?>
		</td>
	 </tr>
	</table>
	</fieldset>
<table>
  <tr>
    <td><span class="mustFillMark">*</span>技术、产品开发引进：<br><input type="text" name="tech_introduce"/></td>
    <td><span class="mustFillMark">*</span>科企合作：<br><input type="text" name="tech_coorperation"/></td>
    <td><span class="mustFillMark">*</span>其他：<br><input type="text" name="orientation_other"/></td>
  </tr>
</table>
</fieldset>
<h3 align="center"><input type="submit" value="提交"></h3>
</form>


</html>
