<?php
session_start();
include ("../../conf.php");
$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
$id = $_SESSION[id];
//不同用户进入此页面的权限
permissionBlocker('apply','');
?>
<!DOCTYPE html5>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../js/my_javascript.js"></script>
<script type="text/javascript" src="../js/project_conclusion.js"></script>
<script type="text/javascript">
mustFillArr = {'project_name':'项目名称','assume_unit':'项目承担单位','primary_member':'主要完成人员','cooperating_unit':'项目协作单位','plan_code':'计划编号','start_finish':'项目起止年月','fiscal_final':'县财政拨款','loan_final':'贷款','self_final':'自筹','consult_cost':'调研咨询费','instrument_cost':'材料、仪器费','experiment_cost':'设计试验费','management_cost':'管理费','other_cost':'其它费用','project_brief':'项目简介（包括项目研究开发的主要内容，与国内外同类技术的比较等）','exceptation_completion':'项目合同规定的主要内容、技术经济指标及完成情况','inovation_point':'关键技术及创新点、获自主知识产权情况、成果应用和产业化情况','material_catalog':'提供的技术资料目录','self_evaluation':'项目承担单位对项目成果的自我评价','goverment_opinion':'县科技局、财政局结题意见','comment':'备注','project_code':'项目编号','investment_total':'项目总投资','reserch_expense':'研发经费投入','fiscal_expenditure':'县财政经费','comment_result':'备注'}
function checkCompanyForm(){
	return checkForm(mustFillArr);
}
</script>

<style type="text/css">
.content ul, .content ol { 
	padding: 0 15px 15px 40px; /* 此填充反映上述标题和段落规则中的右填充。填充放置于下方可用于间隔列表中其它元素，置于左侧可用于创建缩进。您可以根据需要进行调整。 */
}

/* ~~ 导航列表样式（如果选择使用预先创建的 Spry 等弹出菜单，则可以删除此样式） ~~ */
ul.nav_menu {
	position:fixed;
	left:10px;
	top:10px;
	float:left;
	list-style: none; /* 这将删除列表标记 */
	border-top: 1px solid #666; /* 这将为链接创建上边框 – 使用下边框将所有其它项放置在 LI 中 */
	margin-bottom: 15px; /* 这将在下面内容的导航之间创建间距 */
	margin-left:-30px;
	margin-right:10px;
}
ul.nav_menu li {
	border-bottom: 1px solid #666; /* 这将创建按钮间隔 */
	font-family:"宋体";
	font-size:14px;
	line-height:20px;
	text-align:center;
}
ul.nav_menu a, ul.nav_menu a:visited { /* 对这些选择器进行分组可确保链接即使在访问之后也能保持其按钮外观 */
	display: block; /* 这将为链接赋予块属性，使其填满包含它的整个 LI。这样，整个区域都可以响应鼠标单击操作。 */
	width: 110px;  /*此宽度使整个按钮在 IE6 中可单击。如果您不需要支持 IE6，可以删除它。请用侧栏容器的宽度减去此链接的填充来计算正确的宽度。 */
	text-decoration: none;
	color:#000;
}

.form_area {
	position:fixed;
	left:132px;
	top:17px;
}

</style>
<title>项目结题书</title>
</head>
<form method="post" action="../proc/submit_form.php?project_id=<?php echo $_GET[project_id]; ?>" name="conclusion_form" onSubmit="return checkCompanyForm()"> 
<input type="hidden" value="apply" name="apply_edit">
<input type="hidden" value="project_conclusion" name="submit_type">
<ul class="nav_menu">
<li><a href="#" onClick="fr_hide('conclusion1')">结题书（一）</button></a></li>
<li><a href="#" onClick="fr_hide('conclusion2')">结题书（二）</button></a></li>
<li><a href="#" onClick="fr_hide('result')">统计表</button></a></li>
<li><bold align="center"><input style="font-size:14px;color:#30F" type="submit" name="submit" value="保存"></bold></li>
</ul>
<div class="form_area">
<fieldset style="font-size:12px" id="conclusion1">
<legend><h2>龙游县科技计划项目结题书（一）</h2></legend>
<table style="font-size:12px">
	<tr>
		<td>项目名称<br /><input type="text" name="project_name" /></td>	
		<td>项目承担单位<br /><input type="text" name="assume_unit" /></td>
		<td>主要完成人员<br /><input type="text" name="primary_member" /></td>
    </tr>
    <tr>
		<td>项目协作单位<br /><input type="text" name="cooperating_unit" /></td>
		<td>计划编号<br /><input type="text" name="plan_code" /></td>
    	<td>项目起止年月<br /><input type="text" name="start_finish" /></td>
    </tr>	
</table>


<fieldset style="font-size:12px">
<legend>投入经费及来源（万元）</legend>
<table style="font-size:12px">
	<tr>
		<td>县财政拨款<br /><input type="text" name="fiscal_final" /></td>	
		<td>贷款<br /><input type="text" name="loan_final" /></td>
		<td>自筹<br /><input type="text" name="self_final" /></td>
    </tr>	
</table>
</fieldset>

<fieldset style="font-size:12px">
<legend>经费支出(万元)</legend>
<table style="font-size:12px">
	<tr>
		<td>调研咨询费<br /><input type="text" name="consult_cost" /></td>	
		<td>材料、仪器费<br /><input type="text" name="instrument_cost" /></td>
		<td>设计试验费<br /><input type="text" name="experiment_cost" /></td>
    </tr>
    <tr>
        <td>管理费<br /><input type="text" name="management_cost" /></td>
        <td>其它费用<br /><input type="text" name="other_cost" /></td>
    </tr>	
</table>
</fieldset>

<fieldset style="font-size:12px">
<legend>一、项目简介（包括项目研究开发的主要内容，与国内外同类技术的比较等）：</legend>
	<textarea cols="65px" rows="8px" name="project_brief"></textarea>
</fieldset>

<fieldset style="font-size:12px">
<legend>二、项目合同规定的主要内容、技术经济指标及完成情况：</legend>
	<textarea cols="65px" rows="8px" name="exceptation_completion"></textarea>
</fieldset>
</fieldset>

<fieldset style="font-size:12px" id="conclusion2">
<legend><h2>龙游县科技计划项目结题书（二）</h2></legend>
<fieldset style="font-size:12px">
<legend>三、关键技术及创新点、获自主知识产权情况、成果应用和产业化情况：</legend>
	<textarea cols="65px" rows="8px" name="inovation_point"></textarea>
</fieldset>

<fieldset style="font-size:12px">
<legend>提供的技术资料目录</legend>
	<textarea cols="65px" rows="5px" name="material_catalog"></textarea>
</fieldset>

<fieldset style="font-size:12px">
<legend>项目承担单位对项目成果的自我评价</legend>
	<textarea cols="65px" rows="5px" name="self_evaluation"></textarea>
</fieldset>

<fieldset style="font-size:12px">
<legend>县科技局、财政局结题意见</legend>
	<textarea cols="65px" rows="5px" name="goverment_opinion"></textarea>
</fieldset>

<fieldset style="font-size:12px">
<legend>备    注</legend>
	<textarea cols="65px" rows="5px" name="comment"></textarea>
</fieldset>
</fieldset>
<fieldset style="font-size:12px" id="result">
<legend><h3>龙游县科技计划项目实施效果统计表</h3></legend>
<table style="font-size:12px">
	<tr>
		<td>项目编号<br /><input type="text" name="project_code" /></td>	
		<td>项目总投资<br /><input type="text" name="investment_total" /></td>
    </tr>
    <tr>
        <td>研发经费投入<br /><input type="text" name="reserch_expense" /></td>
        <td>县财政经费<br /><input type="text" name="fiscal_expenditure" /></td>
    </tr>	
</table>


<fieldset style="font-size:12px">
<legend>销售收入</legend>
<table style="font-size:12px">
	<tr>
		<th>销售收入</th>	
		<th>税　　收</th>
		<th>利　　润</th>
    </tr>
<?php 
	for($i=0;$i<3;$i++){
		$ii=2010+$i;
		echo "
	<tr>
		<td>".$ii."年<input type='text' name='array_income[$i]' /></td>	
		<td><input type='text' name='array_tax[$i]' /></td>
		<td><input type='text' name='array_profit[$i]' /></td>
    </tr>";
	}
?>
</table>
</fieldset>

<fieldset style="font-size:12px">
<legend>备    注</legend>
	<textarea cols="65px" rows="5px" name="comment_result"></textarea>
</fieldset>
</fieldset>
</form>
<center><input type="button" onClick="history.go(-1)" value="返回"></center>
</div>
</html>
