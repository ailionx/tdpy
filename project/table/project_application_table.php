<!DOCTYPE HTML>
<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	//不同用户进入此页面的权限
	permissionBlocker('apply','');
?>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<title>项目申请</title>
<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../js/my_javascript.js"></script>
<script type="text/javascript" src="../js/project_application.js"></script>
<script type="text/javascript" src="../js/ajaxfileupload.js"></script>
<script type="text/javascript">
	function ajaxFileUpload()
	{
		$("#loading")
		.ajaxStart(function(){
			$(this).show();
		})
		.ajaxComplete(function(){
			$(this).hide();
		});

		$.ajaxFileUpload
		(
			{
				url:'../proc/doajaxfileupload.php?project_name='+document.getElementById('project_name_id').value,
				secureuri:false,
				fileElementId:'fileToUpload',
				dataType: 'json',
				data:{name:'logan', id:'id'},
				success: function (data, status)
				{
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{
							alert(data.error);
						}else
						{
							alert(data.msg);
						}
					}
				},
				error: function (data, status, e)
				{
					alert(e);
				}
			}
		)
		
		return false;

	}

mustFillArr = {'project_name':'项目名称','industry_type':'项目行业分类','start_date':'开始日期','finish_date':'完成日期','tech_resource':'项目技术来源','project_class':'项目计划类别','total_expenditure':'总计','zichou':'自筹','yinhangdaikuan':'银行贷款','caizhengshenqing':'向县财政申请','other_expenditure':'其他','shebeifei':'设备费','cailiaofei':'材料费','shiyanhuayanjiagongfei':'试验化验加工费','ranliaodonglifei':'燃料动力费','chailvfei':'差旅费','renyuanlaowufei':'人员劳务费','hezuojiaoliufei':'合作、协作研究与交流费','zhishichanquanshiwufei':'出版/文献/信息传播知识产权事务费','huiyifei':'会议费','guanlifei':'管理费','zhuanjiazixunfei':'专家咨询费','other_spending':'其他开支','nianzengchanzhi':'年增产值（万元）','nianzenglirun':'年增利润（万元）','nianzengshuijin':'年增税金（万元）','nianchuanghui':'年创汇（万美元）','nianjiehui':'年节汇（万美元）','lunwenshu':'论文数','zhuanli':'专利','famingzhuanli':'其中发明专利','beizhu':'备注','danweimingcheng':'单位名称','danweijiancheng':'单位简称','farendaibiao':'法人代表','suozaididaima':'所在地代码','danweileixing':'单位类型','xiangxidizhi':'详细地址','youzhengbianma':'邮政编码','danweiemail':'单位EMAIL','lianxiren':'联系人','lianxidianhua':'联系电话','chuanzhen':'传真','kaihuyinhang':'开户银行','yinhangzhanghao':'银行帐号','zhuguanbumen':'主管部门','hezuodanweizongshu':'合作单位总数（包含第一申请单位）','chengdandanweishu':'承担单位数','canjiadanweishu':'参加单位数','array_unit_danweimingcheng[0]':'单位名称','array_unit_farendaibiao[0]':'法人代表','array_unit_zhize[0]':'职责','fuzeren_name':'姓名','fuzeren_shenfenzheng':'身份证号码','fuzeren_phone':'联系电话','fuzeren_email':'EMAIL','fuzeren_xueli':'学历','fuzeren_xuewei':'学位','fuzeren_zhiwu':'专业技术职务','fuzeren_zhuanye':'专业','fuzeren_fengong':'在本项目中的分工','fuzeren_danwei':'工作单位','member_number':'总共项目组人数','array_member_name[0]':'姓名','array_member_chushengnianyue[0]':'出生年月','array_member_zhiwu[0]':'专业技术职务','array_member_zhuanye[0]':'专业','array_member_gongzuodanwei[0]':'工作单位','array_member_fengong[0]':'在本项目中分工','litiyiju':'四、本项目的立题依据：包括目的、意义、国内外概况和发展趋势，现有工作基础和条件（包括研究工作基础、装备条件和技术力量及项目负责人技术工作简历）','neirongheyuqichengguo':'五、研究、开发内容和预期成果：具体研究、开发内容和重点解决的技术关键问题，要达到的主要技术、经济指标及经济社会环境效益，拟采取的研究方法和技术路线或工艺流程（可用框图表示）','array_progress_start[0]':'进度目标要求','array_progress_end[0]':'进度目标要求','array_progress_content[0]':'进度目标要求','shichangziyuan':'七、市场资源','shenqingjingfei':'八、项目申请经费：（计算根据及理由）'};
function checkCompanyForm(){
	return checkForm(mustFillArr);
	return false;
}
</script>	
<style type="text/css">
.content ul, .content ol { 
	padding: 0 15px 15px 40px; /* 此填充反映上述标题和段落规则中的右填充。填充放置于下方可用于间隔列表中其它元素，置于左侧可用于创建缩进。您可以根据需要进行调整。 */
}

/* ~~ 导航列表样式（如果选择使用预先创建的 Spry 等弹出菜单，则可以删除此样式） ~~ */
ul.nav_menu {
	position:fixed;
	left:10px;
	top:10px;
	float:left;
	list-style: none; /* 这将删除列表标记 */
	border-top: 1px solid #666; /* 这将为链接创建上边框 – 使用下边框将所有其它项放置在 LI 中 */
	margin-bottom: 15px; /* 这将在下面内容的导航之间创建间距 */
	margin-left:-30px;
	margin-right:10px;
}
ul.nav_menu li {
	border-bottom: 1px solid #666; /* 这将创建按钮间隔 */
	font-family:"宋体";
	font-size:14px;
	line-height:20px;
	text-align:center;
}
ul.nav_menu a, ul.nav_menu a:visited { /* 对这些选择器进行分组可确保链接即使在访问之后也能保持其按钮外观 */
	display: block; /* 这将为链接赋予块属性，使其填满包含它的整个 LI。这样，整个区域都可以响应鼠标单击操作。 */
	width: 110px;  /*此宽度使整个按钮在 IE6 中可单击。如果您不需要支持 IE6，可以删除它。请用侧栏容器的宽度减去此链接的填充来计算正确的宽度。 */
	text-decoration: none;
	color:#000;
}

.form_area {
	position:fixed;
	left:132px;
	top:17px;
}

</style>
</head>


<form method="post" action="../proc/submit_form.php" name="projectForm"  onSubmit="return checkCompanyForm()"> 
<input type="hidden" value="apply" name="apply_edit">
<input type="hidden" value="project_application" name="submit_type">
<ul class="nav_menu">
<li><a href="#" onClick="fr_hide('jibenxiangmu')">项目基本情况</button></a></li>
<li><a href="#" onClick="fr_hide('gailan')">项目概览</button></a></li>
<li><a href="#" onClick="fr_hide('chengdandanwei')">承担单位</button></a></li>
<li><a href="#" onClick="fr_hide('xiangmuchengyuan')">负责人成员</button></a></li>
<li><a href="#" onClick="fr_hide('litiyiju')">立题依据</button></a></li>
<li><a href="#" onClick="fr_hide('kaifaneirong')">开发内容和<br>预期成果</button></a></li>
<li><a href="#" onClick="fr_hide('jihuajindu')">计划进度目标</button></a></li>
<li><a href="#" onClick="fr_hide('shichangziyuan')">市场资源</button></a></li>
<li><a href="#" onClick="fr_hide('shenqingjingfei')">项目申请经费</button></a></li>
<li><bold align="center"><input style="font-size:14px;color:#30F" type="submit" name="submit" value="保存"></bold></li>
</ul>
<div class="form_area">
<fieldset id="jibenxiangmu">
    <legend>一、项目基本情况</legend>
    <table cellpadding="0px">
    <tr>
    <td align="justify">项目名称: <br/><input type="text" name="project_name" id='project_name_id' onBlur="check_project_exist(document.projectForm.project_name.value)"/></td><td><span></span></td>
	<td align="justify" colspan="2">项目行业分类：<br/>
	<select id="select_industry_class" name="industry_type">
			<option value="" selected="selected"></option>
		<optgroup label="农、林、牧、渔业">
			<option value="A01">农业</option>
	    	<option value="A02">林业</option>
	    	<option value="A03">畜牧业</option>
	    	<option value="A04">渔业</option>
	        <option value="A05">农、林、牧、渔服务业</option>
	    </optgroup>
		<optgroup label="采掘业">
			<option value="B06">煤炭采选业</option>
	    	<option value="B07">石油和天然气开采业</option>
	    	<option value="B08">黑色金属矿采选业</option>
	    	<option value="B09">有色金属矿采选业</option>
	        <option value="B10">非金属矿采选业</option>
	        <option value="B11">其他矿采选业</option>
	        <option value="B12">木材及竹材采运业</option>
	    </optgroup>
	    <optgroup label="制造业">
			<option value="C13">食品加工业</option>
	    	<option value="C14">食品制造业</option>
	    	<option value="C15">饮料制造业</option>
    		<option value="C16">烟草加工业</option>
    	    <option value="C17">纺织业</option>
	        <option value="C18">服装及其他纤维制品制造业</option>
	        <option value="C19">皮革、毛皮、羽绒及制品业</option>
	        <option value="C20">木材加工及竹、藤、棕、草制品业</option>
	        <option value="C21">家具制造业</option>
	        <option value="C22">造纸及纸制品业</option>
	        <option value="C23">印刷业、记录媒介的复制</option>
	        <option value="C24">文教体育用品制造业</option>
	        <option value="C25">石油加工及炼焦业</option>
	        <option value="C26">化学原料及化学制品制造业</option>
	        <option value="C27">医药制造业（含生物制造业）</option>
	        <option value="C28">化学纤维制造业</option>
	        <option value="C29">橡胶制品业</option>
	        <option value="C30">塑料制品业</option>
	        <option value="C31">非金属矿物制品业</option>
	        <option value="C32">黑色金属冶炼及压延加工业</option>
	        <option value="C33">有色金属冶炼及压延加工业</option>
	        <option value="C34">金属制品业（含日用金属制品业）</option>
	        <option value="C35">普通机械制造业</option>
	        <option value="C36">专用设备制造业</option>
	        <option value="C37">交通运输设备制造业</option>
	        <option value="C39">武器弹药制造业</option>
	        <option value="C40">电气机械及器材制造业</option>
	        <option value="C41">电子及通信设备制造业</option>
	        <option value="C42">仪器仪表及文化、办公用机械制造业</option>
	        <option value="C43">其他制造业</option>
	    </optgroup>
	    <optgroup label="电力、煤气及水的生产和供应业">
			<option value="D44">电力、蒸气、水的生产和供应业</option>
	    	<option value="D45">煤气生产和供应业</option>
	    	<option value="D46">自来水的生产和供应业</option>
	    </optgroup>
	    <optgroup label="建筑业">
			<option value="E47">土木工程建筑业</option>
	    	<option value="E48">线路、管道和设备安装业</option>
	    	<option value="E49">装修装饰业</option>
	    </optgroup>
	    <optgroup label="地质勘查业、水利管理业">
			<option value="F50">地质勘查业</option>
	    	<option value="F51">水利管理业</option>
	    </optgroup>
	    <optgroup label="交通运输业、仓储及邮电通信业">
			<option value="G52">铁路运输业</option>
	    	<option value="G53">公路运输业</option>
	    	<option value="G54">管道运输业</option>
	    	<option value="G55">水上运输业</option>
	        <option value="G56">航空运输业</option>
	        <option value="G57">交通运输辅助业</option>
	        <option value="G58">其他交通运输业</option>
	        <option value="G59">仓储业</option>
	        <option value="G60">邮电通信业</option>
	    </optgroup>
	    <optgroup label="批发和零售贸易、餐饮业">
	        <option value="H61">仪器、饮料、烟草和家庭日用品批发业</option>
	        <option value="H62">能源、材料和机械电子设备批发业</option>
	        <option value="H63">其他批发业</option>
	        <option value="H64">零售业</option>
	        <option value="H65">商业经纪与代理业</option>
	        <option value="H67">餐饮业</option>
	    </optgroup>
	    <optgroup label="金融、保险业">
	        <option value="I68">金融业</option>
	        <option value="I70">保险业</option>
	    </optgroup>
	    <optgroup label="房地产业">
	        <option value="J72">房地产开发与经营业</option>
	        <option value="J73">房地产管理业</option>
	        <option value="J74">房地产经纪与代理业</option>
	    </optgroup>
	    <optgroup label="社会服务业">
	        <option value="K75">公共设施服务业</option>
	        <option value="K76">居民服务业</option>
	        <option value="K78">旅馆业</option>
	        <option value="K79">租赁服务业</option>
	        <option value="K80">旅游业</option>
	        <option value="K81">娱乐服务业</option>
	        <option value="K82">信息、咨询服务业</option>
	        <option value="K83">计算机应用服务业</option>
	        <option value="K84">其他社会服务业</option>
	    </optgroup>
	    <optgroup label="卫生、体育和社会福利业">
	        <option value="L85">卫生</option>
	        <option value="L86">体育</option>
	        <option value="L87">社会福利保障业</option>
	    </optgroup>
	    <optgroup label="教育、文化艺术及广播电影电视业">
	        <option value="M89">教育</option>
	        <option value="M90">文化艺术业</option>
	        <option value="M91">广播电影电视业</option>
	    </optgroup>
	    <optgroup label="科学研究和综合技术服务业">
	        <option value="N92">科学研究业</option>
	        <option value="N93">综合技术服务业（含气象、地震、测绘等）</option>
	    </optgroup>
	    <optgroup label="国家机关、政党机关和社会团体">
	        <option value="O94">国家机关</option>
	        <option value="O95">政党机关</option>
	        <option value="O96">社会团体</option>
        	<option value="O97">基层群众自治组织</option>
    	</optgroup>
	    <optgroup label="其他行业">
       	 <option value="P99">其他行业</option>
    	</optgroup>  
	</select>
    </td>
    </tr>
    <tr>
    <td align="justify">开始日期：<br/><input type="date" name="start_date"/></td>
	<td align="justify">完成日期：<br/><input type="date" name="finish_date"/></td>
	<td align="justify">项目技术来源：<br/>
<?php
	$tech_resource_list = array('自主开发','产学研联合攻关','省内其他单位技术','引进省外、国外技术消化创新','专利技术产业化');
	draw_pd_reset('tech_resource',$tech_resource_list);	

?>
</td>
     <td align="justify">项目计划类别：<br/><select name="project_class" id="project_class_id">
    			<option value="" selected="selected"></option>
    			<option value="key_project">重点科技计划项目</option>
    			<option value="general_project">一般科技计划项</option>
    			</select></td>
    </tr>
    </table>
</fieldset>

<fieldset id='gailan'>
	<legend>项目概览</legend>
    <fieldset>
    <legend>项目经费预算（万元）</legend>
    总计：<br/><input type="text" name="total_expenditure"/><br/>
    <br/>
    其中包括：<br/>
    <table cellpadding="0px" >
    <tr>
    <td align="justify">自筹：<br/><input type="text" name="zichou"/></td>
    <td align="justify">银行贷款：<br/><input type="text" name="yinhangdaikuan"/></td>
    <td align="justify">向县财政申请：<br/><input type="text" name="caizhengshenqing"/></td>
    <td align="justify">其他：<br/><input type="text" name="other_expenditure"/></td>
    </tr>
    </table>
	</fieldset>

    <fieldset>
    <legend>项目经费开支预算（万元）</legend>
    <table cellpadding="0px" >
    <tr>
    <td align="justify">设备费：<br/><input type="text" name="shebeifei"/></td>
    <td align="justify">材料费：<br/><input type="text" name="cailiaofei"/></td>
    <td align="justify">试验化验加工费：<br/><input type="text" name="shiyanhuayanjiagongfei"/></td>
    <td align="justify">燃料动力费：<br/><input type="text" name="ranliaodonglifei"/></td>
    </tr>
    <tr>
    <td align="justify">差旅费：<br/><input type="text" name="chailvfei"/></td>
    <td align="justify">人员劳务费：<br/><input type="text" name="renyuanlaowufei"/></td>
    <td align="justify">合作、协作研究与交流费：<br/><input type="text" name="hezuojiaoliufei"/></td>
    <td align="justify">出版/文献/信息传播知识产权事务费：<br/><input type="text" name="zhishichanquanshiwufei"/></td>
    </tr>
    <tr>
    <td align="justify">会议费：<br/><input type="text" name="huiyifei"/></td>
    <td align="justify">管理费：<br/><input type="text" name="guanlifei"/></td>
    <td align="justify">专家咨询费：<br/><input type="text" name="zhuanjiazixunfei"/></td>
    <td align="justify">其他开支：<br/><input type="text" name="other_spending"/></td>
    </tr>
    </table>
  	</fieldset>

  	<fieldset>
    <legend>预计经济效益</legend>
    <table cellpadding="0px" border="0">
    <tr>
    <td align="justify">年增产值（万元）：<br/><input type="text" name="nianzengchanzhi"/></td>
    <td align="justify">年增利润（万元）：<br/><input type="text" name="nianzenglirun"/></td>
    <td align="justify">年增税金（万元）：<br/><input type="text" name="nianzengshuijin"/></td>
    </tr>
    <tr>
    <td align="justify">年创汇（万美元）：<br/><input type="text" name="nianchuanghui"/></td>
    <td align="justify">年节汇（万美元）：<br/><input type="text" name="nianjiehui"/></td>
    </tr>
    </table>
  	</fieldset>

  	<fieldset>
    <legend>预计其他成果</legend>
    <table cellpadding="0px" border="0">
    <tr>
    <td align="justify">论文数：<br/><input type="text" name="lunwenshu"/></td>
    <td align="justify">专利：<br/><input type="text" name="zhuanli"/></td>
    <td align="justify">其中发明专利：<br/><input type="text" name="famingzhuanli"/></td>
    </tr>
    </table>
  	</fieldset>
  	备注：<br/><textarea name="beizhu" cols="80" rows="5"></textarea>
</fieldset>

<fieldset id="chengdandanwei">
    <legend>二、承担单位</legend>
	<fieldset>
	<legend>第一申请单位</legend>
	<table cellpadding="6px" border="0">
    <tr>
    <td align="justify">单位名称：<br/><input type="text" name="danweimingcheng"/></td>
    <td align="justify">单位简称：<br/><input type="text" name="danweijiancheng"/></td>
    <td align="justify">法人代表：<br/><input type="text" name="farendaibiao"/></td>
    <td align="justify">所在地代码：<br/><input type="text" name="suozaididaima"/></td>
    </tr>
    <tr>
    <td align="justify">单位类型：<br/><input type="text" name="danweileixing"/></td>
    <td align="justify">详细地址：<br/><input type="text" name="xiangxidizhi"/></td>
    <td align="justify">邮政编码：<br/><input type="text" name="youzhengbianma"/></td>
    <td align="justify">单位EMAIL：<br/><input type="text" name="danweiemail"/></td>
    </tr>
    <tr>
    <td align="justify">联系人：<br/><input type="text" name="lianxiren"/></td>
    <td align="justify">联系电话：<br/><input type="text" name="lianxidianhua"/></td>
    <td align="justify">传真：<br/><input type="text" name="chuanzhen"/></td>
    <td align="justify">开户银行：<br/><input type="text" name="kaihuyinhang"/></td>
    </tr>
    <tr>
    <td align="justify">银行帐号：<br/><input type="text" name="yinhangzhanghao"/></td>
    <td align="justify">主管部门：<br/><input type="text" name="zhuguanbumen"/></td>
    </tr>
	</table>
	</fieldset>
	<fieldset>
	<legend>其他合作单位</legend>
    合作单位总数 △：（包含第一申请单位）<br>
    <input type="text" id="hezuo" name="hezuodanweizongshu" onBlur="define_unitNum()"/><br/>
	承担单位数 <input type="text" name="chengdandanweishu"/>
	参加单位数 <input type="text" name="canjiadanweishu"/>

	<table border="0" id="unit">
	<tr>
	<th>&nbsp;</th><th>单位名称</th><th>法人代表</th><th>职责*(*: 0-承担，1－参加)</th>
	</tr>
    <tr>
	<td>1.</td>
	<td><input type="text" name="array_unit_danweimingcheng[0]"/></td>
	<td><input type="text" name="array_unit_farendaibiao[0]"/></td>
	<td><input type="text" name="array_unit_zhize[0]"/></td>
	</tr>
	</table>
	<br />
	</fieldset>
</fieldset>

<fieldset id="xiangmuchengyuan">
	<legend>三、项目负责人及项目组成员</legend>
	<fieldset>
	<legend>项目负责人</legend>
	<table cellpadding="6px" border="0">
    <tr>
    <td align="justify">姓名<br/><input type="text" name="fuzeren_name"/></td>
    <td align="justify">身份证号码<br/><input type="text" name="fuzeren_shenfenzheng"/></td>
    <td align="justify">联系电话<br/><input type="text" name="fuzeren_phone"/></td>
    <td align="justify">EMAIL<br/><input type="text" name="fuzeren_email"/></td>
	</tr>
	<tr>
    <td align="justify">学历<br/><input type="text" name="fuzeren_xueli"/></td>
    <td align="justify">学位<br/><input type="text" name="fuzeren_xuewei"/></td>
    <td align="justify">专业技术职务<br/><input type="text" name="fuzeren_zhiwu"/></td>
    <td align="justify">专业<br/><input type="text" name="fuzeren_zhuanye"/></td>
	</tr>
	<tr>
    <td align="justify">在本项目中的分工<br/><input type="text" name="fuzeren_fengong"/></td>
    <td align="justify">工作单位<br/><input type="text" name="fuzeren_danwei"/></td>
	</tr>
	</table>
	</fieldset>

	<fieldset class="wideForm">
	<legend>项目组成员</legend>
    总共项目组人数：<input type="text" id="member_num" name="member_number"/>
	<table border="0" id="member">
	<tr>
	<th>&nbsp;</th><th>姓名</th><th>出生年月</th><th>专业技术职务</th><th>专业</th><th>工作单位</th><th>在本项目中分工</th>
	</tr>
	<tr>
	<td>1.</td>
	<td><input type="text" name="array_member_name[0]" style="width:50px"/></td>
	<td><input type="text" name="array_member_chushengnianyue[0]" style="width:80px" /></td>
	<td><input type="text" name="array_member_zhiwu[0]"/></td>
	<td><input type="text" name="array_member_zhuanye[0]" style="width:80px" /></td>
	<td><input type="text" name="array_member_gongzuodanwei[0]"/></td>
	<td><input type="text" name="array_member_fengong[0]"/></td>
	</tr>
	<tr>
	</table>
	</fieldset>
</fieldset>

<fieldset id="litiyiju">
<legend>
四、本项目的立题依据：包括目的、意义、国内外概况和发展趋势，现有工作基础和条件（包括研究工作基础、装备条件和技术力量及项目负责人技术工作简历）
</legend>
<textarea class="bigText" type="text" name="litiyiju" cols="100" rows="40"></textarea>
</fieldset>

<fieldset id="kaifaneirong">
<legend>
五、研究、开发内容和预期成果：具体研究、开发内容和重点解决的技术关键问题，要达到的主要技术、经济指标及经济社会环境效益，拟采取的研究方法和技术路线或工艺流程（可用框图表示）
</legend>
<textarea class="bigText" type="text" name="neirongheyuqichengguo" cols="100" rows="40"></textarea>
</fieldset>

<fieldset id="jihuajindu">
<legend>六、计划进度目标</legend>
<table id="progress" cellspacing="0" border="1">
<tr>
<th>起始年月</th><th>进度目标要求（每栏限80字）</th>
</tr>
<?php
	$n=6;
	for($i=0;$i<=$n;$i++){
		echo "<tr id='tr_$i'><td>
			<input type='date' name='array_progress_start[$i]'/>至<input type='date' name='array_progress_end[$i]'/></td>
			<td><textarea name='array_progress_content[$i]' cols='50' rows='2'></textarea></td>
			</tr>";
	}
?>
</table>
<button type='button' id="progressinc">增加项目进度栏</button>
<button type='button' id="progressdec">减少项目进度栏</button>
<br>
<br>
</fieldset>

<fieldset id="shichangziyuan">
<legend>七、市场资源</legend>
<textarea class="bigText" type="text" name="shichangziyuan" cols="100" rows="40"></textarea>
</fieldset>

<fieldset id="shenqingjingfei">
<legend>八、项目申请经费：（计算根据及理由）</legend>
<textarea class="bigText" type="text" name="shenqingjingfei" cols="100" rows="40"></textarea>
</fieldset>
</form>
<div id="feasibleDiv">
</div>
<center><input type="button" onClick="history.go(-1)" value="返回"></center>
</div>
</html>
