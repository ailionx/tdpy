<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$id = $_SESSION[id];
	$query = "select company_name,industry_class,company_address,website,email,contact,contact_phone,fax from company_info where id=$id";
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	//不同用户进入此页面的权限
	permissionBlocker('apply','');
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="../js/my_javascript.js"></script>
<script type="text/javascript">
mustFillArr = {'company_name':'企业名称','industry_class':'行业','company_address':'通讯地址','contact_name':'联系人','contact_phone':'电话','post_code':'邮编','plan_fund':'拟提供资金','allow_publish':'是否同意网上技术市场发布','project_name':'技术难题名称','content':'主要内容和技术经济指标','key_words':'关键词','start_time':'起始时间','finish_time':'截止时间'}
function checkCompanyForm(){
	return checkForm(mustFillArr);
}
</script>
<title>技术难题</title>
</head>

<h1><center>“企业难题招标、技术需求”工作单</center></h1>
<form method="post" action="../proc/submit_form.php" name="tech_challenge_form" onSubmit="return checkCompanyForm()"> 
<input type="hidden" value="apply" name="apply_edit">
<input type="hidden" value="tech_challenge" name="submit_type">
<fieldset style="font-size:12px">
<table border="0" style="font-size:12px">
  <tr>
    <td>企业名称：<br><input type="text" name="company_name" value="<?php echo $row[company_name] ?>" readonly="readonly"></td>
    <td>行业：<br><input type="text" name="industry_class" value=<?php echo $row[industry_class] ?> ></td>
    <td>通讯地址：<br><input type="text" name="company_address" value=<?php echo $row[company_address] ?> ></td>
    <td>传真：<br><input type="text" name="fax" value=<?php echo $row[fax] ?> ></td>
  </tr>
  <tr>
    <td>网址：<br><input type="text" name="website" value=<?php echo $row[website] ?> ></td>
    <td>E-mail：<br><input type="email" name="email" value=<?php echo $row[email] ?> ></td>
    <td>联系人：<br><input type="text" name="contact_name" value=<?php echo $row[contact] ?> ></td>
    <td>电话：<br><input type="text" name="contact_phone" value=<?php echo $row[contact_phone] ?> ></td>
  </tr>
  <tr>
    <td>邮编：<br><input type="text" name="post_code"></td>
    <td>拟提供资金：<br><input type="text" name="plan_fund"></td>
    <td>是否同意网上技术市场发布：<br>
<?php 
	$allow_publish_list = array('是','否');
	draw_pd_set('allow_publish',$allow_publish_list,'是');	
?>
	</td>
  </tr>
  <tr>  
    <td colspan="6">技术难题名称
（限100字内）<br /><textarea cols="60px" rows="10px" name="project_name"></textarea></td>
  </tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
	<legend>主要内容和技术经济指标（限2000字内）</legend>
	<textarea cols="60px" rows="20px" name="content" maxlength="2000"></textarea>
</fieldset>
<fieldset style="font-size:12px">
	<legend>备    注（限500字内）</legend>
	<textarea cols="60px" rows="8px" name="comment" maxlength="500"></textarea>
</fieldset>
<fieldset style="font-size:12px">
	<legend>关 键 词（限100字内）</legend>
	<textarea cols="60px" rows="2px" name="key_words" maxlength="100" placeholder="1、     　2、     　3、     　4、     　5、     "></textarea>
</fieldset>
<fieldset style="font-size:12px">
<table border="0" style="font-size:12px">
  <tr>
    <td>起始时间</td>
    <td><input type="date" name="start_time"></td>
    <td>截止时间</td>
    <td><input type="date" name="finish_time"></td>
  </tr>
</table>
</fieldset>
<h3><input style="font-size:14px" type="submit" name="submit" value="提交"></h3>
</form>
<center><input type="button" onClick="history.go(-1)" value="返回"></center>
</html>
