<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
		$user = $_SESSION[user];
	if ($_GET['project_id']){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from tech_challenge where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		//不同用户进入此页面的权限
		permissionBlocker('edit',$value[status]);
		//反序列化数组存入的数据
		$arrayLong_list = array_field_inDB('tech_challenge');
		foreach ($arrayLong_list as $arrayLong) {
			${$arrayLong} = unserialize($value[$arrayLong]);
		}	
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
?>
<!DOCTYPE HTML5>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<title>技术难题合同签订</title>
<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../js/my_javascript.js"></script>
<script type="text/javascript" src="../js/tech_contract.js"></script>
<script type="text/javascript">
mustFillArr = {'valid_start':'合同有效日期','valid_end':'合同有效日期','contract_code':'合同编号','project_name':'项目名称','signed_data':'合同签订日期','contract_class':'合同类别','plan_class':'计划类别','financal_target':'社会经济目标','buyer_name':'买方名称','manager_name':'负责人','buyer_class':'买方类别','seller_name':'卖方名称','seller_area':'卖方地区','seller_class':'卖方类别','seller_adress':'卖方通讯地址','seller_zcode':'卖方邮政编码','seller_phone':'卖方联系电话','transaction_amount':'合同成交金额','tech_amount':'其中:技术交易额','additional_invest':'预计新增投入','aditional_output':'预计新增产值'}
function checkCompanyForm(){
	return checkForm(mustFillArr);
}
</script>
<style type="text/css">
ul {
	list-style:none;
}
.form_area {
	width:600px;
	margin:27px auto;
}

select {
	width:155px;
}
</style>
</head>
<form method="post" action="../proc/submit_form.php?project_id=<?php echo $project_id; ?>" name="projectForm"  onSubmit="return checkCompanyForm()">
<input type="hidden" value="apply" name="apply_edit" >
<input type="hidden" value="tech_contract" name="submit_type" >
<div class="form_area">
<fieldset id="jibenxiangmu">
    <legend>一、项目基本情况</legend>
    <table cellpadding="0px">
    <tr>
    <td colspan=2>合同有效日期：<br><input type="text" name="valid_start" />至<input type="text" name="valid_end" /></td>
	</tr><tr>
	<td>合同编号：<br/><input type="text" name="contract_code" /></td>
	<td>项目名称：<br/><input type="text" name="project_name" /></td>
	</tr><tr>
	<td>合同签订日期：<br/><input type="text" name="signed_data" /></td>
	<td>合同类别：<br/>
	<SELECT NAME="contract_class">
		<OPTION SELECTED VALUE="">请选择类别
		<OPTION VALUE="技术开发合同">技术开发合同
		<OPTION VALUE="技术转让合同">技术转让合同
		<OPTION VALUE="技术咨询合同">技术咨询合同
		<OPTION VALUE="技术服务合同">技术服务合同
		</SELECT>
		</td>
	</tr><tr>
	<td>计划类别：<br/><SELECT NAME="plan_class">
		<OPTION SELECTED VALUE="">请选择类别
		<OPTION VALUE="国家，部门计划">国家，部门计划
		<OPTION VALUE="省，自治区，直辖市及计划单列市计划">省，自治区，直辖市及计划单列市计划
		<OPTION VALUE="地，市县计划">地，市县计划
		<OPTION VALUE="计划外">计划外
		</SELECT></td>
	<td>社会经济目标：<br/>
	<SELECT NAME="financal_target">
		<OPTION SELECTED VALUE="">请选择类别
		<OPTION VALUE="陆地，海洋和大气的开发与评估">陆地，海洋和大气的开发与评估
		<OPTION VALUE="民用宇宙空间">民用宇宙空间
		<OPTION VALUE="农业，林业和渔业的发展">农业，林业和渔业的发展
		<OPTION VALUE="促进工业的发展">促进工业的发展
		<OPTION VALUE="能源的生产，储存和分配">能源的生产，储存和分配
		<OPTION VALUE="交通，通讯事业的发展">交通，通讯事业的发展
		<OPTION VALUE="教育事业的发展">教育事业的发展
		<OPTION VALUE="卫生事业的发展">卫生事业的发展
		<OPTION VALUE="社会发展和社会经济服务">社会发展和社会经济服务
		<OPTION VALUE="环境保护">环境保护
		<OPTION VALUE="知识的全面发展">知识的全面发展
		<OPTION VALUE="其它的民用目标">其它的民用目标
		<OPTION VALUE="国防">国防
		</SELECT></td>
	</tr><tr>
	<td>买方名称：<br/><input type="text" name="buyer_name" /></td>
	<td>负责人：<br/><input type="text" name="manager_name" /></td>
	<td>买方类别：<br/><SELECT NAME="buyer_class">
		<OPTION SELECTED VALUE="">请选择类别
		<OPTION VALUE="国有企业">国有企业
		<OPTION VALUE="集体企业">集体企业
		<OPTION VALUE="私营企业">私营企业
		<OPTION VALUE="有限责任公司">有限责任公司
		<OPTION VALUE="股份有限公司">股份有限公司
		<OPTION VALUE="港澳台商投资公司">港澳台商投资公司
		<OPTION VALUE="外商投资公司">外商投资公司
		<OPTION VALUE="科研机构">科研机构
		<OPTION VALUE="各级管理部门">各级管理部门
		<OPTION VALUE="技术贸易机构">技术贸易机构
		<OPTION VALUE="个体经营">个体经营
		<OPTION VALUE="其他">其他
		</SELECT></td>
	</tr><tr>
	<td>卖方名称：<br/><input type="text" name="seller_name" /></td>
	<td>卖方地区：<br/><input type="text" name="seller_area" /></td>
	<td>卖方类别：<br/>
	<SELECT NAME="seller_class">
		<OPTION SELECTED VALUE="">请选择类别
		<OPTION VALUE="科研机构">科研机构
		<OPTION VALUE="大中专院校">大中专院校
		<OPTION VALUE="企业">企业
		<OPTION VALUE="技术贸易机构">技术贸易机构
		<OPTION VALUE="个体经营">个体经营
		<OPTION VALUE="其他">其他
		</SELECT></td>
	</tr><tr>
	<td>卖方通讯地址：<br/><input type="text" name="seller_adress" /></td>
	<td>卖方邮政编码：<br/><input type="text" name="seller_zcode" /></td>
	<td>卖方联系电话：<br/><input type="text" name="seller_phone" /></td>
	</tr><tr>
	<td>合同成交金额：<br/><input type="text" name="transaction_amount" /></td>
	<td>其中:技术交易额：<br/><input type="text" name="tech_amount" /></td>
	</tr><tr>
	<td>预计新增投入：<br/><input type="text" name="additional_invest" /></td>
	<td>预计新增产值：<br/><input type="text" name="aditional_output" /></td>
	</tr>
	</table>
	<br>
请选择合同公开项(注意：如果合同有公开项，则合同名称和编号将自动公开)<br> 
<ul class="check">
<?php
	$array_public_item = array('name_code_p'=>'合同名称和编号',
								'party_name_p'=>'当事人名称',
								'signed_info_p'=>'签订的时间地点和有效期',
								'class_target_p'=>'合同类别和计划类别和社会经济目标',
								'contract_amount_p'=>'合同金额',
								);
	foreach ($array_public_item as $public_item_key=>$public_item) {
/*		if ($_GET[array_public_item][$public_item]==$public_item) {
			echo "<li><input type='checkbox' name=array_public_item[$public_item_key] value=$public_item checked>$public_item</li>";
		} else {*/
			echo "<li><input type='checkbox' name=array_public_item[$public_item_key] value=$public_item>$public_item</li>";
//		}
	}	

?>

</ul>

</fieldset>


</form>
<center>
<bold align="center"><input style="font-size:14px;color:#30F" type="submit" value="保存"></bold>
<input type="button" onClick="history.go(-1)" value="返回"></center>
</div>
</html>
