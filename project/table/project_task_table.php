<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
		$user = $_SESSION[user];
	if ($_GET['project_id']){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from project_application where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		//不同用户进入此页面的权限
		permissionBlocker('edit',$value[status]);
		
		//反序列化数组存入的数据
		$arrayLong_list = array_field_inDB('project_task');
		foreach ($arrayLong_list as $arrayLong) {
			${$arrayLong} = unserialize($value[$arrayLong]);
		}
		$array_num_unit = count($array_unit_danweimingcheng);
		
		
		//控制下拉表单的预设值		
		
		$tech_resource_list = array('independent_develop','cooperation_develop','chanxueyan_cooperation','internal_cooperation','foreign_technology');
		foreach ($tech_resource_list as $cla) {
			if ($value[tech_resource] == $cla) {
				${$cla} = $cla." selected";
			} else {
				${$cla} = $cla;
			}
		}
		
		$innovation_format_list = array('self_innovation','integrate_innovation','re_innovation','absorb_innovation','tech_transform');
		foreach ($innovation_format_list as $cla) {
			if ($value[innovation_format] == $cla) {
				${$cla} = $cla." selected";
			} else {
				${$cla} = $cla;
			}
		}
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
?>
<!DOCTYPE HTML5>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<title>任务书申请</title>
<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../js/my_javascript.js"></script>
<script type="text/javascript" src="../js/project_task.js"></script>
<script type="text/javascript">
mustFillArr = {'project_name':'项目名称','manager_domain':'项目管理领域','tech_resource':'项目技术来源','innovation_format':'技术创新方式','start_date':'开始日期','finish_date':'完成日期','xinzengchanzhi':'新增产值（万元）','xinzenglishui':'新增利税（万元）','zhuanlishenqing':'专利申请数','zhuanlishouquan':'专利授权数','famingzhuanlishenqing':'其中发明专利申请数','famingzhuanlishouquan':'其中发明专利授权数','fabiaolunwen':'发表论文','rencaiyinjin':'人才引进和培养','danweimingcheng':'单位名称','farendaima':'法人代码','xiangxidizhi':'详细地址','youzhengbianma':'邮政编码','diyidanweilianxiren':'联系人','diyidanweiemail':'EMAIL','dianhuachuanzhen':'电话/传真','shouji':'手机','hezuodanweizongshu':'合作单位总数','array_unit_danweimingcheng[0]':'单位名称','array_unit_farendaima[0]':'法人代码','array_unit_zhize[0]':'职责','fuzeren_name':'项目负责人姓名','fuzeren_shenfenzheng':'项目负责人身份证号码','fuzeren_danwei':'项目负责人工作单位','fuzeren_farendaima':'项目负责人法人代码','fuzeren_adress':'项目负责人详细地址','fuzeren_zcode':'项目负责人邮政编码','fuzeren_mobile':'项目负责人移动电话','fuzeren_email':'项目负责人EMAIL','fuzeren_xueli':'项目负责人学历','fuzeren_xuewei':'项目负责人学位','fuzeren_zhicheng':'项目负责人职称','fuzeren_zhuanye':'项目负责人现从事专业','zhibiao':'项目研发主要任务和关键技术经济指标','chengguoxingshi':'项目成果提供形式','zongjingfei':'本项目研发总经费','jiafangbuzhu':'甲方补助','yifangzichou':'乙方自筹','array_jingfei_yusuan[0]':'设备费','array_jingfei_caizheng[0]':'设备费','array_jingfei_yusuan[1]':'材料费','array_jingfei_caizheng[1]':'材料费','array_jingfei_yusuan[2]':'测试化验加工费','array_jingfei_caizheng[2]':'测试化验加工费','array_jingfei_yusuan[3]':'燃料动力费','array_jingfei_caizheng[3]':'燃料动力费','array_jingfei_yusuan[4]':'差旅费','array_jingfei_caizheng[4]':'差旅费','array_jingfei_yusuan[5]':'会议费','array_jingfei_caizheng[5]':'会议费','array_jingfei_yusuan[6]':'合作、协作研究与交流费','array_jingfei_caizheng[6]':'合作、协作研究与交流费','array_jingfei_yusuan[7]':'出版/文献/信息传播/知识产权事务费','array_jingfei_caizheng[7]':'出版/文献/信息传播/知识产权事务费','array_jingfei_yusuan[8]':'人员劳务费','array_jingfei_yusuan[9]':'人员劳务','array_jingfei_caizheng[9]':'专家咨询费','array_jingfei_yusuan[10]':'验收检查费','array_jingfei_caizheng[10]':'验收检查费','array_jingfei_yusuan[11]':'管理费','array_jingfei_caizheng[11]':'管理费','array_jingfei_yusuan[12]':'其他费用','array_jingfei_caizheng[12]':'其他费用','array_yiqi_name[0]':'需增添的仪器设备','array_yiqi_num[0]':'需增添的仪器设备','array_yiqi_price[0]':'需增添的仪器设备','array_yiqi_bokuan[0]':'需增添的仪器设备','array_yiqi_zichou[0]':'需增添的仪器设备','array_yiqi_yongtu[0]':'需增添的仪器设备'}
function checkCompanyForm(){
	return checkForm(mustFillArr);
}
</script>
<style type="text/css">
.content ul, .content ol { 
	padding: 0 15px 15px 40px; /* 此填充反映上述标题和段落规则中的右填充。填充放置于下方可用于间隔列表中其它元素，置于左侧可用于创建缩进。您可以根据需要进行调整。 */
}

/* ~~ 导航列表样式（如果选择使用预先创建的 Spry 等弹出菜单，则可以删除此样式） ~~ */
ul.nav_menu {
	position:fixed;
	left:10px;
	top:10px;
	float:left;
	list-style: none; /* 这将删除列表标记 */
	border-top: 1px solid #666; /* 这将为链接创建上边框 – 使用下边框将所有其它项放置在 LI 中 */
	margin-bottom: 15px; /* 这将在下面内容的导航之间创建间距 */
	margin-left:-30px;
	margin-right:10px;
}
ul.nav_menu li {
	border-bottom: 1px solid #666; /* 这将创建按钮间隔 */
	font-family:"宋体";
	font-size:14px;
	line-height:20px;
	text-align:center;
}
ul.nav_menu a, ul.nav_menu a:visited { /* 对这些选择器进行分组可确保链接即使在访问之后也能保持其按钮外观 */
	display: block; /* 这将为链接赋予块属性，使其填满包含它的整个 LI。这样，整个区域都可以响应鼠标单击操作。 */
	width: 110px;  /*此宽度使整个按钮在 IE6 中可单击。如果您不需要支持 IE6，可以删除它。请用侧栏容器的宽度减去此链接的填充来计算正确的宽度。 */
	text-decoration: none;
	color:#000;
}

.form_area {
	position:fixed;
	left:132px;
	top:17px;
}

</style>
</head>
<form method="post" action="../proc/submit_form.php?project_id=<?php echo $project_id; ?>" name="projectForm"  onSubmit="return checkCompanyForm()">
<input type="hidden" value="apply" name="apply_edit" >
<input type="hidden" value="project_task" name="submit_type" >
<ul class="nav_menu">
<li><a href="#" onClick="fr_hide('jibenxiangmu')">项目基本情况</button></a></li>
<li><a href="#" onClick="fr_hide('chengdandanwei')">承担单位</button></a></li>
<li><a href="#" onClick="fr_hide('xiangmuchengyuan')">负责人及成员</button></a></li>
<li><a href="#" onClick="fr_hide('zhibiao')">技术经济指标</button></a></li>
<li><a href="#" onClick="fr_hide('chengguoxingshi')">成果提供形式</button></a></li>
<li><a href="#" onClick="fr_hide('jingfeilaiyuan')">经费来源与预算</button></a></li>
<li><a href="#" onClick="fr_hide('yiqishebei')">需增添仪器设备</button></a></li>
<li><bold align="center"><input style="font-size:14px;color:#30F" type="submit" name="submit" value="保存"></bold></li>
</ul>
<div class="form_area">
<fieldset id="jibenxiangmu">
    <legend>一、项目基本情况</legend>
    项目名称: <br/><input type="text" name="project_name" value="<?php echo $value[project_name]; ?>" id='must[0]' onBlur="check_project_exist(document.projectForm.project_name.value)"/><span></span>
    <table cellpadding="0px">
    <tr>
    <td>项目管理领域：<br><input type="text" name="manager_domain" value="<?php echo $value[manager_domain]; ?>" /></td>
    </tr>
	<td align="justify">项目技术来源：<br/><select name="tech_resource" >
    			<option value="" ></option>
    			<option value=<?php echo $independent_develop; ?> >自主开发</option>
                <option value=<?php echo $cooperation_develop; ?> >合作开发</option>
    			<option value=<?php echo $chanxueyan_cooperation; ?> >产学研联合开发</option>
    			<option value=<?php echo $internal_cooperation; ?> >引进国内技术</option>
    			<option value=<?php echo $foreign_technology; ?> >引进国外技术</option>
    			</select></td>
	<td align="justify">技术创新方式：<br/><select name="innovation_format" >
    			<option value=""></option>
    			<option value=<?php echo $self_innovation; ?> >自主创新</option>
    			<option value=<?php echo $integrate_innovation; ?> >集成创新</option>
    			<option value=<?php echo $re_innovation; ?> >在引进、消化吸收基础上的再创新</option>
    			<option value=<?php echo $tech_transform; ?> >科技成果转化和产业化</option>
    			</select></td>
    <tr>
	<td align="justify">开始日期：<br/><input type="date" name="start_date" value="<?php echo $value[start_date]; ?>" /></td>
	<td align="justify">完成日期：<br/><input type="date" name="finish_date" value="<?php echo $value[finish_date]; ?>" /></td>
	</tr>
    </table>
    
	<fieldset>
    <legend>项目成果</legend>
    <table cellpadding="0px" border="0">
    <tr>
    </tr><tr>
    <td align="justify">新增产值（万元）：<br/><input type="text" name="xinzengchanzhi" value="<?php echo $value[xinzengchanzhi]; ?>" /></td>
    <td align="justify">新增利税（万元）：<br/><input type="text" name="xinzenglishui" value="<?php echo $value[xinzenglishui]; ?>" /></td>
    </tr><tr>
    <td align="justify">专利申请数：<br/><input type="text" name="zhuanlishenqing" value="<?php echo $value[zhuanlishenqing]; ?>" /></td>
    <td align="justify">专利授权数：<br/><input type="text" name="zhuanlishouquan" value="<?php echo $value[zhuanlishouquan]; ?>" /></td>
    </tr><tr>
    <td align="justify">其中发明专利申请数：<br/><input type="text" name="famingzhuanlishenqing" value="<?php echo $value[famingzhuanlishenqing]; ?>" /></td>
    <td align="justify">其中发明专利授权数：<br/><input type="text" name="famingzhuanlishouquan" value="<?php echo $value[famingzhuanlishouquan]; ?>" /></td>
    </tr><tr>
    <td align="justify">发表论文：<br/><input type="text" name="fabiaolunwen" value="<?php echo $value[fabiaolunwen]; ?>" /></td>
    <td align="justify">人才引进和培养：<br/><input type="text" name="rencaiyinjin" value="<?php echo $value[rencaiyinjin]; ?>" /></td>
    </tr>
    </table>
  	</fieldset>
</fieldset>

<fieldset id="chengdandanwei">
    <fieldset>
	<legend>承担单位</legend>
	<table cellpadding="6px" border="0">
    <tr>
    <td align="justify">单位名称：<br/><input type="text" name="danweimingcheng" value="<?php echo $value[danweimingcheng]; ?>" /></td>
    <td align="justify">法人代码：<br/><input type="text" name="farendaima" value="<?php echo $value[farendaima]; ?>" /></td>
	</tr>
    <tr>
    <td align="justify">详细地址：<br/><input type="text" name="xiangxidizhi" value="<?php echo $value[xiangxidizhi]; ?>" /></td>
    <td align="justify">邮政编码：<br/><input type="text" name="youzhengbianma" value="<?php echo $value[youzhengbianma]; ?>" /></td>
    <td align="justify">联系人：<br/><input type="text" name="diyidanweilianxiren" value="<?php echo $value[diyidanweilianxiren]; ?>" /></td>
    </tr>
    <tr>
    <td align="justify">EMAIL：<br/><input type="text" name="diyidanweiemail" value="<?php echo $value[diyidanweiemail]; ?>" /></td>
    <td align="justify">电话/传真：<br/><input type="text" name="dianhuachuanzhen" value="<?php echo $value[dianhuachuanzhen]; ?>" /></td>
    <td align="justify">手机：<br/><input type="text" name="shouji" value="<?php echo $value[shouji]; ?>" /></td>
    </tr>
	</table>    
	</fieldset>
	<fieldset>
	<legend>合作单位</legend>
    合作单位总数<br>
    <input type="text" id="hezuo" name="hezuodanweizongshu" value="<?php echo $value[hezuodanweizongshu]; ?>"  onBlur="define_unitNum()"/><br/>
	<table border="0" id="unit">
	<tr>
	<th>&nbsp;</th><th>单位名称</th><th>法人代码</th><th>职责</th>
	</tr>
    <?php 
	$hezuo_col = $value[hezuodanweizongshu];
	for ($i=0;$i<$hezuo_col;$i++){
		$ii=$i+1;
		echo "<tr>
		<td>$ii.</td>
		<td><input type='text' name=array_unit_danweimingcheng[$i] value=$array_unit_danweimingcheng[$i]></td>
		<td><input type='text' name=array_unit_farendaima[$i] value=$array_unit_farendaima[$i]></td>
		<td><input type='text' name=array_unit_zhize[$i] value=$array_unit_zhize[$i]></td>
		</tr>";
	}
	?>
	</table>
	<br />
	</fieldset>
</fieldset>

<fieldset id="xiangmuchengyuan">
	<legend>二、项目负责人及项目组成员</legend>
	<fieldset>
	<legend>项目负责人</legend>
	<table cellpadding="6px" border="0">
    <tr>
    <td align="justify">姓名<br/><input type="text" name="fuzeren_name" value="<?php echo $value[fuzeren_name]; ?>" /></td>
    <td align="justify">身份证号码<br/><input type="text" name="fuzeren_shenfenzheng" value="<?php echo $value[fuzeren_shenfenzheng]; ?>" /></td>
    <td align="justify">工作单位<br/><input type="text" name="fuzeren_danwei" value="<?php echo $value[fuzeren_danwei]; ?>" /></td>
    <td align="justify">法人代码<br/><input type="text" name="fuzeren_farendaima" value="<?php echo $value[fuzeren_farendaima]; ?>" /></td>
	</tr>
	<tr>
    <td align="justify">详细地址<br/><input type="text" name="fuzeren_adress" value="<?php echo $value[fuzeren_adress]; ?>" /></td>
    <td align="justify">邮政编码<br/><input type="text" name="fuzeren_zcode" value="<?php echo $value[fuzeren_zcode]; ?>" /></td>
    <td align="justify">移动电话<br/><input type="text" name="fuzeren_mobile" value="<?php echo $value[fuzeren_mobile]; ?>" /></td>
    <td align="justify">EMAIL<br/><input type="text" name="fuzeren_email" value="<?php echo $value[fuzeren_email]; ?>" /></td>
    </tr>
    <tr>
    <td align="justify">学历<br/><input type="text" name="fuzeren_xueli" value="<?php echo $value[fuzeren_xueli]; ?>" /></td>
    <td align="justify">学位<br/><input type="text" name="fuzeren_xuewei" value="<?php echo $value[fuzeren_xuewei]; ?>" /></td>
    <td align="justify">职称<br/><input type="text" name="fuzeren_zhicheng" value="<?php echo $value[fuzeren_zhicheng]; ?>" /></td>
    <td align="justify">现从事专业<br/><input type="text" name="fuzeren_zhuanye" value="<?php echo $value[fuzeren_zhuanye]; ?>" /></td>
	</tr>
	<tr>
	</table>
	</fieldset>

	<fieldset>
	<legend>项目组成员</legend>
    总共项目组人数：<input type="text" id="member_num" name="member_number" value="<?php echo $value[member_number]; ?>" />
	<table border="1" id="member" cellspacing="0">
	<tr>
	<th>&nbsp;</th><th>姓名</th><th>身份证号码</th><th>所在单位</th><th>职称</th><th>从事专业</th><th>在本项目中分工</th><th>年参加项目工作时间(月)</th>
	</tr>
    <?php 
	for ($i=0;$i<$value[member_number];$i++){
		$ii=$i+1;
		echo "<tr>
		<td>$ii.</td>
		<td><input type='text' name=array_member_name[$i] value=$array_member_name[$i]></td>
		<td><input type='text' name=array_member_shenfenzheng[$i] value=$array_member_shenfenzheng[$i]></td>
		<td><input type='text' name=array_member_gongzuodanwei[$i] value=$array_member_gongzuodanwei[$i]></td>
		<td><input type='text' name=array_member_zhicheng[$i] value=$array_member_zhicheng[$i]></td>
		<td><input type='text' name=array_member_zhuanye[$i] value=$array_member_zhuanye[$i]></td>
		<td><input type='text' name=array_member_fengong[$i] value=$array_member_fengong[$i]></td>
		<td><input type='text' name=array_member_canjiashijian[$i] value=$array_member_canjiashijian[$i]></td>
		</tr>";
	}
	?>
	</table>
	</fieldset>
</fieldset>

<fieldset id="zhibiao">
<legend>
三、项目研发主要任务和关键技术经济指标
</legend>
<textarea type="text" name="zhibiao" cols="100" rows="40"><?php echo $value[zhibiao]; ?></textarea>
</fieldset>

<fieldset id="chengguoxingshi">
<legend>
四、项目成果提供形式
</legend>
<textarea type="text" name="chengguoxingshi" cols="100" rows="40"><?php echo $value[chengguoxingshi]; ?></textarea>
</fieldset>

<fieldset id="jingfeilaiyuan">

	<legend>项目经费来源及预算</legend>
本项目研发总经费<input type="text" name="zongjingfei" value="<?php echo $value[zongjingfei]; ?>"  style="width:80px" />万元，其中:甲方补助<input type="text" name="jiafangbuzhu" value="<?php echo $value[jiafangbuzhu]; ?>"  style="width:80px" />万元，乙方自筹<input type="text" name="yifangzichou" value="<?php echo $value[yifangzichou]; ?>"  style="width:80px" />万元。<br>
<br>
<table border="1" cellspacing=0 cellpadding="8">
<tr>
<th>经费开支科目</th><th>预算经费总额(万元)</th><th>其中县财政经费（万元）</th>
</tr><tr>
<th>1.设备费</th>
<td><input type='text' name=array_jingfei_yusuan[0] value="<?php echo $array_jingfei_yusuan[0]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[0] value="<?php echo $array_jingfei_caizheng[0]; ?>"/></td>
</tr><tr>
<th>2.材料费</th>
<td><input type='text' name=array_jingfei_yusuan[1] value="<?php echo $array_jingfei_yusuan[1]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[1] value="<?php echo $array_jingfei_caizheng[1]; ?>"/></td>
</tr><tr>
<th>3.测试化验加工费</th>
<td><input type='text' name=array_jingfei_yusuan[2] value="<?php echo $array_jingfei_yusuan[2]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[2] value="<?php echo $array_jingfei_caizheng[2]; ?>"/></td>
</tr><tr>
<th>4.燃料动力费</th>
<td><input type='text' name=array_jingfei_yusuan[3] value="<?php echo $array_jingfei_yusuan[3]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[3] value="<?php echo $array_jingfei_caizheng[3]; ?>"/></td>
</tr><tr>
<th>5.差旅费</th>
<td><input type='text' name=array_jingfei_yusuan[4] value="<?php echo $array_jingfei_yusuan[4]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[4] value="<?php echo $array_jingfei_caizheng[4]; ?>"/></td>
</tr><tr>
<th>6.会议费 </th>
<td><input type='text' name=array_jingfei_yusuan[5] value="<?php echo $array_jingfei_yusuan[5]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[5] value="<?php echo $array_jingfei_caizheng[5]; ?>"/></td>
</tr><tr>
<th>7.合作、协作研究与交流费</th>
<td><input type='text' name=array_jingfei_yusuan[6] value="<?php echo $array_jingfei_yusuan[6]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[6] value="<?php echo $array_jingfei_caizheng[6]; ?>"/></td>
</tr><tr>
<th>8.出版/文献/信息传播/知识产权事务费</th>
<td><input type='text' name=array_jingfei_yusuan[7] value="<?php echo $array_jingfei_yusuan[7]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[7] value="<?php echo $array_jingfei_caizheng[7]; ?>"/></td>
</tr><tr>
<th>9.人员劳务费</th>
<td><input type='text' name=array_jingfei_yusuan[8] value="<?php echo $array_jingfei_yusuan[8]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[8] value="<?php echo $array_jingfei_caizheng[8]; ?>"/></td>
</tr><tr>
<th>10.专家咨询费</th>
<td><input type='text' name=array_jingfei_yusuan[9] value="<?php echo $array_jingfei_yusuan[9]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[9] value="<?php echo $array_jingfei_caizheng[9]; ?>"/></td>
</tr><tr>
<th>11.验收检查费</th>
<td><input type='text' name=array_jingfei_yusuan[10] value="<?php echo $array_jingfei_yusuan[10]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[10] value="<?php echo $array_jingfei_caizheng[10]; ?>"/></td>
</tr><tr>
<th>12.管理费</th>
<td><input type='text' name=array_jingfei_yusuan[11] value="<?php echo $array_jingfei_yusuan[11]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[11] value="<?php echo $array_jingfei_caizheng[11]; ?>"/></td>
</tr><tr>
<th>13．其他费用</th>
<td><input type='text' name=array_jingfei_yusuan[12] value="<?php echo $array_jingfei_yusuan[12]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[12] value="<?php echo $array_jingfei_caizheng[12]; ?>"/></td>
</tr>
</table>

</fieldset>

<fieldset id="yiqishebei">
<legend> 七、需增添的仪器设备</legend>
<table border="1" cellspacing=0 cellpadding="8">
<tr>
<th>名称</th><th>数量</th><th>单价</th><th>县财政拨款</th><th>自筹或其他</th><th>用途说明</th>
</tr>
<?php
$n=5;
for ($i=0;$i<$n;$i++) {
	echo "
	<tr>
	<td><input type='text' name=array_yiqi_name[$i] value=\"$array_yiqi_name[$i]\" /></td>
	<td><input type='text' name=array_yiqi_num[$i] value=\"$array_yiqi_num[$i]\" style='width:40px' /></td>
	<td><input type='text' name=array_yiqi_price[$i] value=\"$array_yiqi_price[$i]\" style='width:60px' /></td>
	<td><input type='text' name=array_yiqi_bokuan[$i] value=\"$array_yiqi_bokuan[$i]\" /></td>
	<td><input type='text' name=array_yiqi_zichou[$i] value=\"$array_yiqi_zichou[$i]\" /></td>
	<td><input type='text' name=array_yiqi_yongtu[$i] value=\"$array_yiqi_yongtu[$i]\" /></td>
	</tr>
	";
}
?>
</table>
</fieldset>

</form>
<center><input type="button" onClick="history.go(-1)" value="返回"></center>
</div>
</html>
