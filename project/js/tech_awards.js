//jQuery 代码

$(document).ready(function(){
	
	//初始化页面
	fr_hide('jibenxiangmu');
	

	//改变项目完成单位栏个数

	$("button#primary_unitinc").click(function(){
		var n = $("table#primary_unit tr").length-1; //出去表格的标头行
		if (n<15){
			var i = n;
			$("table#primary_unit").append("<tr id='tr_primary_unit_"+i+"'><td>"+(i+1)+"</td> \
        <td><input type='text' name='array_primary_unit_name["+i+"]' /></td> \
		<td><input type='text' name='array_primary_unit_subname["+i+"]' /></td> \
        <td><input type='text' name='array_primary_unit_code["+i+"]' /></td> \
        <td><input type='text' name='array_primary_unit_adress["+i+"]' /></td> \
		<td><input type='text' name='array_primary_unit_apartment["+i+"]' /></td> \
        <td><input type='text' name='array_primary_unit_attribute["+i+"]' /></td> \
			</tr>");
			n = $("table#primary_unit tr").length-1;
		} else {
			alert("你最多只能添加15个完成单位");
		}
	})
	
	
	$("button#primary_unitdec").click(function(){
		var n = $("table#primary_unit tr").length-1; //出去表格的标头行
		if (n>0) {
			var i = n-1;
			$("tr#tr_primary_unit_"+i).remove();
			n = $("table#primary_unit tr").length-1;
		} else {
			alert("已经没有完成单位栏了");
		}

	})



	//改变项目完成人员栏个数
	var n = $("table#primary_member tr").length-1; //出去表格的标头行
	$("button#primary_memberinc").click(function(){
		var n = $("table#primary_member tr").length-1; //出去表格的标头行
		if (n<50){
			var i = n;
			$("table#primary_member").append("<tr id='tr_primary_member_"+i+"'><td>"+(i+1)+"</td><td> \
			<input type='text' name='array_primary_member_name["+i+"]'/></td> \
		<td><input type='text' name='array_primary_member_sex["+i+"]' /></td> \
        <td><input type='text' name='array_primary_member_birthday["+i+"]' /></td> \
		<td><input type='text' name='array_primary_member_title["+i+"]' /></td> \
        <td><input type='text' name='array_primary_member_education["+i+"]' /></td> \
		<td><input type='text' name='array_primary_member_unit["+i+"]' /></td> \
		<td><input type='text' name='array_primary_member_contribution["+i+"]' /></td> \
			</tr>");
			n = $("table#primary_member tr").length-1;
		} else {
			alert("你最多只能添加50个合作人员");
		}
	})
	
	
	$("button#primary_memberdec").click(function(){
		var n = $("table#primary_member tr").length-1; //出去表格的标头行
		if (n>0) {
			var i = n-1;
			$("tr#tr_primary_member_"+i).remove();
			n = $("table#primary_member tr").length-1;
		} else {
			alert("已经没有合作人员栏了");
		}

	})
})

//javascript 代码

function fr_hide(name){
	var menu = new Array("jibenxiangmu","xiangmujianjie","xiangmuchuangxin","baomiyaodian","tongleibijiao","tuiguangyingyong","jingjixiaoyi","zhishichanquan","pingdingyijian","jiandingweiyuanhui","wanchengdanwei","wanchengrenyuan","diyiwanchengren","zhuanjiatuijianyijian","fujianmulu","tianxieshuoming");		
	for(var item in menu) {
		if (menu[item]==name) {
			$("#"+menu[item]).show();
		} else {
			$("#"+menu[item]).hide();
		}
		// 在项目基本情况中要显示可行性报告部分
		if (name === "jibenxiangmu") {
			$("#feasibleDiv").show();
		} else {
			$("#feasibleDiv").hide();
		}
	}
}

function formCheck(){
	if (projectForm.project_name.value===""){
		alert("请填写项目名称");
		projectForm.project_name.focus();
		return false;
	}
}