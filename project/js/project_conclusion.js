//jQuery 代码

$(document).ready(function(){
	
	//初始化页面
	fr_hide('conclusion1');

	//改变项目进度栏个数
	var n = $("table#progress tr").length-1; //出去表格的标头行
	$("button#progressinc").click(function(){
		if (n<10){
			var i = n;
			$("table#progress").append("<tr id='tr_"+i+"'><td> \
			<input type='date' name='array_progress_start["+i+"]'/>至<input type='date' name='array_progress_end["+i+"]'/></td> \
			<td><textarea name='array_progress_content["+i+"]' cols='50' rows='2'></textarea></td> \
			</tr>");
			n = $("table#progress tr").length-1;
		} else {
			alert("你最多只能添加10个进度栏");
		}
	})
	
	
	$("button#progressdec").click(function(){
		if (n>0) {
			var i = n-1;
			$("tr#tr_"+i).remove();
			n = $("table#progress tr").length-1;
		} else {
			alert("已经没有栏进度栏了");
		}

	})
})

//javascript 代码

function fr_hide(name){
	var menu = new Array("conclusion1","conclusion2","result");		
	for(var item in menu) {
		if (menu[item]==name) {
			$("#"+menu[item]).show();
		} else {
			$("#"+menu[item]).hide();
		}
	}
}

function formCheck(){
	if (projectForm.project_name.value===""){
		alert("请填写项目名称");
		projectForm.project_name.focus();
		return false;
	}
}