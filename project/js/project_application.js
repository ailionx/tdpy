//jQuery 代码

$(document).ready(function(){

	//初始化页面
	fr_hide('jibenxiangmu');
	var divFeasibility = "<fieldset id='feasibleReportId'><legend>可行性报告</legend><pre>龙游县重点科技项目可行性报告及经费概算编写提纲\n \
一、项目可行性报告   \n\
（一）立项的背景和意义。  \n\
（二）国内外研究现状和发展趋势。  \n\
（三）项目主要研究开发内容、技术关键及主要创新点。  \n\
（四）项目预期目标（主要技术经济指标、社会效益、技术应用和产业化前景以及获取自主知识产权的情况）。\n\
（五）项目实施方案、技术路线、组织方式与课题分解。\n\
（六）计划进度安排。\n\
（七）现有工作基础和条件。\n\
二、经费概算\n\
凡申请财政分期补助、事先立项事后补助的项目，均应当编制科研项目经费概算。\n\
经费概算包括两部分：一是经费概算列表，二是经费概算说明。经费概算列表的表式见该提纲附表“县级科技计划项目经费概算表”。\n经费概算说明包括： 对承担单位和相关部门承诺提供的支撑条件进行说明；\n对各科目支出的主要用途、与项目研究的相关性、概算方法、概算依据进行分析说明；对其他来源经费进行说明。\n\
项目可行性报告及经费概算编写应当回避项目申报单位和项目负责人、成员相关的信息，否则作无效申报处置。\n\
</pre>\n\
请下载以下word文档，完成项目可行性报告及经费概算编写提纲，并将完成的文档上传，<br />\n\
<a href='../files/龙游县重点科技项目可行性报告及经费概算编写提纲.DOC' style='font-size:12px;line-height:30px'>龙游县重点科技项目可行性报告及经费概算编写提纲.DOC</a><br />\n\
上传已完成的项目可行性报告及经费概算编写提纲：<br />\n\
（请注意：如果你修改了项目名称，请重新上传该文件）\n\
<form action='../proc/upload_file_server.php' method='post' enctype='multipart/form-data'>\
<input type='hidden' name='MAX_FILE_SIZE' value='2000000'>\
<input type='file' id='fileToUpload' name='fileToUpload' id='upload' enctype='multipart/form-data' />\
<button class='button' id='buttonUpload' onClick='return ajaxFileUpload();'>上传</button><img id='loading' src='../pic/loading.gif' style='display:none;'>\
</fieldset>";

// 载入这个页面时，也要判断是不是需要显示可行性报告页面	
	window.onload = function(){
		var n=document.getElementById('project_class_id').value;
		if (n==='key_project') {
			$("#feasibleDiv").append(divFeasibility);
		} else {
			$("fieldset#feasibleReportId").remove();
		}
	}

//项目名称也要传入上传文件的文件名	，对两个project_name的input进行同步输入
$("input#project_name_id").change(function(){
	var n=document.getElementById('project_name_id').value;
            $('#project_name_feasibility_id').val(n);
//	document.getElementById('project_name_feasibility_id').value = n;
	});
	
//如果是重点科技项目，则出现可行性分析表
	$("select#project_class_id").change(function(){
		var n=document.getElementById('project_class_id').value;
		if (n==='key_project') {
			$("#feasibleDiv").append(divFeasibility);
			} else {
			$("fieldset#feasibleReportId").remove();
			}
		});
	
//改变合作单位表单输入个数
	$("input#hezuo").change(function(){
		$("table#unit").empty();
		$("table#unit").append("<tr><th>&nbsp;</th><th>单位名称</th><th>法人代表</th><th>职责*(*: 0-承担，1－参加)</th></tr>");
		var n=document.getElementById('hezuo').value;
//设置上限10个
		if (n>10) {
			n=10;
			for (i=0;i<n-1;i++){
			$("table#unit").append("<tr><td>"+(i+1)+".</td><td><input type='text' name='array_unit_danweimingcheng["+i+"]'/></td><td><input type='text' name='array_unit_farendaibiao["+i+"]'/></td><td><input type='text' name='array_unit_zhize["+i+"]'/></td></tr>");
			}
			alert('超过上限,将最多保留9个除第一申请单位以外的合作单位信息栏，如果您需要更多的合作单位信息栏，请联系我们');
		} else {
			for (i=0;i<n-1;i++){
				$("table#unit").append("<tr><td>"+(i+1)+".</td><td><input type='text' name='array_unit_danweimingcheng["+i+"]'/></td><td><input type='text' name='array_unit_farendaibiao["+i+"]'/></td><td><input type='text' name='array_unit_zhize["+i+"]'/></td></tr>");
			}
		}
	});



	//改变项目组成员表单输入个数
	$("input#member_num").change(function(){
		$("table#member").empty();
		$("table#member").append("<tr><th>&nbsp;</th><th style='width:50px' >姓名</th><th style='width:80px' >出生年月</th><th style='width:80px' >专业技术职务</th><th style='width:80px' >专业</th><th  style='width:80px' >工作单位</th><th style='width:80px' >在本项目中分工</th></tr>");
	var x=document.getElementById('member_num').value;
	//设置上限15个
		if (x>15) {
			x=15;
		}
		for (i=0;i<=x-1;i++){
			$("table#member").append("<tr><td>"+(i+1)+".</td><td><input type='text' name='array_member_name["+i+"]' style='width:50px' /></td><td><input type='text' name='array_member_chushengnianyue["+i+"]' style='width:80px' /></td><td><input type='text' name='array_member_zhiwu["+i+"]' style='width:80px' /></td><td><input type='text' name='array_member_zhuanye["+i+"]' style='width:80px' /></td><td><input type='text' name='array_member_gongzuodanwei["+i+"]'  style='width:80px' /></td><td><input type='text' name='array_member_fengong["+i+"]'  style='width:80px' /></td></tr>");
		}
	});	
	//改变项目进度栏个数
	var n = $("table#progress tr").length-1; //出去表格的标头行
	$("button#progressinc").click(function(){
		if (n<10){
			var i = n;
			$("table#progress").append("<tr id='tr_"+i+"'><td> \
			<input type='date' name='array_progress_start["+i+"]'/>至<input type='date' name='array_progress_end["+i+"]'/></td> \
			<td><textarea name='array_progress_content["+i+"]' cols='50' rows='2'></textarea></td> \
			</tr>");
			n = $("table#progress tr").length-1;
		} else {
			alert("你最多只能添加10个进度栏");
		}
	})
	
	
	$("button#progressdec").click(function(){
		if (n>0) {
			var i = n-1;
			$("tr#tr_"+i).remove();
			n = $("table#progress tr").length-1;
		} else {
			alert("已经没有栏进度栏了");
		}

	})
})

//javascript 代码

function fr_hide(name){
	var menu = new Array("jibenxiangmu","gailan","chengdandanwei","xiangmuchengyuan","litiyiju","kaifaneirong","jihuajindu","shichangziyuan","shenqingjingfei");		
	for(var item in menu) {
		if (menu[item]==name) {
			$("#"+menu[item]).show();
		} else {
			$("#"+menu[item]).hide();
		}
		// 在项目基本情况中要显示可行性报告部分
		if (name === "jibenxiangmu") {
			$("#feasibleDiv").show();
		} else {
			$("#feasibleDiv").hide();
		}
	}
}

function formCheck(){
	if (projectForm.project_name.value===""){
		alert("请填写项目名称");
		projectForm.project_name.focus();
		return false;
	}
}