$(document).ready(function(){
	$("#veri_code_img").click(function(){
		this.src="../proc/verification_code.php?"+Math.random();
	});
});


function check_name_exist(name){
	$(document).ready(function(){
		$("span").load('./proc/check_name_exist.php',{name:name});

	});

}

function check_project_exist(name){
	$(document).ready(function(){
		var type = document.getElementsByName("submit_type");
		$("span").load('../proc/check_name_exist.php?submit_type='+type,{name:name});
	});

}

function RegCheck() {
	if (loginForm.user.value.replace(/\s/g,"")==="") {
		alert("请填写用户名");
		loginForm.user.focus();
		return false;
	}
	if (loginForm.password.value.replace(/\s/g,"")==="") {
		alert("请输入密码");
		loginForm.password.focus();
		return false;
	}
		if (loginForm.input_code.value.replace(/\s/g,"")==="") {
		alert("请输入验证码");
		loginForm.input_code.focus();
		return false;
	}
}

function checkNull(field,msg) {
	var inputX = document.getElementsByName(field),
            _$inputX = $(inputX[0]),
            _$fieldset = _$inputX.parents().find('fieldset'),
            _fieldsetId = _$fieldset.attr('id');
//	alert(field);
//	alert(inputX[0].value);
	if (inputX[0].value.replace(/\s/g,"")==="") {
		alert(msg);
                fr_hide(_fieldsetId);
		inputX[0].focus();
		return false;
	} 
}

function checkForm(mustFillArr){
	for (key in mustFillArr) {
		var field = key;
		msg = "请填写"+mustFillArr[key];
		if(checkNull(field,msg)===false) {
			return false;
		}
	}
}

function getDir(){
    var locHref = location.href; //取得完整路径
    var locArr = locHref.split("/"); //用"/"切分路径为数组
    delete locArr[locArr.length - 1]; //删除数组中最后一位，即当前网页的文件名
    return locArr.join("/"); //用"/"组合数组，并返回值
}

function getRootPath(){
	var strFullPath=location.href;
	var strPath=location.pathname;
	var pos=strFullPath.indexOf(strPath);
	var prePath=strFullPath.substring(0,pos);
	var postPath=strPath.substring(0,strPath.substr(1).indexOf('/')+1);
	return(prePath+postPath);
}

(function($){
    
    var managerAccountPassword = new (function(){
        
        this._$actions = $('#manage-account').find(".manage-change-password");
        
        this.init = function(){
            var _this = this;
            
            _this._$actions.each(function(){
                var _$this = $(this),
                    _id = _$this.attr('account-id'),
                    _companyName = _$this.parent().siblings('.company-name').html();
                
                _$this.click(function(e){
                    e.preventDefault();
                    var _newPw = prompt('请为 ' + _companyName + ' 输入新的密码');
                    
                    if(_newPw === null) {
                        return false;
                    }
                    
                    if(_newPw === '') {
                        alert('新密码无效，请重新输入');
                        return false;
                    }
                    
                    $.ajax({
                        
                        type: 'POST',
                        
                        url: '../proc/ajaxHandlerChangePw.php',
                        
                        dataType: 'json',
                        
                        data: {
                            ID: _id,
                            NewPassword: _newPw
                        },
                        
                        beforeSend: function(){
                            
                        },
                        
                        success: function(){
                            alert('变更密码成功');
                        },
                        
                        error: function(){
                            alert('操作失败');
                        }
                        
                    });
                    
                });
            });
        };

        this.init();
        
        
    });        
    
    
    
})(jQuery);
	
