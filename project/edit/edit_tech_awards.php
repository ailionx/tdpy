<?php
session_start();
include ("../../conf.php");
$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
$id = $_SESSION[id];
	$user = $_SESSION[user];
	if ($_GET['project_id'] !== NULL){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from tech_awards where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		
		//不同用户进入此页面的权限
		permissionBlocker('edit',$value[status]);
		
		//反序列化数组存入的数据
		$arrayLong_list = array_field_inDB('tech_awards');
		foreach ($arrayLong_list as $arrayLong) {
			${$arrayLong} = unserialize($value[$arrayLong]);
		}




	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
?>
<!DOCTYPE html5>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../js/my_javascript.js"></script>
<script type="text/javascript" src="../js/tech_awards.js"></script>
<style type="text/css">
.content ul, .content ol { 
	padding: 0 15px 15px 40px; /* 此填充反映上述标题和段落规则中的右填充。填充放置于下方可用于间隔列表中其它元素，置于左侧可用于创建缩进。您可以根据需要进行调整。 */
}

/* ~~ 导航列表样式（如果选择使用预先创建的 Spry 等弹出菜单，则可以删除此样式） ~~ */
ul.nav_menu {
	position:fixed;
	left:10px;
	top:10px;
	float:left;
	list-style: none; /* 这将删除列表标记 */
	border-top: 1px solid #666; /* 这将为链接创建上边框 – 使用下边框将所有其它项放置在 LI 中 */
	margin-bottom: 15px; /* 这将在下面内容的导航之间创建间距 */
	margin-left:-30px;
	margin-right:10px;
}
ul.nav_menu li {
	border-bottom: 1px solid #666; /* 这将创建按钮间隔 */
	font-family:"宋体";
	font-size:14px;
	line-height:20px;
	text-align:center;
}
ul.nav_menu a, ul.nav_menu a:visited { /* 对这些选择器进行分组可确保链接即使在访问之后也能保持其按钮外观 */
	display: block; /* 这将为链接赋予块属性，使其填满包含它的整个 LI。这样，整个区域都可以响应鼠标单击操作。 */
	width: 110px;  /*此宽度使整个按钮在 IE6 中可单击。如果您不需要支持 IE6，可以删除它。请用侧栏容器的宽度减去此链接的填充来计算正确的宽度。 */
	text-decoration: none;
	color:#000;
}

.form_area {
	position:fixed;
	left:132px;
	top:17px;
}

.wideForm input {
	width:100px;
}

.wideForm td {
	padding:0px;
}

.leftTable td {
	text-align:left;	
}

</style>
<title>科技进步奖</title>
</head>

<form method="post" action="../proc/submit_form.php?project_id=<?php echo $project_id; ?>" name="tech_awards_form" onSubmit="return formCheck()"> 
<input type="hidden" value="edit" name="apply_edit" value="<?php echo $value['apply_edit'];?>">
<input type="hidden" value="tech_awards" name="submit_type" value="<?php echo $value['submit_type'];?>">
<ul class="nav_menu">
<li><a href="#" onClick="fr_hide('jibenxiangmu')">项目基本情况</button></a></li>
<li><a href="#" onClick="fr_hide('xiangmujianjie')">项目简介</button></a></li>
<li><a href="#" onClick="fr_hide('xiangmuchuangxin')">项目创新</button></a></li>
<li><a href="#" onClick="fr_hide('baomiyaodian')">保密要点</button></a></li>
<li><a href="#" onClick="fr_hide('tongleibijiao')">同类比较</button></a></li>
<li><a href="#" onClick="fr_hide('tuiguangyingyong')">推广应用</button></a></li>
<li><a href="#" onClick="fr_hide('jingjixiaoyi')">经济效益</button></a></li>
<li><a href="#" onClick="fr_hide('zhishichanquan')">知识产权</button></a></li>
<li><a href="#" onClick="fr_hide('pingdingyijian')">评定意见</button></a></li>
<li><a href="#" onClick="fr_hide('jiandingweiyuanhui')">评审委员会</button></a></li>
<li><a href="#" onClick="fr_hide('wanchengdanwei')">完成单位</button></a></li>
<li><a href="#" onClick="fr_hide('wanchengrenyuan')">完成人员</button></a></li>
<li><a href="#" onClick="fr_hide('diyiwanchengren')">第一完成人</button></a></li>
<li><a href="#" onClick="fr_hide('zhuanjiatuijianyijian')">专家推荐意见</button></a></li>
<li><a href="#" onClick="fr_hide('fujianmulu')">附件目录</button></a></li>
<li><a href="#" onClick="fr_hide('tianxieshuoming')">填写说明</button></a></li>
<li><bold align="center"><input style="font-size:14px;color:#30F" type="submit" value="保存"></bold></li>
</ul>
<div class="form_area">

<fieldset id='jibenxiangmu'>
<legend>项目基本情况</legend>
    <table border="0" class="leftTable">
        <tr>
            <td>成果类别：<br />
            <select name="achievement_class">
            	<option value=''></option>
            	<?php 
				$achievement_class_array = array('1'=>'基础研究类','2'=>'技术发明类','3'=>'技术开发类','4'=>'社会公益类','5'=>'重大工程类','6'=>'软科学类');
				foreach ($achievement_class_array as $arrKey=>$option) {
					if ($value['achievement_class']=== "$arrKey") {
						echo "<option value=$arrKey selected>$option</option>";
					} else {
						echo "<option value=$arrKey>$option</option>";
					}
				}
				?>
                </select></td>
            <td>行业评审组代码：<br />
            <select name="review_code">
            	<option value=''></option>
            	<?php 
				$review_code_array = array('1'=>'机械、电力','2'=>'电子、通讯、信息','3'=>'化工、冶金、环保','4'=>'建设、建工、建材、交通、水工、地矿、煤炭','5'=>'轻工、纺织、食品','6'=>'农业','7'=>'畜牧、林业、渔业、气象、农田水利','8'=>'医学、卫生','9'=>'中医、医药、医疗器械（器材）','10'=>'软科学');
				foreach ($review_code_array as $arrKey=>$option) {
					if ($value['review_code']=== "$arrKey") {
						echo "<option value=$arrKey selected>$option</option>";
					} else {
						echo "<option value=$arrKey>$option</option>";
					}
				}
				?>
                </select></td>
            <td>编号：<br /><input type="text" name="id_number" value="<?php echo $value['id_number'];?>"/></td>
        </tr>
        <tr>
            <td>项目名称：<br /><input type="text" name="project_name" value="<?php echo $value['project_name'];?>"></td>
            <td>主要完成人：<br /><input type="text" name="primary_member" value="<?php echo $value['primary_member'];?>"></td>
            <td>主要完成单位：<br /><input type="text" name="primary_unit" value="<?php echo $value['primary_unit'];?>"></td>
        </tr>
    <tr>
    <td colspan="3">部门或单位推荐意见及建议奖励等级<br/>
    <textarea  name="unit_commemt" rows="2px" cols="60px"><?php echo $value['unit_commemt'];?></textarea></td>
    </tr>
    </table>
    
    <table border="0"  class="leftTable">
        <tr>
            <td>项目联系人姓名<br><input type="text" name="contact_name" value="<?php echo $value['contact_name'];?>"></td>
            <td>项目联系人电话<br><input type="text" name="contact_phone" value="<?php echo $value['contact_phone'];?>"></td>
        </tr><tr>
            <td>所属国民经济行业<br>
            <select name="financial_class">
            	<option value=''></option>
            	<?php 
				$financial_class_array = array('A'=>'农、林、牧、渔业','B'=>'采掘业','C'=>'制造业','D'=>'电力、煤气及水的生产和供应业','E'=>'建筑业','F'=>'地质勘察业、水利管理业','G'=>'交通运输、仓储及邮电通信业','H'=>'批发和零售贸易、餐饮业','I'=>'金融、保险业','J'=>'房地产业','K'=>'社会服务业','L'=>'卫生、体育和社会福利业','M'=>'教育、文化艺术和广播电影电视事业','N'=>'科学研究和综合技术服务业','O'=>'国家机关、党政机关和社会团体','P'=>'其它行业');
				foreach ($financial_class_array as $arrKey=>$option) {
					if ($value['financial_class']=== "$arrKey") {
						echo "<option value=$arrKey selected>$option</option>";
					} else {
						echo "<option value=$arrKey>$option</option>";
					}
				}
				?>
                </select></td>
            <td>任务来源<br>
            <select name="task_resource">
            	<option value=''></option>
            	<?php 
				$task_resource_array = array('A'=>'国家科技攻关','B'=>'国家“863计划”','C'=>'国家基础性研究重大项目计划','D'=>'国家科技型中小企业创新基金','E'=>'国家重点新产品计划','F'=>'国家其它计划','G'=>'省重大科研计划','H'=>'省重点科研计划','I'=>'省一般科研计划','J'=>'省科技型中小企业创新资金专项','K'=>'省自然科学基金','L'=>'省国际合作计划','M'=>'省新产品计划','N'=>'省其它科技计划','O'=>'其它部委计划','P'=>'其它单位委托','Q'=>'自选','R'=>'非职务');
				foreach ($task_resource_array as $arrKey=>$option) {
					if ($value['task_resource']=== "$arrKey") {
						echo "<option value=$arrKey selected>$option</option>";
					} else {
						echo "<option value=$arrKey>$option</option>";
					}
				}
				?>
                </select></td>
        </tr>
        <tr>
            <td>主持评审单位<br><input type="text" name="judgement_unit" value="<?php echo $value['judgement_unit'];?>"></td>
            <td>评审日期<br><input type="date" name="judgement_date" value="<?php echo $value['judgement_date'];?>"  /></td>
        </tr>
        <tr>
            <td colspan="2">成果水平:<br />
            <?php
			$achievement_level_array = array('international_lead'=>'国际领先','international_advanced'=>'国际先进','internal_lead'=>'国内领先','internal_advanced'=>'国内先进');
			foreach ($achievement_level_array as $arrKey=>$option) {
					if ($value['achievement_level']=== "$arrKey") {
						echo "<input type='radio' name='achievement_level' value=$arrKey checked>$option";
					} else {
						echo "<input type='radio' name='achievement_level' value=$arrKey>$option";
					}
				}
			?>
		</td>
        </tr>
        <tr>
            <td colspan="2">计划（基金）名称和编号<br><textarea name="fundation_name_num"><?php echo $value['fundation_name_num'];?></textarea></td>
        </tr>
        <tr>
            <td>项目起始时间：<br><input type="date" name="start_time" value="<?php echo $value['start_time'];?>"></td>
            <td>项目完成时间：<br><input type="date" name="finish_time" value="<?php echo $value['finish_time'];?>"></td>
        </tr>
    </table>
</fieldset>

<fieldset id='xiangmujianjie'>
<legend>项目简介（所属科学技术领域、主要内容、特点、推广应用、产业化情况）</legend>
<textarea name="project_brief" cols="65px" rows="15px" placeholder="项目简介：（不超过600个汉字）" maxlength="600"><?php echo $value['project_brief'];?></textarea>
</fieldset>

<fieldset id='xiangmuchuangxin'>
<legend>发现、发明及创新点</legend>
创新类别：<br />
            <?php
			$innovate_class_array = array('original_innovate'=>'原始创新','integrate_innovate'=>'集成创新','introduce_innovate'=>'引进消化吸收再创新');
			foreach ($innovate_class_array as $arrKey=>$option) {
					if ($value['innovate_class']=== "$arrKey") {
						echo "<input type='radio' name='innovate_class' value=$arrKey checked>$option";
					} else {
						echo "<input type='radio' name='innovate_class' value=$arrKey>$option";
					}
				}
			?>

<br />
<textarea name="innovate_piont" cols="65px" rows="10px" placeholder="发现、发明及创新点（不超过400个汉字）："  maxlength="400"><?php echo $value['innovate_piont'];?></textarea>
</fieldset>

<fieldset id='baomiyaodian'>
<legend>保密要点</legend>
<textarea name="classify_point" cols="65px" rows="3px" placeholder="保密要点：（不超过100个汉字）" maxlength="100"><?php echo $value['classify_point'];?></textarea>
</fieldset>

<fieldset id='tongleibijiao'>
<legend>与当前国内外同类研究、同类技术的综合比较</legend>
<textarea name="class_comparasion" cols="65px" rows="20px" placeholder="与当前国内外同类研究、同类技术的综合比较：（不超过1000个汉字）" maxlength="1000"><?php echo $value['class_comparasion'];?></textarea>
</fieldset>

<fieldset id='tuiguangyingyong'>
<legend>推广应用情况</legend>
*应用情况栏目基础研究类项目填写研究论文、学术专著发表国内外引用情况：（不超过800个汉字）<br />
<textarea name="application_status" cols="65px" rows="15px" placeholder="*应用情况栏目基础研究类项目填写研究论文、学术专著发表国内外引用情况：（不超过800个汉字）" maxlength="800"><?php echo $value['application_status'];?></textarea>
</fieldset>

<fieldset id='jingjixiaoyi'>
<legend>经济效益（*经济效益栏目基础研究类项目可不填写）</legend>
<table border='0'>
	<tr>
		<td>经济效益 （直接效益）</td>
        <td>单位：万元（人民币）</td>
    </tr>
    <tr>
    	<td>项目总投资额</td>
        <td><input type="text" name="total_investment" value="<?php echo $value['total_investment'];?>" /></td>
    </tr>
</table>
<table class="wideForm" border='0'>
    <tr>
    	<th>年份</th><th>新增销售额</th><th>新增利润</th><th>新增税收</th><th>创收外汇（美元）</th><th>节支总额</th><br />
	</tr>
<?php
	for($i=0;$i<3;$i++) {
		echo "
	<tr>
		<td><input type='text' name='array_financial_year[$i]' value='$array_financial_year[$i]' /></td>
        <td><input type='text' name='array_financial_revenue[$i]' value='$array_financial_revenue[$i]' /></td>
        <td><input type='text' name='array_financial_benefit[$i]' value='$array_financial_benefit[$i]' /></td>
        <td><input type='text' name='array_financial_fax[$i]' value='$array_financial_fax[$i]' /></td>
        <td><input type='text' name='array_financial_forex[$i]' value='$array_financial_forex[$i]' /></td>
        <td><input type='text' name='array_financial_total[$i]' value='$array_financial_total[$i]' /></td>
    </tr>";
	}
?>
</table>
各栏目的计算依据：<br />
<textarea name="caculation_gist" cols="65px" rows="5px"><?php echo $value['caculation_gist'];?></textarea>
<br />间接效益：<br />
<textarea name="direct_benefit" cols="65px" rows="5px"><?php echo $value['direct_benefit'];?></textarea>
<br />社会效益：<br />
<textarea name="society_benefit" cols="65px" rows="5px"><?php echo $value['society_benefit'];?></textarea>
</fieldset>

<fieldset id='zhishichanquan'>
<legend>享有自主知识产权情况</legend>
<table>
	<tr>
    	<th>国别</th><th>申请号</th><th>专利号</th><th>项目名称</th>
    </tr>
<?php
	for($i=0;$i<5;$i++) {
		echo "
	<tr>
        <td><input type='text' name='array_intellectual_country[$i]' value='$array_intellectual_country[$i]' /></td>
        <td><input type='text' name='array_intellectual_id[$i]' value='$array_intellectual_id[$i]' /></td>
        <td><input type='text' name='array_intellectual_number[$i]' value='$array_intellectual_number[$i]' /></td>
        <td><input type='text' name='array_intellectual_name[$i]' value='$array_intellectual_name[$i]' /></td>
    </tr>";
	}
?>
</table>
<textarea name="other_property" cols="65px" rows="15px" placeholder="其他知识产权情况：（如著作权、软件登记、商标权、动植特新品种审定、药品、医疗器械、农药、食品或饲料添加剂、行业标准等证书）"><?php echo $value['other_property'];?></textarea>
</fieldset>

<fieldset id='pingdingyijian' >
<legend>评审（评审）意见</legend>
<textarea name="judgement_opinion" cols="65px" rows="20px" placeholder="评审（评审）意见：（全文）"><?php echo $value['judgement_opinion'];?></textarea>
</fieldset>

<fieldset id='jiandingweiyuanhui' class="wideForm"> 
<legend>评 审 委 员 会 名 单</legend>
<table>
	<tr>
		<th>序号</th><th>评审会职务</th><th>姓 名</th><th>工作单位</th><th>所学专业</th><th>现从事专业</th><th>职称职务</th>
    </tr>
<?php
	for($i=0;$i<13;$i++) {
		$ii=$i+1;
		echo "
	<tr>
        <td>$ii</td>
        <td><input type='text' name='array_council_job[$i]' value='$array_council_job[$i]' /></td>
        <td><input type='text' name='array_council_name[$i]' value='$array_council_name[$i]' /></td>
        <td><input type='text' name='array_council_unit[$i]' value='$array_council_unit[$i]' /></td>
		<td><input type='text' name='array_council_speciality[$i]' value='$array_council_speciality[$i]' /></td>
        <td><input type='text' name='array_council_career[$i]' value='$array_council_career[$i]' /></td>
        <td><input type='text' name='array_council_title[$i]' value='$array_council_title[$i]' /></td>
    </tr>";
	}
?>
</table>
&nbsp;&nbsp;<p>请在纸质材料中附上原件复印件</p>
</fieldset>


<fieldset id='wanchengdanwei' class="wideForm">
<legend>主 要 完 成 单 位 情 况</legend>
注：
<ol>
<li>完成单位序号超过9个可加附页。其顺序必须与评审证书封面上的完全一致。</li>
<li>完成单位名称必须填写全称，不得简化，与单位公章完全一致，并填入完成单位名称的第一栏中，其下属机构名称则填入第二栏中。</li>
<li>详细通信地址要写明县、街道和门牌号码。</li>
<li>隶属部门是指本单位和行政关系隶属哪一个主管部门。并将其名称填入表中。</li>
<li>单位属性是指本单位在  1.独立科研机构　2.大专院校  3.国有企业  4.民营企业  5.其他 五类性质中属于哪一类，在栏中选填1.2.3.4.5.既可。</li>
</ol>
<table id='primary_unit'>
	<tr>
		<th>序号</th><th>完成单位名称</th><th>邮政编码</th><th>详细通信地址</th><th>隶属部门</th><th>单位属性</th>
    </tr>
<?php
	for($i=0;$i<9;$i++) {
		$ii=$i+1;
		echo "
	<tr id='tr_primary_unit_$i'>
        <td>$ii</td>
        <td><input type='text' name='array_primary_unit_name[$i]' value='$array_primary_unit_name[$i]' /></td>
		<td><input type='text' name='array_primary_unit_subname[$i]' value='$array_primary_unit_subname[$i]' /></td>
        <td><input type='text' name='array_primary_unit_code[$i]' value='$array_primary_unit_code[$i]' /></td>
        <td><input type='text' name='array_primary_unit_adress[$i]' value='$array_primary_unit_adress[$i]' /></td>
		<td><input type='text' name='array_primary_unit_apartment[$i]' value='$array_primary_unit_apartment[$i]' /></td>
    </tr>";
	}
?>
</table>
<button type='button' id="primary_unitinc">增加完成单位</button>
<button type='button' id="primary_unitdec">减少完成单位</button>
</fieldset>

<fieldset id='wanchengrenyuan' class="wideForm">
<legend>主 要 完 成 人 员 情 况</legend>
注：
<ol>
<li>主要完成人员顺序必须与评审证书上的顺序完全一致，并与推荐书首页主要完成人员的顺序一致。</li>
<li>本人签名栏不得代签。</li>
</ol>
<table id='primary_member'>
	<tr>
		<th>序号</th><th>姓名</th><th>性别</th><th>出生年月</th><th>技术职称</th><th>文化程度</th><th>工作单位</th><th>对成果创造性贡献</th>
    </tr>
<?php
	for($i=0;$i<9;$i++) {
		$ii=$i+1;
		echo "
	<tr id='tr_primary_member_$i'>
        <td>$ii</td>
        <td><input type='text' name='array_primary_member_name[$i]' value='$array_primary_member_name[$i]' /></td>
        <td><input type='text' name='array_primary_member_sex[$i]' value='$array_primary_member_sex[$i]' /></td>
        <td><input type='text' name='array_primary_member_birthday[$i]' value='$array_primary_member_birthday[$i]' /></td>
		<td><input type='text' name='array_primary_member_title[$i]' value='$array_primary_member_title[$i]' /></td>
        <td><input type='text' name='array_primary_member_education[$i]' value='$array_primary_member_education[$i]' /></td>
		<td><input type='text' name='array_primary_member_unit[$i]' value='$array_primary_member_unit[$i]' /></td>
		<td><input type='text' name='array_primary_member_contribution[$i]' value='$array_primary_member_contribution[$i]' /></td>
    </tr>";
	}
?>
</table>
<button type='button' id="primary_memberinc">增加项完成人员</button>
<button type='button' id="primary_memberdec">减少项完成人员</button>
</fieldset>

<fieldset id='diyiwanchengren'>
<legend>第一主要完成人情况</legend>
<table class="leftTable">
	<tr>
		<td>姓名<br /><input type='text' name='array_mvp_info[name]' value="<?php echo $array_mvp_info[name];?>" /></td>
        <td>性别<br /><input type='text' name='array_mvp_info[sex]' value="<?php echo $array_mvp_info[sex];?>" /></td>
        <td>民族<br /><input type='text' name='array_mvp_info[nation]' value="<?php echo $array_mvp_info[nation];?>" /></td>
		<td>出生地<br /><input type='text' name='array_mvp_info[birthplace]' value="<?php echo $array_mvp_info[birthplace];?>" placeholder='省（自治区、市）   市（县）'/></td>
    </tr>
  	<tr>
        <td>出生日期<br /><input type='text' name='array_mvp_info[birthday]' value="<?php echo $array_mvp_info[birthday];?>" placeholder='年  月'/></td>
        <td>党派<br /><input type='text' name='array_mvp_info[partygroup]' value="<?php echo $array_mvp_info[partygroup];?>" /></td>
		<td>工作单位<br /><input type='text' name='array_mvp_info[unit]' value="<?php echo $array_mvp_info[unit];?>" /></td>
        <td>联系电话<br /><input type='text' name='array_mvp_info[phone]' value="<?php echo $array_mvp_info[phone];?>" /></td>   
    </tr>  
    <tr>
    	<td colspan="3">通讯地址及邮政编码<br /><input type='text' name='array_mvp_info[adress]' value="<?php echo $array_mvp_info[adress];?>"  style="width:450px"/></td>
        <td>电子信箱<br><input type='text' name='array_mvp_info[email]' value="<?php echo $array_mvp_info[email];?>" /></td>
    </tr>
    <tr>
    	<td colspan="2">家庭住址<br><input type='text' name='array_mvp_info[home]' value="<?php echo $array_mvp_info[home];?>"  style="width:300px"/></td>
        <td>住宅电话<br><input type='text' name='array_mvp_info[homenumber]' value="<?php echo $array_mvp_info[homenumber];?>" /></td>
    </tr>
    <tr>
    	<td>毕业学校<br><input type='text' name='array_mvp_info[school]' value="<?php echo $array_mvp_info[school];?>" /></td>
        <td>文化程度<br><input type='text' name='array_mvp_info[education]' value="<?php echo $array_mvp_info[education];?>" /></td>
        <td>学位<br><input type='text' name='array_mvp_info[degree]' value="<?php echo $array_mvp_info[degree];?>" /></td>
    </tr>
    <tr>
    	<td>职务、职称<br><input type='text' name='array_mvp_info[title]' value="<?php echo $array_mvp_info[title];?>" /></td>
        <td>专业、专长<br><input type='text' name='array_mvp_info[speciality]' value="<?php echo $array_mvp_info[speciality];?>" /></td>
        <td>毕业时间<br><input type='text' name='array_mvp_info[graduatedate]' value="<?php echo $array_mvp_info[graduatedate];?>" /></td>
    </tr>
    <tr>
    	<td colspan="3">曾获奖励及荣誉称号情况：<br><textarea name='array_mvp_info[awards]' cols="65px" rows="3px"/><?php echo $array_mvp_info[awards];?></textarea></td>
    </tr>
	<tr><td colspan="3">参加本项目的起止时间：<br>
自<input type="text" name="array_mvp_info[syear]" value="<?php echo $array_mvp_info[syear];?>" style="width:80px"/>年<input type="text" name="array_mvp_info[smonth]" value="<?php echo $array_mvp_info[smonth];?>"  style="width:30px"/>月  至  于<input type="text" name="array_mvp_info[fyear]" value="<?php echo $array_mvp_info[fyear];?>" style="width:80px" />年<input type="text" name="array_mvp_info[fmonth]" value="<?php echo $array_mvp_info[fmonth];?>"  style="width:30px"/>月
</td>
</tr><tr>
<td  colspan="3">对本项目的主要学术（技术）贡献<br>
<textarea name='array_mvp_info[contribution]' cols="65px" rows="5px"/><?php echo $array_mvp_info[contribution];?></textarea></td>
</tr>
</table>
</fieldset>

<fieldset id='zhuanjiatuijianyijian'>
<legend>专 家 推 荐 意 见（推荐类别仅限基础研究类项目或技术发明类项目）</legend>
(单位推荐的项目，此表不填)
<fieldset>
<legend>推荐专家情况</legend>
<table>
	<tr>
    	<td>姓名<br /><input type="text" name="array_specialist_info[name]" value="<?php echo $array_specialist_info[name];?>" /></td>
        <td>工作单位<br /><input type="text" name="array_specialist_info[unit]" value="<?php echo $array_specialist_info[unit];?>" /></td>
    </tr>
    <tr>
        <td>通信地址<br /><input type="text" name="array_specialist_info[adress]" value="<?php echo $array_specialist_info[adress];?>" /></td>
        <td>联系电话<br /><input type="text" name="array_specialist_info[phone]" value="<?php echo $array_specialist_info[phone];?>" /></td>
    </tr>
    <tr>
        <td>专业、专长<br /><input type="text" name="array_specialist_info[speciality]" value="<?php echo $array_specialist_info[speciality];?>" /></td>
        <td rowspan="2">专家情况（用√表示）<br />
        
         <?php

			$array_specialist_info_prize_array = array('highest_prize'=>'最高科技奖得主','CASer'=>'中国科学院院士','CAEer'=>'中国工程院院士');
			foreach ($array_specialist_info_prize_array as $arrKey=>$option) {
					if ($array_specialist_info_prize[$arrKey]=== "$arrKey") {
						echo "$option<input type='checkbox' name=array_specialist_info_prize[$arrKey] value=$arrKey checked><br>";
					} else {
						echo "$option<input type='checkbox' name=array_specialist_info_prize[$arrKey] value=$arrKey><br>";
					}
				}
		?>
        </td>
    </tr>
    <tr>
        <td>现从事的科学技术工作<br /><input type="text" name="array_specialist_info[job]" value="<?php echo $array_specialist_info[job];?>" /></td>
    </tr>        
</table>
</fieldset>
推荐意见（不超过500个汉字）<br />
<textarea name='array_specialist_info[comment]' cols="65px" rows="15px" maxlength="500" placeholder="（不超过500个汉字）"/><?php echo $array_specialist_info[comment];?></textarea>

</fieldset>

</form>

<fieldset id="fujianmulu">
<legend>附件目录</legend>
<ol style="font-size:10px">
<li>技术评价证明（原件）</li>
<li>试制工作总结、技术总结材料</li>
<li>检验报告</li>
<li>经济效益证明</li>
<li>应用证明</li>
<li>“享有自主知识产权情况”栏填写的是指国家专利证书及发明权利要求书，著作权、软件登记、商标权、动植特新品种审定、药品、医疗器械、农药、食品或饲料添加剂、行业标准等证书）</li>
<li>其他证明</li>
</ol>
</fieldset>

<fieldset id="tianxieshuoming">
<legend>填写说明</legend>
<ol style="font-size:10px">
<li>成果类别：①基础研究类；②技术发明类；③技术开发类；④社会公益类；⑤重大工程类；⑥软科学类。</li>
<li>行业评审组代码：①机械、电力②电子、通讯、信息③化工、冶金、环保④建设、建工、建材、交通、水工、地矿、煤炭⑤轻工、纺织、食品⑥农业⑦畜牧、林业、渔业、气象、农田水利⑧医学、卫生⑨中医、医药、医疗器械（器材）⑩软科学</li>
<li>“所属国民经济行业”按推荐项目所属行业在相应字母上划“√”。</li>
国家标准《GB4754—94》国民经济行业分16个门类：（A）农、林、牧、渔业；（B）采掘业；（C）制造业；（D）电力、煤气及水的生产和供应业；（E）建筑业；（F）地质勘察业、水利管理业；（G）交通运输、仓储及邮电通信业；（H）批发和零售贸易、餐饮业；（I）金融、保险业；（J）房地产业；（K）社会服务业；（L）卫生、体育和社会福利业；（M）教育、文化艺术和广播电影电视事业；（N）科学研究和综合技术服务业；（O）国家机关、党政机关和社会团体；（P）其它行业。</li>
<li>“任务来源”按推荐项目所属计划在相应字母上划“√”。</li>
<ol type="A">
<li>国家科技攻关</li><li>国家“863计划”</li><li>国家基础性研究重大项目计划</li>
<li>国家科技型中小企业创新基金</li><li>国家重点新产品计划</li>
<li>国家其它计划</li><li>省重大科研计划</li><li>省重点科研计划</li>
<li>省一般科研计划</li><li>省科技型中小企业创新资金专项</li>
<li>省自然科学基金</li><li>省国际合作计划</li><li>省新产品计划</li>
<li>省其它科技计划</li><li>其它部委计划</li><li>其它单位委托</li>
<li>自选</li><li>非职务</li>
</ol>

<li>“经济效益”栏中填写的数字只填写在推荐前三年项目完成单位（包括以该项目技术出资入股或转让的相关企业）销售申报项目产品（或技术）所取得的直接效益，若申报一等奖（基础研究类、社会公益类、软科学项目除外）项目，要求提交销售发票复印件或专项审计报告。<br>
  各栏目的计算依据，应就生产或应用该项目后产生的直接累计净增效益以及提高产品的质量、提高劳动生产率等作用简要说明，并具体列出本表所填各项效益的计算方法和计算依据。</li>
<li>“间接效益”是指项目用户单位应用该项目（技术）后取得的效益。</li>
<li>“社会效益”栏填写的是指推荐项目在推动科学技术进步，保护自然资源或生态环境；提高国防能力；保障国家和社会安全；改善人民物质、文化、生活及健康水平等方面所起的作用，应扼要地做出说明。</li>
<li>中国科学院院士、中国工程院院士推荐的项目，应有3名院士以上共同推荐，“专家推荐意见”由推荐人独立填写、不打印、不代填后签名，不可联名推荐。</li>
</ol>
</fieldset>

<center><input type="button" onClick="history.go(-1)" value="返回"></center>
</div>
</html>
