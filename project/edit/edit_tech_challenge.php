<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$id = $_SESSION[id];
	$user = $_SESSION[user];
	if ($_GET['project_id'] !== NULL){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from tech_challenge where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		//不同用户进入此页面的权限
		permissionBlocker('edit',$value[status]);
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>技术难题</title>
</head>

<h1><center>“企业难题招标、技术需求”工作单</center></h1>
<form method="post" action="../proc/submit_form.php?project_id=<?php echo $project_id; ?>" name="tech_challenge_form"> 
<input type="hidden" value="edit" name="apply_edit">
<input type="hidden" value="tech_challenge" name="submit_type">
<fieldset style="font-size:12px">
<table border="0" style="font-size:12px">
  <tr>
    <td>企业名称：<br><input type="text" name="company_name" value="<?php echo $value[company_name] ?>" readonly="readonly"></td>
    <td>行业：<br><input type="text" name="industry_class" value=<?php echo $value[industry_class] ?> ></td>
  </tr>
  <tr>
    <td>通讯地址：<br><input type="text" name="company_address" value=<?php echo $value[company_address] ?> ></td>
    <td>传真：<br><input type="text" name="fax" value=<?php echo $value[fax] ?> ></td>
  </tr>
  <tr>
    <td>网址：<br><input type="text" name="website" value=<?php echo $value[website] ?> ></td>
    <td>E-mail：<br><input type="email" name="email" value=<?php echo $value[email] ?> ></td>
  </tr>
  <tr>
    <td>联系人：<br><input type="text" name="contact_name" value=<?php echo $value[contact_name] ?> ></td>
    <td>电话：<br><input type="text" name="contact_phone" value=<?php echo $value[contact_phone] ?> ></td>
  </tr>
  <tr>
    <td>邮编：<br><input type="text" name="post_code" value=<?php echo $value[post_code] ?> ></td>
    <td>拟提供资金：<br><input type="text" name="plan_fund" value=<?php echo $value[plan_fund] ?> ></td>
	<td>是否同意网上技术市场发布：<br>
<?php 
	$allow_publish_list = array('是','否');
	draw_pd_set('allow_publish',$allow_publish_list,$value['allow_publish']);	
?>
	</td>
  </tr>
  <tr>  
    <td colspan="6">技术难题名称
（限100字内）<br /><textarea cols="60px" rows="10px" name="project_name"><?php echo $value[project_name] ?></textarea></td>
  </tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
	<legend>主要内容和技术经济指标（限2000字内）</legend>
	<textarea cols="60px" rows="20px" name="content" maxlength="2000"><?php echo $value[content] ?></textarea>
</fieldset>
<fieldset style="font-size:12px">
	<legend>备    注（限500字内）</legend>
	<textarea cols="60px" rows="8px" name="comment" maxlength="500"><?php echo $value[comment] ?></textarea>
</fieldset>
<fieldset style="font-size:12px">
	<legend>关 键 词（限100字内）</legend>
	<textarea cols="60px" rows="2px" name="key_words" maxlength="100"><?php echo $value[key_words] ?></textarea>
</fieldset>
<fieldset style="font-size:12px">
<table border="0" style="font-size:12px">
  <tr>
    <td>起始时间</td>
    <td><input type="date" name="start_time" value=<?php echo $value[start_time] ?> ></td>
    <td>截止时间</td>
    <td><input type="date" name="finish_time" value=<?php echo $value[finish_time] ?> ></td>
  </tr>
</table>
</fieldset>
<h3><input style="font-size:14px" type="submit" name="submit" value="提交"></h3>
</form>
<center><input type="button" onClick="history.go(-1)" value="返回"></center>
</html>
