<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$user = $_SESSION[user];
	if ($_GET['project_id'] !== NULL){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from project_contract where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		//不同用户进入此页面的权限
		permissionBlocker('edit',$value[status]);
		
		//反序列化数组存入的数据
		$arrayLong_list = array_field_inDB('project_contract');
		foreach ($arrayLong_list as $arrayLong) {
			${$arrayLong} = unserialize($value[$arrayLong]);
		}
		$array_num_unit = count($array_unit_danweimingcheng);
		
		
		//控制下拉表单的预设值
	
		$project_class_list = array("key_industry","key_agriculture","key_society");
		foreach ($project_class_list as $cla) {
			if ($value[project_class] == $cla) {
				${$cla} = $cla." selected";
			} else {
				${$cla} = $cla;
			}
		}
		
		$industry_type_list = array("A01","A02","A03","A04","A05","B06","B07","B08","B09","B10","B11","B12","C13","C14","C15","C16","C17","C18","C19","C20","C21","C22","C23","C24","C25","C26","C27","C28","C29","C30","C31","C32","C33","C34","C35","C36","C37","C39","C40","C41","C42","C43","D44","D45","D46","E47","E48","E49","F50","F51","G52","G53","G54","G55","G56","G57","G58","G59","G60","H61","H62","H63","H64","H65","H67","I68","I70","J72","J73","J74","K75","K76","K78","K79","K80","K81","K82","K83","K84","L85","L86","L87","M89","M90","M91","N92","N93","O94","O95","O96","O97","P99");
		foreach ($industry_type_list as $cla) {
			if ($value[industry_type] == $cla) {
				${$cla} = $cla." selected";
			} else {
				${$cla} = $cla;
			}
		}
		
		$tech_resource_list = array('independent_develop','cooperation_develop','chanxueyan_cooperation','internal_cooperation','foreign_technology');
		foreach ($tech_resource_list as $cla) {
			if ($value[tech_resource] == $cla) {
				${$cla} = $cla." selected";
			} else {
				${$cla} = $cla;
			}
		}
		
		$innovation_format_list = array('self_innovation','integrate_innovation','re_innovation','absorb_innovation','tech_transform');
		foreach ($innovation_format_list as $cla) {
			if ($value[innovation_format] == $cla) {
				${$cla} = $cla." selected";
			} else {
				${$cla} = $cla;
			}
		}
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
?>
<!DOCTYPE HTML5>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<title>项目申请</title>
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../js/my_javascript.js"></script>
<script type="text/javascript" src="../js/project_contract.js"></script>
<style type="text/css">

.content ul, .content ol { 
	padding: 0 15px 15px 40px; /* 此填充反映上述标题和段落规则中的右填充。填充放置于下方可用于间隔列表中其它元素，置于左侧可用于创建缩进。您可以根据需要进行调整。 */
}

/* ~~ 导航列表样式（如果选择使用预先创建的 Spry 等弹出菜单，则可以删除此样式） ~~ */
ul.nav_menu {
	position:fixed;
	left:10px;
	top:10px;
	float:left;
	list-style: none; /* 这将删除列表标记 */
	border-top: 1px solid #666; /* 这将为链接创建上边框 – 使用下边框将所有其它项放置在 LI 中 */
	margin-bottom: 15px; /* 这将在下面内容的导航之间创建间距 */
	margin-left:-30px;
	margin-right:10px;
}
ul.nav_menu li {
	border-bottom: 1px solid #666; /* 这将创建按钮间隔 */
	font-family:"宋体";
	font-size:14px;
	line-height:20px;
	text-align:center;
}
ul.nav_menu a, ul.nav_menu a:visited { /* 对这些选择器进行分组可确保链接即使在访问之后也能保持其按钮外观 */
	display: block; /* 这将为链接赋予块属性，使其填满包含它的整个 LI。这样，整个区域都可以响应鼠标单击操作。 */
	width: 110px;  /*此宽度使整个按钮在 IE6 中可单击。如果您不需要支持 IE6，可以删除它。请用侧栏容器的宽度减去此链接的填充来计算正确的宽度。 */
	text-decoration: none;
	color:#000;
}

.form_area {
	position:fixed;
	left:132px;
	top:17px;
}


</style>
</head>
<form method="post" action="../proc/submit_form.php?project_id=<?php echo $project_id; ?>" name="projectForm" onSubmit="return formCheck()">
<input type="hidden" value="edit" name="apply_edit" >
<input type="hidden" value="project_contract" name="submit_type" >
<ul class="nav_menu">
<li><a href="#" onClick="fr_hide('jibenxiangmu')">项目基本情况</button></a></li>
<li><a href="#" onClick="fr_hide('chengdandanwei')">项目承担单位</button></a></li>
<li><a href="#" onClick="fr_hide('xiangmuchengyuan')">负责人及成员</button></a></li>
<li><a href="#" onClick="fr_hide('xiangmuneirong')">项目主要内容</button></a></li>
<li><a href="#" onClick="fr_hide('zhibiao')">技术经济指标</button></a></li>
<li><a href="#" onClick="fr_hide('chengguoxingshi')">成果提供形式</button></a></li>
<li><a href="#" onClick="fr_hide('jinduanpai')">进度安排</button></a></li>
<li><a href="#" onClick="fr_hide('jingfeilaiyuan')">经费来源</button></a></li>
<li><a href="#" onClick="fr_hide('zhichuyusuan')">经费支出预算</button></a></li>
<li><a href="#" onClick="fr_hide('yiqishebei')">需增添仪器设备</button></a></li>
<li><a href="#" onClick="fr_hide('hetongtiaokuan')">合同其他条款</button></a></li>
<li><bold align="center"><input style="font-size:14px;color:#30F" type="submit" value="保存"></bold></li>
</ul>
<div class="form_area">
<fieldset id="jibenxiangmu">
    <legend>一、项目基本情况</legend>
    项目名称: <br/><input type="text" name="project_name" value="<?php echo $value[project_name]; ?>" id='must[0]' onBlur="check_project_exist(document.projectForm.project_name.value)"/><span></span>
    <table cellpadding="0px">
    <tr>
    <td align="justify">项目计划类别：<br/><select name="project_class" value="<?php echo $value[project_class]; ?>">
    			<option value="" ></option>
    			<option value=<?php echo $key_industry; ?> >重点工业项目</option>
    			<option value=<?php echo $key_agriculture; ?> >重点农业项目</option>
                <option value=<?php echo $key_society; ?> >重点社会发展项目</option>
    			</select></td>

	<td align="justify">项目行业分类：<br/>
	<select id="select_industry_class" name="industry_type" >
			<option value=""></option>
		<optgroup label="农、林、牧、渔业">
			<option value=<?php echo $A01; ?>>农业</option>
	    	<option value=<?php echo $A02; ?>>林业</option>
	    	<option value=<?php echo $A03; ?>>畜牧业</option>
	    	<option value=<?php echo $A04; ?>>渔业</option>
	        <option value=<?php echo $A05; ?>>农、林、牧、渔服务业</option>
	    </optgroup>
		<optgroup label="采掘业">
			<option value=<?php echo $B06; ?>>煤炭采选业</option>
	    	<option value=<?php echo $B07; ?>>石油和天然气开采业</option>
	    	<option value=<?php echo $B08; ?>>黑色金属矿采选业</option>
	    	<option value=<?php echo $B09; ?>>有色金属矿采选业</option>
	        <option value=<?php echo $B10; ?>>非金属矿采选业</option>
	        <option value=<?php echo $B11; ?>>其他矿采选业</option>
	        <option value=<?php echo $B12; ?>>木材及竹材采运业</option>
	    </optgroup>
	    <optgroup label="制造业">
			<option value=<?php echo $C13; ?>>食品加工业</option>
	    	<option value=<?php echo $C14; ?>>食品制造业</option>
	    	<option value=<?php echo $C15; ?>>饮料制造业</option>
    		<option value=<?php echo $C16; ?>>烟草加工业</option>
    	    <option value=<?php echo $C17; ?>>纺织业</option>
	        <option value=<?php echo $C18; ?>>服装及其他纤维制品制造业</option>
	        <option value=<?php echo $C19; ?>>皮革、毛皮、羽绒及制品业</option>
	        <option value=<?php echo $C20; ?>>木材加工及竹、藤、棕、草制品业</option>
	        <option value=<?php echo $C21; ?>>家具制造业</option>
	        <option value=<?php echo $C22; ?>>造纸及纸制品业</option>
	        <option value=<?php echo $C23; ?>>印刷业、记录媒介的复制</option>
	        <option value=<?php echo $C24; ?>>文教体育用品制造业</option>
	        <option value=<?php echo $C25; ?>>石油加工及炼焦业</option>
	        <option value=<?php echo $C26; ?>>化学原料及化学制品制造业</option>
	        <option value=<?php echo $C27; ?>>医药制造业（含生物制造业）</option>
	        <option value=<?php echo $C28; ?>>化学纤维制造业</option>
	        <option value=<?php echo $C29; ?>>橡胶制品业</option>
	        <option value=<?php echo $C30; ?>>塑料制品业</option>
	        <option value=<?php echo $C31; ?>>非金属矿物制品业</option>
	        <option value=<?php echo $C32; ?>>黑色金属冶炼及压延加工业</option>
	        <option value=<?php echo $C33; ?>>有色金属冶炼及压延加工业</option>
	        <option value=<?php echo $C34; ?>>金属制品业（含日用金属制品业）</option>
	        <option value=<?php echo $C35; ?>>普通机械制造业</option>
	        <option value=<?php echo $C36; ?>>专用设备制造业</option>
	        <option value=<?php echo $C37; ?>>交通运输设备制造业</option>
	        <option value=<?php echo $C39; ?>>武器弹药制造业</option>
	        <option value=<?php echo $C40; ?>>电气机械及器材制造业</option>
	        <option value=<?php echo $C41; ?>>电子及通信设备制造业</option>
	        <option value=<?php echo $C42; ?>>仪器仪表及文化、办公用机械制造业</option>
	        <option value=<?php echo $C43; ?>>其他制造业</option>
	    </optgroup>
	    <optgroup label="电力、煤气及水的生产和供应业">
			<option value=<?php echo $D44; ?>>电力、蒸气、水的生产和供应业</option>
	    	<option value=<?php echo $D45; ?>>煤气生产和供应业</option>
	    	<option value=<?php echo $D46; ?>>自来水的生产和供应业</option>
	    </optgroup>
	    <optgroup label="建筑业">
			<option value=<?php echo $E47; ?>>土木工程建筑业</option>
	    	<option value=<?php echo $E48; ?>>线路、管道和设备安装业</option>
	    	<option value=<?php echo $E49; ?>>装修装饰业</option>
	    </optgroup>
	    <optgroup label="地质勘查业、水利管理业">
			<option value=<?php echo $F50; ?>>地质勘查业</option>
	    	<option value=<?php echo $F51; ?>>水利管理业</option>
	    </optgroup>
	    <optgroup label="交通运输业、仓储及邮电通信业">
			<option value=<?php echo $G52; ?>>铁路运输业</option>
	    	<option value=<?php echo $G53; ?>>公路运输业</option>
	    	<option value=<?php echo $G54; ?>>管道运输业</option>
	    	<option value=<?php echo $G55; ?>>水上运输业</option>
	        <option value=<?php echo $G56; ?>>航空运输业</option>
	        <option value=<?php echo $G57; ?>>交通运输辅助业</option>
	        <option value=<?php echo $G58; ?>>其他交通运输业</option>
	        <option value=<?php echo $G59; ?>>仓储业</option>
	        <option value=<?php echo $G60; ?>>邮电通信业</option>
	    </optgroup>
	    <optgroup label="批发和零售贸易、餐饮业">
	        <option value=<?php echo $H61; ?>>仪器、饮料、烟草和家庭日用品批发业</option>
	        <option value=<?php echo $H62; ?>>能源、材料和机械电子设备批发业</option>
	        <option value=<?php echo $H63; ?>>其他批发业</option>
	        <option value=<?php echo $H64; ?>>零售业</option>
	        <option value=<?php echo $H65; ?>>商业经纪与代理业</option>
	        <option value=<?php echo $H67; ?>>餐饮业</option>
	    </optgroup>
	    <optgroup label="金融、保险业">
	        <option value=<?php echo $I68; ?>>金融业</option>
	        <option value=<?php echo $I70; ?>>保险业</option>
	    </optgroup>
	    <optgroup label="房地产业">
	        <option value=<?php echo $J72; ?>>房地产开发与经营业</option>
	        <option value=<?php echo $J73; ?>>房地产管理业</option>
	        <option value=<?php echo $J74; ?>>房地产经纪与代理业</option>
	    </optgroup>
	    <optgroup label="社会服务业">
	        <option value=<?php echo $K75; ?>>公共设施服务业</option>
	        <option value=<?php echo $K76; ?>>居民服务业</option>
	        <option value=<?php echo $K78; ?>>旅馆业</option>
	        <option value=<?php echo $K79; ?>>租赁服务业</option>
	        <option value=<?php echo $K80; ?>>旅游业</option>
	        <option value=<?php echo $K81; ?>>娱乐服务业</option>
	        <option value=<?php echo $K82; ?>>信息、咨询服务业</option>
	        <option value=<?php echo $K83; ?>>计算机应用服务业</option>
	        <option value=<?php echo $K84; ?>>其他社会服务业</option>
	    </optgroup>
	    <optgroup label="卫生、体育和社会福利业">
	        <option value=<?php echo $L85; ?>>卫生</option>
	        <option value=<?php echo $L86; ?>>体育</option>
	        <option value=<?php echo $L87; ?>>社会福利保障业</option>
	    </optgroup>
	    <optgroup label="教育、文化艺术及广播电影电视业">
	        <option value=<?php echo $M89; ?>>教育</option>
	        <option value=<?php echo $M90; ?>>文化艺术业</option>
	        <option value=<?php echo $M91; ?>>广播电影电视业</option>
	    </optgroup>
	    <optgroup label="科学研究和综合技术服务业">
	        <option value=<?php echo $N92; ?>>科学研究业</option>
	        <option value=<?php echo $N93; ?>>综合技术服务业（含气象、地震、测绘等）</option>
	    </optgroup>
	    <optgroup label="国家机关、政党机关和社会团体">
	        <option value=<?php echo $O94; ?>>国家机关</option>
	        <option value=<?php echo $O95; ?>>政党机关</option>
	        <option value=<?php echo $O96; ?>>社会团体</option>
        	<option value=<?php echo $O97; ?>>基层群众自治组织</option>
    	</optgroup>
	    <optgroup label="其他行业">
       	 <option value=<?php echo $P99; ?>>其他行业</option>
    	</optgroup>  
	</select>
    </td>
    <tr>
    <td>项目学科领域：<br><input type="text" name="science_domain" value="<?php echo $value[science_domain]; ?>"/></td>
    <td>项目管理领域：<br><input type="text" name="manager_domain" value="<?php echo $value[manager_domain]; ?>"/></td>
    </tr>
	<td align="justify">项目技术来源：<br/><select name="tech_resource" value="<?php echo "$value[tech_resource]";?>" >
    			<option value=""></option>
    			<option value=<?php echo $independent_develop; ?>  >自主开发</option>
                <option value=<?php echo $cooperation_develop; ?>  >合作开发</option>
    			<option value=<?php echo $chanxueyan_cooperation; ?>  >产学研联合开发</option>
    			<option value=<?php echo $internal_cooperation; ?>  >引进国内技术</option>
    			<option value=<?php echo $foreign_technology; ?>  >引进国外技术</option>
    			</select></td>
	<td align="justify">技术创新方式：<br/><select name="innovation_format" value="<?php echo $value[innovation_format]; ?>">
    			<option value=""></option>
    			<option value=<?php echo $self_innovation; ?> >自主创新</option>
    			<option value=<?php echo $integrate_innovation; ?> >集成创新</option>
    			<option value=<?php echo $re_innovation; ?> >在引进、消化吸收基础上的再创新</option>
    			<option value=<?php echo $tech_transform; ?> >科技成果转化和产业化</option>
    			</select></td>
    <tr>
	<td align="justify">开始日期：<br/><input type="date" name="start_date" value="<?php echo $value[start_date]; ?>"/></td>
	<td align="justify">完成日期：<br/><input type="date" name="finish_date" value="<?php echo $value[finish_date]; ?>"/></td>
	</tr>
    </table>
    

    <fieldset>
    <legend>预计经济效益</legend>
    <table cellpadding="0px" border="0">
    <tr>
    <td align="justify">年增产值（万元）：<br/><input type="text" name="nianzengchanzhi" value="<?php echo $value[nianzengchanzhi]; ?>"/></td>
    <td align="justify">年增利润（万元）：<br/><input type="text" name="nianzenglirun" value="<?php echo $value[nianzenglirun]; ?>"/></td>
    <td align="justify">年增税金（万元）：<br/><input type="text" name="nianzengshuijin" value="<?php echo $value[nianzengshuijin]; ?>"/></td>
    </tr>
    <tr>
    <td align="justify">年创汇（万美元）：<br/><input type="text" name="nianchuanghui" value="<?php echo $value[nianchuanghui]; ?>"/></td>
    <td align="justify">年节汇（万美元）：<br/><input type="text" name="nianjiehui" value="<?php echo $value[nianjiehui]; ?>"/></td>
    </tr>
    </table>
  	</fieldset>

	<fieldset>
    <legend>项目成果</legend>
    <table cellpadding="0px" border="0" class='wideForm'>
    <tr>
    <td align="justify">论文数：<br/><input type="text" name="lunwenshu" value="<?php echo $value[lunwenshu]; ?>"/></td>
    <td align="justify">专利申请数：<br/><input type="text" name="zhuanlishenqing" value="<?php echo $value[zhuanlishenqing]; ?>"/></td>
    <td align="justify">专利授权数：<br/><input type="text" name="zhuanlishouquan" value="<?php echo $value[zhuanlishouquan]; ?>"/></td>
    <td align="justify">其中发明专利申请数：<br/><input type="text" name="famingzhuanlishenqing" value="<?php echo $value[famingzhuanlishenqing]; ?>"/></td>
    <td align="justify">其中发明专利授权数：<br/><input type="text" name="famingzhuanlishouquan" value="<?php echo $value[famingzhuanlishouquan]; ?>"/></td>
    </tr>
    </table>
  	</fieldset>
    
    <fieldset>
    <legend>人才引进和培养</legend>
    <table cellpadding="0px" border="0">
    <tr>
    <td align="justify">研究生培养：<br/><input type="text" name="yanjiusheng" value="<?php echo $value[yanjiusheng]; ?>"/></td>
    <td align="justify">中级及以下：<br/><input type="text" name="zhongjiyixia" value="<?php echo $value[zhongjiyixia]; ?>"/></td>
    <td align="justify">副高级及以上：<br/><input type="text" name="fugaoyishang" value="<?php echo $value[fugaoyishang]; ?>"/></td>
    </tr>
    </table>
  	</fieldset>

  	备注：<br/><textarea name="beizhu" cols="80" rows="5"><?php echo $value[beizhu]; ?></textarea>
</fieldset>

<fieldset id="chengdandanwei">
    <legend>二、项目承担单位</legend>
	<fieldset>
	<legend>第一申请单位</legend>
	<table cellpadding="6px" border="0">
    <tr>
    <td align="justify">单位名称：<br/><input type="text" name="danweimingcheng" value="<?php echo $value[danweimingcheng]; ?>"/></td>
    <td align="justify">法人代码：<br/><input type="text" name="farendaima" value="<?php echo $value[farendaima]; ?>"/></td>
    <td align="justify">单位类型：<br/><input type="text" name="danweileixing" value="<?php echo $value[danweileixing]; ?>"/></td>
	</tr>
    <tr>
    <td align="justify">详细地址：<br/><input type="text" name="xiangxidizhi" value="<?php echo $value[xiangxidizhi]; ?>"/></td>
    <td align="justify">邮政编码：<br/><input type="text" name="youzhengbianma" value="<?php echo $value[youzhengbianma]; ?>"/></td>
    <td align="justify">联系人：<br/><input type="text" name="diyidanweilianxiren" value="<?php echo $value[diyidanweilianxiren]; ?>"/></td>
    <td align="justify">EMAIL：<br/><input type="text" name="diyidanweiemail" value="<?php echo $value[diyidanweiemail]; ?>"/></td>
    </tr>
    <tr>
    <td align="justify">电话/传真：<br/><input type="text" name="dianhuachuanzhen" value="<?php echo $value[dianhuachuanzhen]; ?>"/></td>
    <td align="justify">手机：<br/><input type="text" name="shouji" value="<?php echo $value[shouji]; ?>"/></td>
    <td align="justify">开户银行：<br/><input type="text" name="kaihuyinhang" value="<?php echo $value[kaihuyinhang]; ?>"/></td>
    <td align="justify">银行帐号：<br/><input type="text" name="yinhangzhanghao" value="<?php echo $value[yinhangzhanghao]; ?>"/></td>
    </tr>
	</table>    
	</fieldset>
	<fieldset>
	<legend>合作单位</legend>
    合作单位总数<br>
    <input type="text" id="hezuo" name="hezuodanweizongshu" value="<?php echo $value[hezuodanweizongshu]; ?>" onBlur="define_unitNum()"/><br/>
	承担单位数 <input type="text" name="chengdandanweishu" value="<?php echo $value[chengdandanweishu]; ?>"/>
	参加单位数 <input type="text" name="canjiadanweishu" value="<?php echo $value[canjiadanweishu]; ?>"/>
	<table border="0" id="unit">
	<tr>
	<th>&nbsp;</th><th>单位名称</th><th>法人代码</th><th>职责*(*: 0-承担，1－参加)</th>
	</tr>
    <?php 
	$hezuo_col = $value[hezuodanweizongshu];
	for ($i=0;$i<$hezuo_col;$i++){
		$ii=$i+1;
		echo "<tr>
		<td>$ii.</td>
		<td><input type='text' name=array_unit_danweimingcheng[$i] value=$array_unit_danweimingcheng[$i]></td>
		<td><input type='text' name=array_unit_farendaima[$i] value=$array_unit_farendaima[$i]></td>
		<td><input type='text' name=array_unit_zhize[$i] value=$array_unit_zhize[$i]></td>
		</tr>";
	}
	?>
	</table>
	<br />
	</fieldset>
</fieldset>

<fieldset id="xiangmuchengyuan">
	<legend>三、项目负责人及项目组成员</legend>
	<fieldset>
	<legend>项目负责人</legend>
	<table cellpadding="6px" border="0">
    <tr>
    <td align="justify">姓名<br/><input type="text" name="fuzeren_name" value="<?php echo $value[fuzeren_name]; ?>"/></td>
    <td align="justify">身份证号码<br/><input type="text" name="fuzeren_shenfenzheng" value="<?php echo $value[fuzeren_shenfenzheng]; ?>"/></td>
    <td align="justify">工作单位<br/><input type="text" name="fuzeren_danwei" value="<?php echo $value[fuzeren_danwei]; ?>"/></td>
    <td align="justify">法人代码<br/><input type="text" name="fuzeren_farendaima" value="<?php echo $value[fuzeren_farendaima]; ?>"/></td>
	</tr>
	<tr>
    <td align="justify">详细地址<br/><input type="text" name="fuzeren_adress" value="<?php echo $value[fuzeren_adress]; ?>"/></td>
    <td align="justify">邮政编码<br/><input type="text" name="fuzeren_zcode" value="<?php echo $value[fuzeren_zcode]; ?>"/></td>
    <td align="justify">移动电话<br/><input type="text" name="fuzeren_mobile" value="<?php echo $value[fuzeren_mobile]; ?>"/></td>
    <td align="justify">EMAIL<br/><input type="text" name="fuzeren_email" value="<?php echo $value[fuzeren_email]; ?>"/></td>
    </tr>
    <tr>
    <td align="justify">学历<br/><input type="text" name="fuzeren_xueli" value="<?php echo $value[fuzeren_xueli]; ?>"/></td>
    <td align="justify">学位<br/><input type="text" name="fuzeren_xuewei" value="<?php echo $value[fuzeren_xuewei]; ?>"/></td>
    <td align="justify">职称<br/><input type="text" name="fuzeren_zhicheng" value="<?php echo $value[fuzeren_zhicheng]; ?>"/></td>
    <td align="justify">现从事专业<br/><input type="text" name="fuzeren_zhuanye" value="<?php echo $value[fuzeren_zhuanye]; ?>"/></td>
	</tr>
	<tr>
	</table>
	</fieldset>

	<fieldset>
	<legend>项目组成员</legend>
    总共项目组人数：<input type="text" id="member_num" name="member_number" value="<?php echo $value[member_number]; ?>"/>
	<table border="1" id="member" cellspacing="0" class='wideForm'>
	<tr>
	<th>&nbsp;</th><th>姓名</th><th>身份证号码</th><th>所在单位</th><th>职称</th><th>从事专业</th><th>在本项目中分工</th><th>年参加项目工作时间(月)</th>
	</tr>
    <?php 
	for ($i=0;$i<$value[member_number];$i++){
		$ii=$i+1;
		echo "<tr>
		<td>$ii.</td>
		<td><input type='text' name=array_member_name[$i] value='$array_member_name[$i]' style='width:60px' ></td>
		<td><input type='text' name=array_member_shenfenzheng[$i] value='$array_member_shenfenzheng[$i]'></td>
		<td><input type='text' name=array_member_gongzuodanwei[$i] value='$array_member_gongzuodanwei[$i]' style='width:80px'></td>
		<td><input type='text' name=array_member_zhicheng[$i] value='$array_member_zhicheng[$i]' style='width:80px' ></td>
		<td><input type='text' name=array_member_zhuanye[$i] value='$array_member_zhuanye[$i]' style='width:80px'></td>
		<td><input type='text' name=array_member_fengong[$i] value='$array_member_fengong[$i]' style='width:80px'></td>
		<td><input type='text' name=array_member_canjiashijian[$i] value='$array_member_canjiashijian[$i]'></td>
		</tr>";
	}
	?>
	</table>
	</fieldset>
</fieldset>

<fieldset id="xiangmuneirong">
<legend>
四、项目主要内容（关键技术）
</legend>
<textarea type="text" name="xiangmuneirong" cols="100" rows="40"><?php echo $value[xiangmuneirong]; ?></textarea>
</fieldset>

<fieldset id="zhibiao">
<legend>
五、项目主要技术、经济指标
</legend>
<textarea type="text" name="zhibiao" cols="100" rows="40"><?php echo $value[zhibiao]; ?></textarea>
</fieldset>

<fieldset id="chengguoxingshi">
<legend>
六、项目成果提供形式
</legend>
<textarea type="text" name="chengguoxingshi" cols="100" rows="40"><?php echo $value[chengguoxingshi]; ?></textarea>
</fieldset>

<fieldset id="jinduanpai">
<legend>七、项目分年度（阶段）进度安排</legend>
<table id="progress">
<tr>
<th>起始年月</th><th>计划进度</th><th>投入经费（万元）</th>
</tr>
<?php
	$n=6;
	for($i=0;$i<=$n;$i++){
		echo "<tr id='tr_$i'><td>
			<input type='date' name=array_progress_start[$i] value='$array_progress_start[$i]' />至<input type='date' name=array_progress_end[$i] value='$array_progress_end[$i]' /></td>
			<td><textarea name=array_progress_content[$i] cols='40' rows='2'>$array_progress_content[$i]</textarea></td>
			<td><input type='text' name=array_progress_fund[$i] value=$array_progress_fund[$i] ></td>
			</tr>";
	}
?>
</table>
<button type='button' id="progressinc">增加项目进度栏</button>
<button type='button' id="progressdec">减少项目进度栏</button>
<br>
<br>
</fieldset>

<fieldset id="jingfeilaiyuan">
<legend>八、项目经费来源</legend>
1．本项目研发总经费<input type="text" name="zongjingfei" value="<?php echo $value[zongjingfei]; ?>" style="width:80px" />万元，其中:甲方补助<input type="text" name="jiafangbuzhu" value="<?php echo $value[jiafangbuzhu]; ?>" style="width:80px" />万元，乙方自筹<input type="text" name="yifangzichou" value="<?php echo $value[yifangzichou]; ?>" style="width:80px" />万元。<br>
<br>
2．甲方经费拨付计划：<br>
<table border="1" cellspacing=0 cellpadding="8">
<tr>
<th>拨款时间</th>
<th>合计</th>
<?php 
$n = 4;
for( $i=0;$i<$n; $i++) {
	echo "<th><input type='text' name=array_bokuan_nian[$i] value='$array_bokuan_nian[$i]' style='width:60px' />年<input type='text' name=array_bokuan_yue[$i] value='$array_bokuan_yue[$i]' style='width:40px' />月</th>";
}
?>
</tr>
<tr>
<th>金额(万元)</th>
<?php 
$n = 5;
for( $i=0;$i<$n; $i++) {
	echo "<td><input type='text' name=array_bokuan_qian[$i] value='$array_bokuan_qian[$i]' style='width:130px' /></td>";
}
?>
</tr>
</table>
甲方首期拨款后，其余经费根据项目执行情况，分期拨给乙方。<br>
<br>
3．乙方自筹经费到位计划：<br>
<table border="1" cellspacing=0 cellpadding="8">
<tr>
<th>&nbsp;</th>
<th>总金额（万元）</th>
<?php 
$n = 3;
for( $i=0;$i<$n; $i++) {
	echo "<th><input type='text' name=array_zichou_nian[$i] value='$array_zichou_nian[$i]' style='width:80px' />年<input type='text' name=array_zichou_yue[$i] value='$array_zichou_yue[$i]' style='width:40px' />月</th>";
}
?>
</tr>
<tr>
<th>乙方自筹</th>
<?php 
$n = 4;
for( $i=0;$i<$n; $i++) {
	echo "<td><input type='text' name=array_zichou_qian[$i] value='$array_zichou_qian[$i]' /></td>";
}
?>
</tr>
</table>
</fieldset>


<fieldset id="zhichuyusuan">
<legend> 九、项目经费支出预算</legend>
<table border="1" cellspacing=0 cellpadding="8">
<tr>
<th>经费开支科目</th><th>预算经费总额(万元)</th><th>其中县财政经费（万元）</th>
</tr><tr>
<th>1.设备费</th>
<td><input type='text' name=array_jingfei_yusuan[0] value="<?php echo $array_jingfei_yusuan[0]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[0] value="<?php echo $array_jingfei_caizheng[0]; ?>"/></td>
</tr><tr>
<th>2.材料费</th>
<td><input type='text' name=array_jingfei_yusuan[1] value="<?php echo $array_jingfei_yusuan[1]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[1] value="<?php echo $array_jingfei_caizheng[1]; ?>"/></td>
</tr><tr>
<th>3.测试化验加工费</th>
<td><input type='text' name=array_jingfei_yusuan[2] value="<?php echo $array_jingfei_yusuan[2]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[2] value="<?php echo $array_jingfei_caizheng[2]; ?>"/></td>
</tr><tr>
<th>4.燃料动力费</th>
<td><input type='text' name=array_jingfei_yusuan[3] value="<?php echo $array_jingfei_yusuan[3]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[3] value="<?php echo $array_jingfei_caizheng[3]; ?>"/></td>
</tr><tr>
<th>5.差旅费</th>
<td><input type='text' name=array_jingfei_yusuan[4] value="<?php echo $array_jingfei_yusuan[4]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[4] value="<?php echo $array_jingfei_caizheng[4]; ?>"/></td>
</tr><tr>
<th>6.会议费 </th>
<td><input type='text' name=array_jingfei_yusuan[5] value="<?php echo $array_jingfei_yusuan[5]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[5] value="<?php echo $array_jingfei_caizheng[5]; ?>"/></td>
</tr><tr>
<th>7.合作、协作研究与交流费</th>
<td><input type='text' name=array_jingfei_yusuan[6] value="<?php echo $array_jingfei_yusuan[6]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[6] value="<?php echo $array_jingfei_caizheng[6]; ?>"/></td>
</tr><tr>
<th>8.出版/文献/信息传播/知识产权事务费</th>
<td><input type='text' name=array_jingfei_yusuan[7] value="<?php echo $array_jingfei_yusuan[7]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[7] value="<?php echo $array_jingfei_caizheng[7]; ?>"/></td>
</tr><tr>
<th>9.人员劳务费</th>
<td><input type='text' name=array_jingfei_yusuan[8] value="<?php echo $array_jingfei_yusuan[8]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[8] value="<?php echo $array_jingfei_caizheng[8]; ?>"/></td>
</tr><tr>
<th>10.专家咨询费</th>
<td><input type='text' name=array_jingfei_yusuan[9] value="<?php echo $array_jingfei_yusuan[9]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[9] value="<?php echo $array_jingfei_caizheng[9]; ?>"/></td>
</tr><tr>
<th>11.管理费</th>
<td><input type='text' name=array_jingfei_yusuan[10] value="<?php echo $array_jingfei_yusuan[10]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[10] value="<?php echo $array_jingfei_caizheng[10]; ?>"/></td>
</tr><tr>
<th>12．其他费用</th>
<td><input type='text' name=array_jingfei_yusuan[11] value="<?php echo $array_jingfei_yusuan[11]; ?>"/></td>
<td><input type='text' name=array_jingfei_caizheng[11] value="<?php echo $array_jingfei_caizheng[11]; ?>"/></td>
</tr>
</table>
</fieldset>

<fieldset id="yiqishebei">
<legend> 十、需增添的仪器设备</legend>
<table border="1" cellspacing=0 cellpadding="8" class='wideForm'>
<tr>
<th>名称</th><th>数量</th><th>单价</th><th>县财政拨款</th><th>自筹或其他</th><th>用途说明</th>
</tr>
<?php
$n=6;
for ($i=0;$i<$n;$i++) {
	echo "
	<tr>
	<td><input type='text' name=array_yiqi_name[$i] value='$array_yiqi_name[$i]' /></td>
	<td><input type='text' name=array_yiqi_num[$i] value='$array_yiqi_num[$i]' style='width:40px'/></td>
	<td><input type='text' name=array_yiqi_price[$i] value='$array_yiqi_price[$i]' style='width:60px' /></td>
	<td><input type='text' name=array_yiqi_bokuan[$i] value='$array_yiqi_bokuan[$i]' /></td>
	<td><input type='text' name=array_yiqi_zichou[$i] value='$array_yiqi_zichou[$i]' /></td>
	<td><input type='text' name=array_yiqi_yongtu[$i] value='$array_yiqi_yongtu[$i]' /></td>
	</tr>
	";
}
?>
</table>
</fieldset>


</form>
<fieldset id="hetongtiaokuan">
<legend>十一、合同其他条款</legend>
<ol>
<li>各方应严格遵守本合同的各项条款。因合同执行过程中出现的客观原因，任何一方认为有必要变更合同条款内容的，需经协商一致。 </li>
<li>甲方有权按照合同的要求，监督、检查乙方项目进展和经费使用情况，乙方应予以配合。乙方应按要求向甲方报送项目执行和经费使用情况。 </li>
<li>乙方有权按照合同的要求组织实施项目、使用项目经费。乙方未按本合同落实自筹经费，或未按规定使用项目经费的，甲方有权暂停拨款直至解除合同，并收回已投入的经费。 </li>
<li>甲方由多家单位组成的(如联合招标项目)，各方的出资数额、方式、时间以及其它相关权利和义务需单独订立协议，作为本合同的附件，视作本合同的组成部分。</li>
<li>乙方与合作单位的合作协议（包括各方承担的任务、经费分配、研究成果的归属等），作为本合同的附件，视作本合同的组成部分。</li>
<li>根据《龙游县科技计划项目申报管理办法（试行）》（龙政办发〔2010〕62号）的规定，乙方完成本项目任务后，应及时申请验收或结题。</li>
<li>乙方在合同执行过程中，确因技术、市场等不可抗拒原因，必须调整合同相关任务或指标的，应及时书面提出申请，甲方审批同意后，按批准后合同任务和指标执行。未经批准的，按原合同任务和指标要求完成。</li>
<li>成果的权属和保密。本项目研究取得的技术成果，其知识产权归属及成果转化，按国家和本省的有关规定执行。项目技术成果涉及国家利益的，乙方在有偿转让之前，应经过甲方的审查批准；涉及国家机密的，按国家《中华人民共和国保守国家秘密法》和《科学技术保密规定》有关规定执行。 </li>
<li>本合同文本一式六份，分存甲方、乙方及有关单位。</li>
</ol>
</fieldset>
<center><input type="button" onClick="history.go(-1)" value="返回"></center>
</div>
</html>
