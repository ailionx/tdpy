<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$company_name = $_SESSION[company_name];
	$query = "select * from company_info where company_name='$company_name'";
	$result = mysql_query($query);
	$value = mysql_fetch_array($result);

// 从mysql中读取checkbox的内容并显示到页面，又是array，又要unserialize，又是list，又要php控制html显示，烦死了！
	$arrayLong_list = array_field_inDB('company_info');
	foreach ($arrayLong_list as $arrayLong) {
		${$arrayLong} = unserialize($value[$arrayLong]);
	}
	$array_company_class_list = array(hightech_country,hightech_city,shengchuangxinshifan,shengchuangxinxingshidian,shengkejixingzhongxiao,shengzhuanlishifan,shengnongyekeji,shizhuanlishifan,shichuangxinxing,xiankejixing,class_other);
	foreach ($array_company_class_list as $cla) {
		if ($array_company_class[$cla]) {
			${$cla}= $cla." checked";
		} else {
			${$cla}= $cla;
		}
	}
	$array_reserch_class_list = array( shengjiqiyeyanjiuyuan,shengjigaoxinjishuqiyefazhanzhongxin,shengjinongyeqiyekejiyanfazhongxin,shengjiquyukejichuangxinfuwuzhongxin,shengjidayuanmingxiaogongjianchuangxinzaiti,shijigongchengjishuyanjiukaifazhongxin,qiyeshuyanfajigou,rd_other);
	foreach ($array_reserch_class_list as $cla) {
		if ($array_reserch_class[$cla]) {
			${$cla}= $cla." checked";
		} else {
			${$cla}= $cla;
		}
	}

?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<title>科技型企业基本信息表</title>
<script type="text/javascript" src="../../conf.js"></script>
</head>

<h1><center>科技型企业基本信息表</center></h1>
<form action="../controllers/submitCompanyInfo.php" method="post">
<input type="hidden" value="company_info" name="submit_type">
<input type="hidden" value="edit" name="apply_edit">
<input type="hidden" value="<?php echo $value['user'] ?>" name="user">
<input type="hidden" value="<?php echo $value['id'] ?>" name="id">
<fieldset style="font-size:12px">
<table border="0" style="font-size:12px">
  <tr>
    <td>企业名称：（无法修改）<br><input type="text" readonly name="company_name" value=<?php echo "$value[company_name]"; ?> ></td>
  </tr>
  <tr>
    <td>成立时间：<br><input type="text" name="establish_time" value=<?php echo "$value[establish_time]"; ?> ></td>
  </tr>
  <tr>
    <td>企业地址：<br><input type="text" name="company_address" value=<?php echo "$value[company_address]"; ?> ></td>
    <td>行业类别：<br>
<?php 
	$industry_class_list = array('工业','农业','社会发展等');
	draw_pd_set('industry_class',$industry_class_list,$value['industry_class']);	
?>
</td>
  </tr>
  <tr>
    <td>注册时间：<br><input type="text" name="register_time" value=<?php echo "$value[register_time]"; ?> ></td>
    <td>注册资本：<br><input type="text" name="registered_capital" value=<?php echo "$value[registered_capital]"; ?> ></td>
    <td>组织机构代码：<br><input type="text" name="company_code" value=<?php echo "$value[company_code]"; ?> ></td>
  </tr>
  <tr>
    <td>法人代表：<br><input type="text" name="corporate_rep" value=<?php echo "$value[corporate_rep]"; ?> ></td>
    <td>联系方式：<br><input type="text" name="rep_phone" value=<?php echo "$value[rep_phone]"; ?> ></td>
    <td>职务/职称：<br><input type="text" name="rep_title" value=<?php echo "$value[rep_title]"; ?> ></td>
  </tr>
  <tr>
    <td>联系人：<br><input type="text" name="contact" value=<?php echo "$value[contact]"; ?> ></td>
    <td>联系方式：<br><input type="text" name="contact_phone" value=<?php echo "$value[contact_phone]"; ?> ></td>
    <td>职务/职称：<br><input type="text" name="contact_title" value=<?php echo "$value[contact_title]"; ?> ></td>
  </tr>
   <tr>
    <td>传真：<br><input type="text" name="fax" value=<?php echo "$value[fax]"; ?> ></td>
    <td>网址：<br><input type="url" name="website" value=<?php echo "$value[website]"; ?> ></td>
    <td>电子邮件：<br><input type="email" name="email" value=<?php echo "$value[email]"; ?> ></td>
  </tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend>经营范围</legend>
<textarea name="business_scope"  cols="80px" rows="5px"><?php echo "$value[business_scope]"; ?></textarea>
</fieldset>

<fieldset style="font-size:12px">
<legend>企业规模和占地情况</legend>
<table>
  <tr>
    <td>占地面积：<br><input type="text" name="floor_space" value=<?php echo "$value[floor_space]"; ?> ></td>
    <td>营业收入：<br><input type="text" name="revenue" value=<?php echo "$value[revenue]"; ?> ></td>
    <td>职工总数：<br><input type="text" name="work_force" value=<?php echo "$value[work_force]"; ?> ></td>
    <td>大专以上职工数：<br><input type="text" name="high_work_force" value=<?php echo "$value[high_work_force]"; ?> ></td>
  </tr>
  <tr>
  	<td>利税收入：<br><input type="text" name="tax_revenue" value=<?php echo "$value[tax_revenue]"; ?> ></td>
    <td>上缴税额：<br><input type="text" name="pay_tax" value=<?php echo "$value[pay_tax]"; ?> ></td>
    <td>高新技术产品、新产品等销售收入：<br><input type="text" name="hightech_revenue" value=<?php echo "$value[hightech_revenue]"; ?> ></td>
  </tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend>企业类别</legend>
<table>
	<tr>
		<td>高新技术企业 <input type="checkbox" name="array_company_class[hightech_country]" value=<?php echo $hightech_country; ?> >国家  <input type="checkbox" name="array_company_class[hightech_city]" value=<?php echo $hightech_city; ?> >市</td>
        <td><input type="checkbox" name="array_company_class[shengchuangxinshifan]" value=<?php echo $shengchuangxinshifan; ?> >省级创新型示范企业</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_company_class[shengchuangxinxingshidian]" value=<?php echo $shengchuangxinxingshidian; ?> >省级创新型试点企业</td>
        <td><input type="checkbox" name="array_company_class[shengkejixingzhongxiao]" value=<?php echo $shengkejixingzhongxiao; ?> >省级科技型中小企业</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_company_class[shengzhuanlishifan]" value=<?php echo $shengzhuanlishifan; ?> >省级专利示范企业</td>
        <td><input type="checkbox" name="array_company_class[shengnongyekeji]" value=<?php echo $shengnongyekeji; ?> >省级农业科技型企业</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_company_class[shizhuanlishifan]" value=<?php echo $shizhuanlishifan; ?> >市级专利示范企业</td>
        <td><input type="checkbox" name="array_company_class[shichuangxinxing]" value=<?php echo $shichuangxinxing; ?> >市级创新型企业</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_company_class[xiankejixing]" value=<?php echo $xiankejixing; ?> >县级科技型企业</td>
        <td><input type="checkbox" name="array_company_class[class_other]" value=<?php echo $class_other; ?> >其他</td>
	</tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend>研发中心类别</legend>
<table>
	<tr>
		<td><input type="checkbox" name="array_reserch_class[shengjiqiyeyanjiuyuan]"  value=<?php echo $shengjiqiyeyanjiuyuan; ?> >省级企业研究院</td>
        <td><input type="checkbox" name="array_reserch_class[shengjigaoxinjishuqiyefazhanzhongxin]"  value=<?php echo $shengjigaoxinjishuqiyefazhanzhongxin; ?> >省级高新技术企业研发中心</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_reserch_class[shengjinongyeqiyekejiyanfazhongxin]"  value=<?php echo $shengjinongyeqiyekejiyanfazhongxin; ?> >省级农业企业科技研发中心</td>
        <td><input type="checkbox" name="array_reserch_class[shengjiquyukejichuangxinfuwuzhongxin]"  value=<?php echo $shengjiquyukejichuangxinfuwuzhongxin; ?> >省级区域科技创新服务中心</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_reserch_class[shengjidayuanmingxiaogongjianchuangxinzaiti]"  value=<?php echo $shengjidayuanmingxiaogongjianchuangxinzaiti; ?> >省级大院名校共建创新载体</td>
        <td><input type="checkbox" name="array_reserch_class[shijigongchengjishuyanjiukaifazhongxin]"  value=<?php echo $shijigongchengjishuyanjiukaifazhongxin; ?> >市级工程技术研究开发中心</td>
	</tr>
    <tr>
    	<td><input type="checkbox" name="array_reserch_class[qiyeshuyanfajigou]"  value=<?php echo $qiyeshuyanfajigou; ?> >企业所属研发机构</td>
        <td><input type="checkbox" name="array_reserch_class[rd_other]"  value=<?php echo $rd_other; ?> >其他</td>
	</tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend>科研活动情况</legend>
<table>
  <tr>
    <td>科技人员数：<br><input type="text" name="tech_member" value=<?php echo "$value[tech_member]"; ?> ></td>
    <td>技术依托单位：<br><input type="text" name="jishuyituodanwei" value=<?php echo "$value[jishuyituodanwei]"; ?> ></td>
    <td>研发费占销售收入比：<br><input type="text" name="rd_sale_ratio" value=<?php echo "$value[rd_sale_ratio]"; ?> ></td>
  </tr>
  <tr>
  	<td>发明专利数(当年)：<br><input type="text" name="invent_patent" value=<?php echo "$value[invent_patent]"; ?> ></td>
    <td>发明专利数(累计)：<br><input type="text" name="invent_patent_all" value=<?php echo "$value[invent_patent_all]"; ?> ></td>
    <td>实用新型专利(当年)：<br><input type="text" name="practical_patent" value=<?php echo "$value[practical_patent]"; ?> ></td>
    <td>实用新型专利(累计)：<br><input type="text" name="practical_patent_all" value=<?php echo "$value[practical_patent_all]"; ?> ></td>
  </tr><tr>
    <td>外观设计专利数(当年)：<br><input type="text" name="facade_patent" value=<?php echo "$value[facade_patent]"; ?> ></td>
    <td>外观设计专利数(累计)：<br><input type="text" name="facade_patent_all" value=<?php echo "$value[facade_patent_all]"; ?> ></td>
  </tr>
</table>
科技成果获奖<br>
<textarea name="tech_awards"  cols="80px" rows="5px"><?php echo "$value[tech_awards]"; ?></textarea><br>
高新技术及产品和新产品<br>
<textarea name="high_product" cols="80px" rows="5px"><?php echo "$value[high_product]"; ?></textarea>
</fieldset>
<fieldset style="font-size:12px">
<legend>科技创新项目意向</legend>
<fieldset style="font-size:12px">
<legend>科技创新项目意向</legend>
	<fieldset style="font-size:12px">
	<legend>专利申报</legend>
	<table>
	  <tr>
 	   <td>发明(项)：<br><input type="text" name="patent_invent" value="<?php echo "$value[patent_invent]"; ?>" ></td>
	    <td>实用型(项)：<br><input type="text" name="patent_practical" value="<?php echo "$value[patent_practical]"; ?>" ></td>
 	   <td>外观设计(项)：<br><input type="text" name="patent_appear" value="<?php echo "$value[patent_appear]"; ?>" ></td>
 	 </tr>
	</table>
	</fieldset>
	<fieldset style="font-size:12px">
	<legend>研发中心建设</legend>
	<table>
	  <tr>
	  <td colspan=4 >
<?php
$research_construction_arr = array('construction_country'=>'国家级','construction_province'=>'省级','construction_city'=>'市级','construction_county'=>'县级');
draw_checkbox_set('array_research_construction',$research_construction_arr,$array_research_construction);

?>
	   </td>
 	 </tr>
	</table>
	</fieldset>
	<fieldset style="font-size:12px">
	<legend>科技型企业升级</legend>
	<table>
	  <tr>
 	   <td colspan=4 >
<?php
$tech_upgrade_arr = array('upgrade_country'=>'国家级','upgrade_province'=>'省级','upgrade_city'=>'市级','upgrade_county'=>'县级');
draw_checkbox_set('array_tech_upgrade',$tech_upgrade_arr,$array_tech_upgrade);

?>	   
	   </td>
	 </tr>
	</table>
	</fieldset>
<table>
  <tr>
    <td>科企合作：<br><input type="text" name="tech_coorperation" value=<?php echo "$value[tech_coorperation]"; ?> ></td>
    <td>技术、产品开发引进：<br><input type="text" name="tech_introduce" value=<?php echo "$value[tech_introduce]"; ?> ></td>
    <td>其他：<br><input type="text" name="orientation_other" value=<?php echo "$value[orientation_other]"; ?> ></td>
  </tr>
</table>
</fieldset>
<h3 align="center"><input type="submit" name="submit" value="提交"></h3>
</form>

</html>
