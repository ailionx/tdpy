<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$id = $_SESSION[id];
	$user = $_SESSION[user];
	if ($_GET['project_id'] !== NULL){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from project_inprogress where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		//不同用户进入此页面的权限
		permissionBlocker('edit',$value[status]);
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>中期检查</title>
</head>
<h2><center>项目执行情况中期检查表</center></h2>

<form method="post" action="../proc/submit_form.php?project_id=<?php echo $project_id; ?>" name="inprogress_form" onSubmit="return formCheck()"> 
<input type="hidden" value="edit" name="apply_edit">
<input type="hidden" value="inprogress" name="submit_type">
<fieldset style="font-size:12px">
<legend>一、项目基本情况</legend>
<table border="0" style="font-size:12px">
  <tr>
    <td>项目名称：<input value="<?php echo $value[project_name]; ?>" type="text" name="project_name"></td>
  </tr>
  <tr>
    <td>计划编号：<br><input value="<?php echo $value[project_code]; ?>" type="text" name="project_code"></td>
    <td>开始日期：<br><input value="<?php echo $value[start_date]; ?>" type="text" name="start_date"></td>
  </tr>
  <tr>
    <td>承担单位：<br><input value="<?php echo $value[assume_unit]; ?>" type="text" name="assume_unit"/></td>
    <td>结束日期：<br><input value="<?php echo $value[finish_date]; ?>" type="text" name="finish_date"/></td>
  </tr>
  <tr>
    <td>项目负责人：<br><input value="<?php echo $value[project_manager]; ?>" type="text" name="project_manager"/></td>
    <td>联系电话：<br><input value="<?php echo $value[manager_phone]; ?>" type="text" name="manager_phone"/></td>
  </tr>
  <tr>
    <td>电子邮件：<br><input value="<?php echo $value[manager_email]; ?>" type="email" name="manager_email"/></td>
    <td>通讯地址：<br><input value="<?php echo $value[adress]; ?>" type="text" name="adress"/></td>
  </tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend>二、项目资金及其他配套条件落实情况</legend>
<table border="0" style="font-size:12px">
  <tr>
    <th>&nbsp;</th><th>项目总经费</th><th>其中：自筹</th><th>财政拨款</th><th>银行贷款</th>
  </tr>
  <tr>
    <th>预算总额（按合同）</th>
    <td><input value="<?php echo $value[total_budget]; ?>" type="text" name="total_budget"></td>
    <td><input value="<?php echo $value[self_budget]; ?>" type="text" name="self_budget"></td>
    <td><input value="<?php echo $value[fiscal_budget]; ?>" type="text" name="fiscal_budget"></td>
    <td><input value="<?php echo $value[loan_budget]; ?>" type="text" name="loan_budget"></td>
  </tr>
  <tr>
    <th>已落实经费</th>
    <td><input value="<?php echo $value[total_implemented]; ?>" type="text" name="total_implemented"/></td>
    <td><input value="<?php echo $value[self_implemented]; ?>" type="text" name="self_implemented"/></td>
    <td><input value="<?php echo $value[fiscal_implemented]; ?>" type="text" name="fiscal_implemented"/></td>
    <td><input value="<?php echo $value[loan_implemented]; ?>" type="text" name="loan_implemented"/></td>
  </tr>
</table>
</fieldset>
<fieldset style="font-size:12px">
<legend>项目执行情况</legend>
	<textarea name="progress_status" cols="100" rows="10"><?php echo $value[progress_status]; ?></textarea>
</fieldset>
<h3><input style="font-size:14px" type="submit" name="submit" value="提交"></h3>
</form>
<center><input type="button" onClick="history.go(-1)" value="返回"></center>
</html>
