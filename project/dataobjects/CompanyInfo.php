<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CompanyInfo
 *
 * @author slin
 */
class CompanyInfo extends DataObject{
    //put your code here
    
    public $className = 'CompanyInfo';

    public $dbName = 'company_info';
    
    public $fields = array(
        'id',
        'user',
        'company_name',
        'establish_time',
        'company_address',
        'industry_class',
        'register_time',
        'registered_capital',
        'company_code',
        'corporate_rep',
        'rep_phone',
        'rep_title',
        'contact',
        'contact_phone',
        'contact_title',
        'fax',
        'website',
        'email',
        'floor_space',
        'revenue',
        'work_force',
        'high_work_force',
        'tax_revenue',
        'pay_tax',
        'hightech_revenue',
        'array_company_class',
        'array_reserch_class',
        'tech_member',
        'jishuyituodanwei',
        'rd_sale_ratio',
        'invent_patent',
        'invent_patent_all',
        'practical_patent',
        'practical_patent_all',
        'facade_patent',
        'facade_patent_all',
        'business_scope',
        'tech_awards',
        'high_product',
        'patent_invent',
        'patent_practical',
        'patent_appear',
        'tech_coorperation',
        'tech_introduce',
        'orientation_other',
        'array_research_construction',
        'array_tech_upgrade'
    );
    

}
