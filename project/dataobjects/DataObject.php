<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class DataObject {
    
    public $className;
    
    public $dbName;

    public $fields = array(
        
    );
    
    /**
     * To initialize DataObject
     */
    public function __construct() {
        
        foreach ($this->fields as $k => $v) {
            $this->$v = '';
            
        }
    }

    /**
     * Function is to get all records from database
     * @return type
     */
    
    public function get(){
        $sql = 'SELECT * FROM ' . $this->dbName . " WHERE 1";
        $query = mysql_query($sql);
        
        $results = array();
        while ($row = mysql_fetch_assoc($query)){
            $dataObject = new self;
            $dataObject->update($row);
            
            $results[] = $dataObject;
        }
        
        return $results;
    }

    /**
     * Function is to get a record with spcific id
     * @param type $id
     * @return \self
     */
    public function get_by_id($id){
        $sql = 'SELECT * FROM ' . $this->dbName . " WHERE id='" . $id . "'";
        $query = mysql_query($sql);
        
        $result = mysql_fetch_assoc($query);
        
        $dataObject = new self;
        $dataObject->update($result);
        
        return $dataObject;
    }
    
    /**
     * Function is to update object from given data array
     * @param type $data
     * @return \DataObject
     */
    public function update($data){
        
        if (!is_array($data)) {
            die('update function in DataObject must have an array as input');
        }
        
        foreach ($data as $k => $v){
            if (in_array($k, $this->fields)){
                $this->$k = $v;
            }
        }
        
        return $this;
    }
    
    /**
     * Function is to write the object properties into database, if with id property and the record exist, it will update the record, else it will create a new record.
     * @return type
     */
    public function write(){
        $values = get_object_vars($this);
        
        $data = array();
        
        if($this->id && mysql_query('SELECT id FROM ' . $this->dbName . " WHERE id='" . $this->id . "'")){
            foreach ($values as $k => $v) {
                if(in_array($k, $this->fields)){
                    
                    $v = is_array($v) ? serialize($v) : $v;
                    
                    $str = $k."='".$v."'";
                    $data[$k] = $str;
                }
            }
            $sql = implode(',', $data);

            $sql = 'UPDATE ' . $this->dbName . " SET " . $sql . " WHERE id='" . $this->id . "'";
            $query = mysql_query($sql);
            
        } else {
            
            foreach ($values as $k => $v) {
                if(in_array($k, $this->fields)){
                    
                    $data[$k] = $v;
                }
            }
            
            $keys = array_keys($data);
            $sqlKeys = "(`" . implode('`,`', $keys) . "`)";
            $sqlValues = "('" . implode("','", $data) . "')";
            $sql = "INSERT INTO " . $this->dbName . " " . $sqlKeys . " VALUES " . $sqlValues;
            $query = mysql_query($sql);
            
            if($query){
                $this->id = mysql_insert_id();
            } else {
                die('Insert record error when doing object writing');
            }
            
        }
        
        return $this->id;
        
    }
    
}
