<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background-color:#CCC;
	background-image:url(../pic/tech_bg.jpg);
	background-attachment:fixed;
	background-position:0 0;
	background-repeat:no-repeat;
	background-size:100% auto;
	margin: 0;
	padding: 0;
	color: #000;
}

table {
	font-size:14px;
}

.container {
	height:300px;
	width: 500px;
/*	background:rgba(255, 255, 255, 0) none repeat scroll 0 0 !important;/*实现FF背景透明，文字不透明*/
/*	filter:Alpha(opacity=80); background:#fff;/*实现IE背景透明*/
	margin: 200px auto ; /* 侧边的自动值与宽度结合使用，可以将布局居中对齐 */
}


/* ~~ 这是布局信息。 ~~ 

1) 填充只会放置于 div 的顶部和/或底部。此 div 中的元素侧边会有填充。这样，您可以避免使用任何“方框模型数学”。请注意，如果向 div 自身添加任何侧边填充或边框，这些侧边填充或边框将与您定义的宽度相加，得出 *总计* 宽度。您也可以选择删除 div 中的元素的填充，并在该元素中另外放置一个没有任何宽度但具有设计所需填充的 div。

*/
.content {

	padding: 10px 0;
}

/* ~~ 其它浮动/清除类 ~~ */
.fltrt {  /* 此类可用于在页面中使元素向右浮动。浮动元素必须位于其在页面上的相邻元素之前。 */
	float: right;
	margin-left: 8px;
}
.fltlft { /* 此类可用于在页面中使元素向左浮动。浮动元素必须位于其在页面上的相邻元素之前。 */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* 如果从 .container 中删除了 overflow:hidden，则可以将此类放置在 <br /> 或空 div 中，作为 #container 内最后一个浮动 div 之后的最终元素 */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}
-->
</style>
<title>龙游县科技型企业梯度培育平台</title>
<script type="text/javascript" src="../../conf.js"></script>
<script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../js/my_javascript.js"></script>
<SCRIPT type=text/javascript>
function RegCheck() {
	if (regForm.user.value==="") {
	alert("请输入用户名");
	regForm.user.focus();
	return false;
	}
	if (regForm.password.value==="") {
	alert("请输入密码");
	regForm.password.focus();
	return false;
	}
	if (regForm.password.value.length<6) {
		alert("为了保证账户安全，请输入大于6位的密码");
		regForm.password.focus();
		return false;
	}
	if (regForm.password_again.value==="") {
		alert("请再次输入密码");
		regForm.password_again.focus();
		return false;
	}
	if (regForm.password.value!=regForm.password_again.value) {
	alert("两次输入密码不同");
	regForm.password.focus();
	return false;
	}

}
</SCRIPT>
</head>
<?php
	session_start();
	include "../../conf.php";
	$query = "select company_name from account where pems='2'";
	$result = mysql_query($query);
	while ($row=mysql_fetch_row($result)) {
		$company_list_all[] = $row[0];
	}
	$company_list = array_filter(array_unique($company_list_all)); //去除重复元素
	if($_GET['account_type']) {
		$account_type = $_GET['account_type'];
		switch ($account_type) {
			case ('company')://单位用户注册页面
				echo "
		<div class='container'>
		<form method='post' action='./register_submit.php' name='regForm' onsubmit='return RegCheck()'>
		<input type='hidden' value='company' name='account_type'>
		<table align='center' border='0' cellspacing='0' cellpadding='4'>
		<tr><th>
		用户名</th><td>
		<input type='text' name='user' id='check_exist' maxlength=10 onblur='check_name_exist(document.regForm.user.value)'>
		</td><td style='color:red'>*只能包含数字和字母</td></tr>
		<tr><th>
		密码</th><td>
		<input type='password' name='password' maxlength=10><br>
		</td><td style='color:red'>*只能包含数字和字母</td></tr>
		<tr><th>
		再次输入密码</th><td width='80px'>
		<input type='password' name='password_again'><br>
		</td></tr>
		<tr><th>
		单位名称</th><td>
		<input type='text' name='company_name'><br>
		</td></tr>
		<tr><th>
		联系电话</th><td>
		<input type='text' name='company_phone'><br>
		</td></tr>
		<tr><th>
		email</th><td>
		<input type='email' name='company_email'><br>
		</td></tr>
		<tr><th>
		验证码
		</th><td><input type='text' name='input_code' maxlength='4' style='width:60px;vertical-align:top'>
		<img id='veri_code_img' src='../proc/verification_code.php' style='cursor:hand' alt='点击更换图片'>
		</td></tr>
		<tr><td colspan=2 style='text-align:center'>
		<input type='submit' value='提交注册'>
		</td></tr>
		</table>
		<input type='hidden' name='veri_code' value=\"$_SESSION[veri_code]\" />
		</div>
		";
				break;
			case ('personal')://个人用户注册页面
				echo "
		<div class='container'>
		<form method='post' action='./register_submit.php' name='regForm' onsubmit='return RegCheck()'>
		<input type='hidden' value='personal' name='account_type' style='width:150px'>
		<table align='center' border='0' cellspacing='0' cellpadding='4'>
		<tr><th>
		用户名</th><td>
		<input type='text' name='user' id='check_exist' style='width:150px' onblur='check_name_exist(document.regForm.user.value)'>
		</td><td style='color:red'>*只能包含数字和字母</td></tr>
		<tr><th>
		密码</th><td>
		<input type='password' name='password' style='width:150px'><br>
		</td><td style='color:red'>*只能包含数字和字母</td></tr>
		<tr><th>
		再次输入密码</th><td>
		<input type='password' name='password_again' style='width:150px'><br>
		</td></tr>
		<tr><th valign=top>
		所属单位名称</th><td valign=top>
		<select name='company_name' style='width:150px'>
			<option value=''></option>";
		
		foreach ($company_list as $ent) {
			echo "<option value=$ent>$ent</option>";
		}
		echo "</select></td><td width=120px  style='color:red'>若在这里找不到所属的单位名称，说明该单位未在本系统注册成功，请您稍后再来注册，或与单位管理员联系</td>
		</tr>
		<tr><th>
		姓名</th><td>
		<input type='text' name='person_name' style='width:150px'><br>
		</td></tr>
		<tr><th>
		身份证号码</th><td>
		<input type='text' name='id_card' style='width:150px'><br>
		</td></tr>
		<tr><th>
		联系电话</th><td>
		<input type='text' name='person_phone' style='width:150px'><br>
		</td></tr>
		<tr><th>
		email</th><td>
		<input type='email' name='personal_email' style='width:150px'><br>
		</td></tr>
		<tr><th>
		验证码
		</th><td><input type='text' name='input_code' maxlength='4' style='width:60px;vertical-align:top'>
		<img id='veri_code_img' src='../proc/verification_code.php' style='cursor' alt='点击更换图片'>
		</td></tr>
		<tr><td colspan=2 style='text-align:center'>
		<input type='submit' value='提交注册'>
		</td></tr>
		</table>
		<input type='hidden' name='veri_code' value=\"$_SESSION[veri_code]\" />
		</div>
		";
				break;
			default:
				echo "<script>alert('错误的账户类型');history.go(-1)</script>";
				exit();
		}
		
	} else {
		echo "<script>alert('请选择账户类型');history.go(-1)</script>";
		exit();
	}
?>
<body>
</body>
</html>
