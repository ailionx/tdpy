<!DOCTYPE html>
<?php 
session_start();
include "../../conf.php";
if ($_POST) {
	header("Content-type: text/html; charset=utf-8"); 
	if($_POST[input_code] != $_SESSION[veri_code]) {
		 echo "<script>alert('你输入的验证码有误');history.go(-1)</script>";
		 echo "<script>history.go(-1)</script>";
		 exit(); 
	}
		// 验证 账号密码是不是由字母和数字组成
	if (ctype_alnum($_POST['user']) && ctype_alnum($_POST['password'])) {
		$user = $_POST['user'];
		$_POST['password']=md5($_POST['password'].POST_PRO);
	} else {
		echo "<script>alert('账号和密码含有非法字符，只能包含字母和数字')</script>";
		echo "<script>history.go(-1)</script>";
		exit();
	}
	$query="select id from `account` where user='$user'";
	/*查看是否用户名已经被占用*/
	$result=mysql_query($query);
	$numrows=mysql_num_rows($result);
	if($numrows!=0) {
		echo "<SCRIPT>alert('该用户名已被占用')</SCRIPT>";
		echo "<script>history.go(-1)</script>";
		exit();
	}
	$field_array = array('account_type','pems','user','password','company_name','company_phone','company_email','person_name','id_card','person_phone','personal_email','register_date'); 
	$person_array = array('person_name','id_card','person_phone','personal_email');
	$company_array = array('company_name','company_phone','company_email');
	$date = date('Y-m-d/H:i:s');
	$_POST['register_date'] = $date;
	switch($_POST['account_type']) {
		case ('company'): //企业账户的提交
			$_POST['pems'] = '20';
			$success_string = '注册成功，请等待管理员审核账号，审核通过之后可以正常登陆';
			foreach ($field_array as $field) {
				if ($_POST[$field]) {
					$value_array[$field] = $_POST[$field]; //提交上来有值的话，就放入数组
				} else if (in_array($field,$person_array)) {//如果没有值，先确定是不是个人账户的键名
					$value_array[$field] = 'N/A';
				} else {
//					echo $field;
//					echo $_POST[$field];
					echo "<script>alert('输入信息不完整')</script>";
					echo "<script>history.go(-1)</script>";
					exit();
				}
			}
			break;
		case ('personal')://个人账户的提交
			$_POST['pems'] = '30';
			$success_string = '注册成功，请等待您所属的企业审核账号，审核通过之后可以正常登陆';
			$query = "select company_name,company_phone,company_email from account where company_name='$_POST[company_name]'";
			$result = mysql_query($query);
			$row = mysql_fetch_array($result);
			foreach ($field_array as $field) {
				if ($_POST[$field]) {
					$value_array[$field] = $_POST[$field]; //提交上来有值的话，就放入数组
				} else if (in_array($field,$company_array)) {//如果没有值，先确定是不是企业账户的键名
					$value_array[$field] = $row[$field]; //如果是企业账户的键名，就从企业账户信息中复制到个人账户里
				} else {
//					echo $field;
//					echo $_POST[$field];
					echo "<script>alert('输入信息不完整')</script>";
					echo "<script>history.go(-1)</script>";
					exit();
				}
			}
			break;
		default:
			echo "<script>alert('未知的账户类型')</script>";
			echo "<script>history.go(-1)</script>";
			exit();
	}
	

	$field_list = implode(",",$field_array);
	$value_list = "'".implode("','",$value_array)."'";		
	$query = "insert into account ($field_list) values ($value_list)";
	if(!mysql_query($query)) {
		echo "<script>alert('写入数据库出错')</script>";
		echo "<script>history.go(-1)</script>";
		exit();
	} else {
		echo "<script>alert('".$success_string."')</script>";
		echo "<script>location.href='./login.php';</script>";
	}
} else {
	echo "<script>alert('请输入信息')</script>";
	echo "<script>history.go(-1)</script>";
	exit();
}

?>
