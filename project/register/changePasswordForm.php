<?php
    session_start();
    include ("../../conf.php");

    if($_POST['password']) {
        $change = new changeProfile();
        $change->changePassword($_POST['password']);
        echo "<script>alert('修改密码成功');</script>";
        echo "<script>top.location.href='../frame/mainPage.php'</script>";
    } else {

?>

<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        .container {
            margin-top: 100px;
        }
    </style>
</head>
<body>
    <div class='container'>
        <form method="post" name="changePasswordForm" action="./changePasswordForm.php" onsubmit='return PasswordCheck()'>
            <table align='center' border='0' cellspacing='0' cellpadding='4'>
            <tr><th>
            请输入新密码</th><td>
            <input type='password' name='password' maxlength=10><br>
            </td><td style='color:red'>*只能包含数字和字母</td></tr>
            <tr><th>
            再次输入密码</th><td width='80px'>
            <input type='password' name='password_again'><br>
            </td></tr>
            <tr><td colspan=2 style='text-align:center'>
            <input type='submit' value='提交更改'>
            </td></tr>
            </table>
        </form>
    </div>
    <script type="text/javascript" src="../../conf.js"></script>
    <script type="text/javascript" src="../js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="../js/my_javascript.js"></script>
    <script type=text/javascript>
        function PasswordCheck() {
          
                if (changePasswordForm.password.value==="") {
                alert("请输入密码");
                changePasswordForm.password.focus();
                return false;
                }
                if (changePasswordForm.password.value.length<6) {
                        alert("为了保证账户安全，请输入大于6位的密码");
                        changePasswordForm.password.focus();
                        return false;
                }
                if (changePasswordForm.password_again.value==="") {
                        alert("请再次输入密码");
                        changePasswordForm.password_again.focus();
                        return false;
                }
                if (changePasswordForm.password.value!=changePasswordForm.password_again.value) {
                alert("两次输入密码不同");
                changePasswordForm.password_again.focus();
                return false;
                }

        }
    </script>
</body>

<?php 
    }
