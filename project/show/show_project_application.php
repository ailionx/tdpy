<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$user = $_SESSION[user];
	if ($_GET['project_id']){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from project_application where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		// 如果值为空就替换为空格，让表格显示更为美观
		foreach ($value as $key=>&$eachValue) {
			if ($eachValue === NULL) {								
				$eachValue = '&nbsp;';
			}
		}
		
		//不同用户进入此页面的权限
		permissionBlocker('show',$value[status]);
		
		//反序列化数组存入mysql的数据
		$arrayLong_list = array_field_inDB('project_application');
		foreach ($arrayLong_list as $arrayLong) {
			${$arrayLong} = unserialize($value[$arrayLong]);
		}
		

	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
	
?>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<style type="text/css">
    
.init_passed {

    background-image: url(../pic/init_passed_30.png);
}

.check_passed{
    background-image: url(../pic/check_passed_30.jpg);
}
</style>
<title>科技项目申请</title>
</head>
<div class="container <?php 

    echo checkPassClass($value); 
?> ">
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
<table border="1" cellspacing=0 style="position:absolute;top:1cm;right:1cm;width:3.5cm;font-size:10.5pt;">
<tr>
<td>县科技局编号</td>
</tr><tr>
<td><?php
$id_length = strlen($value['project_id']);
$add_length = 3-$id_length;
$add_string = "";
for ($i=0;$i<$add_length;$i++) {
	$add_string .= '0';
}
$plan_code = date('Y').$value['industry_type'].$add_string.$value['project_id'];
echo $plan_code;
?>
</td>
</tr>
</table>
<h1 style="font-family:黑体;font-weight:bold;font-size:24pt;line-height:40pt;text-align:center;margin:3cm 0 0 0;">龙游县科技计划项目申请书</h1>
<br><br><br><br><br><br><br><br>
<div style="font-family:黑体;font-size:15pt;text-align:left;margin: 0 3cm">
<h3>项 目 名 称：<?php echo replaceText($value[project_name]); ?></h3>
<h3>第一申请单位（盖章）：<?php echo replaceText($value[danweimingcheng]); ?></h3>
<h3>项目负责人：<?php echo replaceText($value[fuzeren_name]); ?></h3>
<h3>通 讯 地 址：<?php echo replaceText($value[xiangxidizhi]); ?></h3>
<h3>邮 政 编 码：<?php echo replaceText($value[youzhengbianma]); ?></h3>
<h3>电       话：<?php echo replaceText($value[lianxidianhua]); ?></h3>
<h3>申 请 日 期：<?php $dataArr = explode('/',$value[apply_date]); echo $dataArr[0]; ?></h3>
</div>
<br><br><br><br><br>
<h3 class="paging" style="font:仿宋;font-size:18pt;line-height:22pt;">龙游县科学技术局</h3>
<center>
<h2>一、项目情况</h2>
<table class='showTable paging' border=1 cellspacing=0 height=80%>
 <tr>
  <td >
  项目名称
  </td>
  <td colspan=8>
  <?php echo replaceText($value[project_name]);?>
  </td>
 </tr>
 <tr>
  <td >
  项目计划类别
  </td>
  <td colspan=8>
  <?php 
  switch ($value[project_class]) {
	  case (key_project):
	  	echo "重点科技计划项目";
	  	break;
	  case (general_project):
	  	echo "一般科技计划项目";
		break;
	  default:
	  	echo "计划类别未知";
  }
  ?>
  </td>
 </tr>
 <tr>
  <td >
  项目行业分类（显示为分类代码）
  </td>
  <td colspan=8>
  <?php echo replaceText($value[industry_type]);?>
  </td>
 </tr>
 <tr>
  <td >
  项目技术来源
  </td>
  <td colspan=8>
  <?php echo replaceText($value[tech_resource]);?>
  </td>
 </tr>
 <tr>
  <td >
  开始日期
  </td>
  <td colspan=3>
  <?php echo replaceText($value[start_date]);?>
  </td>
  <td colspan=2>
  完成日期
  </td>
  <td colspan=3>
  <?php echo replaceText($value[finish_date]);?>
  </td>
 </tr>
 <tr>
  <td rowspan=3>
  项目经费预算（万元）
  </td>
  <td >
  总计
  </td>
  <td colspan=7>
  其中
  </td>
 </tr>
 <tr>
  <td rowspan=2>
  <?php echo replaceText($value[total_expenditure]);?>
  </td>
  <td >
  自筹
  </td>
  <td colspan=2>
  银行贷款
  </td>
  <td colspan=3>
  向县财政申请
  </td>
  <td >
  其他
  </td>
 </tr>
 <tr>
  <td >
  <?php echo replaceText($value[zichou]);?>
  </td>
  <td colspan=2>
  <?php echo replaceText($value[yinhangdaikuan]);?>
  </td>
  <td colspan=3>
  <?php echo replaceText($value[caizhengshenqing]);?>
  </td>
  <td >
  <?php echo replaceText($value[other_expenditure]);?>
  </td>
 </tr>
 <tr>
  <td rowspan=6>
  项目经费开支预算（万元）
  </td>
  <td >
  设备费
  </td>
  <td >
  材料费
  </td>
  <td colspan=4>
  试验化验加工费
  </td>
  <td colspan=2>
  燃料动力费
  </td>
 </tr>
 <tr>
  <td >
  <?php echo replaceText($value[shebeifei]);?>
  </td>
  <td >
  <?php echo replaceText($value[cailiaofei]);?>
  </td>
  <td colspan=4>
  <?php echo replaceText($value[shiyanhuayanjiagongfei]);?>
  </td>
  <td colspan=2>
  <?php echo replaceText($value[ranliaodonglifei]);?>
  </td>
 </tr>
 <tr>
  <td >
  差旅费
  </td>
  <td >
  人员劳务费
  </td>
  <td colspan=4>
  合作、协作研究与交流费
  </td>
  <td colspan=2>
  出版/文献/信息传播知识产权事务费
  </td>
 </tr>
 <tr>
  <td >
  <?php echo replaceText($value[chailvfei]);?>
  </td>
  <td >
  <?php echo replaceText($value[renyuanlaowufei]);?>
  </td>
  <td colspan=4>
  <?php echo replaceText($value[hezuojiaoliufei]);?>
  </td>
  <td colspan=2>
  <?php echo replaceText($value[zhishichanquanshiwufei]);?>
  </td>
 </tr>
 <tr>
  <td >
  会议费
  </td>
  <td >
  管理费
  </td>
  <td colspan=4>
  专家咨询费
  </td>
  <td colspan=2>
  其他开支
  </td>
 </tr>
 <tr>
  <td >
  <?php echo replaceText($value[huiyifei]);?>
  </td>
  <td >
  <?php echo replaceText($value[guanlifei]);?>
  </td>
  <td colspan=4>
  <?php echo replaceText($value[zhuanjiazixunfei]);?>
  </td>
  <td colspan=2>
  <?php echo replaceText($value[other_spending]);?>
  </td>
 </tr>
 <tr>
  <td rowspan=2>
  预计经济效益
  </td>
  <td >
  年增产值<br>
  （万元）
  </td>
  <td >
  年增利润<br>
  （万元）
  </td>
  <td colspan=2>
  年增税金<br>
  （万元）
  </td>
  <td colspan=2>
  年创汇<br>
  （万美元）
  </td>
  <td colspan=2>
  年节汇<br>
  （万美元）
  </td>
 </tr>
 <tr>
  <td >
  <?php echo replaceText($value[nianzengchanzhi]);?>
  </td>
  <td >
  <?php echo replaceText($value[nianzenglirun]);?>
  </td>
  <td colspan=2>
  <?php echo replaceText($value[nianzengshuijin]);?>
  </td>
  <td colspan=2>
  <?php echo replaceText($value[nianchuanghui]);?>
  </td>
  <td colspan=2>
  <?php echo replaceText($value[nianjiehui]);?>
  </td>
 </tr>
 <tr>
  <td rowspan=2>
  预计其他成果
  </td>
  <td >
  论文数
  </td>
  <td >
  专利
  </td>
  <td colspan=2>
  其中发明专利
  </td>
  <td colspan=2>
  </td>
  <td colspan=2>
  </td>
 </tr>
 <tr>
  <td >
  <?php echo replaceText($value[lunwenshu]);?>
  </td>
  <td >
  <?php echo replaceText($value[zhuanli]);?>
  </td>
  <td colspan=2>
  <?php echo replaceText($value[famingzhuanli]);?>
  </td>
  <td colspan=2>
  </td>
  <td colspan=2>
  </td>
 </tr>
 <tr>
  <td >
  备注
  </td>
  <td colspan=8>
  <?php echo replaceText($value[beizhu]);?>
  </td>
 </tr>
</table>
<h2>二、承担单位</h2>
<table class='showTable paging' border=1 cellspacing=0  height=75%>
 <tr>
  <td rowspan=8>
  第一申请单位
  </td>
  <td >
  单位名称
  </td>
  <td colspan=3>
  <?php echo replaceText($value[danweimingcheng]);?>
  </td>
 </tr>
 <tr>
  <td >
  单位简称
  </td>
  <td >
  <?php echo replaceText($value[danweijiancheng]);?>
  </td>
  <td >
  法人代码
  </td>
  <td >
  <?php echo replaceText($value[farendaibiao]);?>
  </td>
 </tr>
 <tr>
  <td >
  所在地代码
  </td>
  <td >
  <?php echo replaceText($value[suozaididaima]);?>
  </td>
  <td >
  单位类型
  </td>
  <td >
  <?php echo replaceText($value[danweileixing]);?>
  </td>
 </tr>
 <tr>
  <td >
  详细地址
  </td>
  <td >
  <?php echo replaceText($value[xiangxidizhi]);?>
  </td>
  <td >
  邮政编码
  </td>
  <td >
  <?php echo replaceText($value[youzhengbianma]);?>
  </td>
 </tr>
 <tr>
  <td >
  单位EMAIL
  </td>
  <td >
  <?php echo replaceText($value[danweiemail]);?>
  </td>
  <td >
  联系人
  </td>
  <td >
  <?php echo replaceText($value[lianxiren]);?>
  </td>
 </tr>
 <tr>
  <td >
  联系电话
  </td>
  <td >
  <?php echo replaceText($value[lianxidianhua]);?>
  </td>
  <td >
  传真
  </td>
  <td >
  <?php echo replaceText($value[chuanzhen]);?>
  </td>
 </tr>
 <tr>
  <td >
  开户银行
  </td>
  <td >
  <?php echo replaceText($value[kaihuyinhang]);?>
  </td>
  <td >
  银行帐号
  </td>
  <td >
  <?php echo replaceText($value[yinhangzhanghao]);?>
  </td>
 </tr>
 <tr>
  <td >
  主管部门
  </td>
  <td colspan=3>
  <?php echo replaceText($value[zhuguanbumen]);?>
  </td>
 </tr>
 <tr>
  <td rowspan=<?php echo replaceText($value[hezuodanweizongshu]); ?> >
  其他合作单位
  </td>
  <td colspan=2>
  单位名称
  </td>
  <td >
  法人代码
  </td>
  <td >
  职责*
  </td>
 </tr>
    <?php 
	$hezuo_col = $value[hezuodanweizongshu]-1;
	for ($i=0;$i<$hezuo_col;$i++){
		$ii=$i+1;
		echo "<tr>
		<td>$ii.</td>
		<td>$array_unit_danweimingcheng[$i]</td>
		<td>$array_unit_farendaibiao[$i]</td>
		<td>$array_unit_zhize[$i]</td>
		</tr>";
	}
	?>
  <td rowspan=2>
  合作单位总数 △
  </td>
  <td colspan=2 rowspan=2>
  <?php echo replaceText($value[hezuodanweizongshu]); ?>
  </td>
  <td >
  承担单位数
  </td>
  <td >
  参加单位数
  </td>
 </tr>
 <tr>
  <td >
  <?php echo replaceText($value[chengdandanweishu]); ?>
  </td>
  <td >
  <?php echo replaceText($value[canjiadanweishu]); ?>
  </td>
 </tr>
 <tr>
  <td colspan=5>
  说明： * : 0-承担，1－参加； △ ：包含第一申请单位
  </td>
 </tr>
</table>
<h2>三、项目负责人及项目组成员</h2>
<table class='showTable' border=1 cellspacing=0  height=40%>
 <tr>
  <td rowspan=8>
  项目负责人
  </td>
  <td >
  姓名
  </td>
  <td colspan=3>
  <?php echo replaceText($value[fuzeren_name]); ?>
  </td>
 </tr>
 <tr>
  <td>
  身份证号码
  </td>
  <td colspan=3>
  <?php echo replaceText($value[fuzeren_shenfenzheng]); ?>
  </td>
 </tr>
 <tr>
  <td>
  联系电话
  </td>
  <td colspan=3>
  <?php echo replaceText($value[fuzeren_phone]); ?>
  </td>
 </tr>
 <tr>
  <td>
  EMAIL
  </td>
  <td colspan=3>
  <?php echo replaceText($value[fuzeren_email]); ?>
  </td>
 </tr>
 <tr>
  <td>
  学历
  </td>
  <td >
  <?php echo replaceText($value[fuzeren_xueli]); ?>
  </td>
  <td >
  学位
  </td>
  <td >
  <?php echo replaceText($value[fuzeren_xuewei]); ?>
  </td>
 </tr>
 <tr>
  <td>
  专业技术职务
  </td>
  <td>
  <?php echo replaceText($value[fuzeren_zhiwu]); ?>
  </td>
  <td>
  专业
  </td>
  <td>
  <?php echo replaceText($value[fuzeren_zhuanye]); ?>
  </td>
 </tr>
 <tr>
  <td>
  在本项目中的分工
  </td>
  <td colspan=3>
  <?php echo replaceText($value[fuzeren_fengong]); ?>
  </td>
 </tr>
 <tr>
  <td>
  工作单位
  </td>
  <td colspan=3>
  <?php echo replaceText($value[fuzeren_danwei]); ?>
  </td>
 </tr>
</table>
<br>
<table class='showTable paging members' border=1 cellspacing=0  height=40%>
 <tr>
  <td rowspan=<?php echo replaceText($value[member_number])+1; ?> class='header' >
  项目组成员
  </td>
  <td class='name'>
  姓名
  </td>
  <td class='birth'>
  出生年月
  </td>
  <td class='title'>
  专业技术职务
  </td>
  <td class='major'>
  专业
  </td>
  <td class='company'>
  工作单位
  </td>
  <td class='duty'>
  在本项目中分工
  </td>
 </tr>
 <?php 
	for ($i=0;$i<$value[member_number];$i++){
		$ii=$i+1;
		echo "<tr>
		<td>$array_member_name[$i]</td>
		<td>$array_member_chushengnianyue[$i]</td>
		<td>$array_member_zhiwu[$i]</td>
		<td>$array_member_zhuanye[$i]</td>
		<td>$array_member_gongzuodanwei[$i]</td>
		<td>$array_member_fengong[$i]</td>
		</tr>";
	}
	?>
</table>
<h2>四、本项目的立题依据：包括目的、意义、国内外概况和发展趋势，现有工作基础和条件（包括研究工作基础、装备条件和技术力量及项目负责人技术工作简历）</h2>
<table class='showTable paging' border=1 cellspacing=0  height=75%>
 <tr>
  <td valign=top height=100 style='text-align:left'>
  <?php echo replaceText($value[litiyiju]); ?>
  </td>
 </tr>
</table>
<h2>五、研究、开发内容和预期成果：具体研究、开发内容和重点解决的技术关键问题，要达到的主要技术、经济指标及经济社会环境效益，拟采取的研究方法和技术路线或工艺流程（可用框图表示）</h2>
<table class='showTable paging' border=1 cellspacing=0  height=75%>
 <tr>
  <td valign=top height=100 style='text-align:left'>
  <?php echo replaceText($value[neirongheyuqichengguo]); ?>
  </td>
 </tr>
</table>
<h2>六、计划进度目标</h2>
<table class='showTable paging' border=1 cellspacing=0  height=75%>
 <tr>
  <td >
  起始年月
  </td>
  <td>
  进度目标要求（每栏限80字）
  </td>
 </tr>
 <?php
	$n=count($array_progress_start);
	for($i=0;$i<$n;$i++){
		echo "<tr id='tr_$i'><td >
			$array_progress_start[$i] 至 $array_progress_end[$i]</td>
			<td>$array_progress_content[$i]</td>
			</tr>";
	}
 ?>
</table>
<h2>七、市场资源：</h2>
<table class='showTable' border=1 cellspacing=0  height=45%>
 <tr>
  <td valign=top style='text-align:left'>
  <?php echo replaceText($value[shichangziyuan]);?>
  </td>
 </tr>
</table>
<h2>八、项目申请经费：（计算根据及理由）</h2>
<table class='showTable paging' border=1 cellspacing=0  height=30%>
 <tr>
  <td valign=top style='text-align:left'>
  <?php echo replaceText($value[shenqingjingfei]);?>
  </td>
 </tr>
</table>
<h2>九、承诺书</h2>
<table class='showTable' border=1 cellspacing=0  height=35% style="font-size:14px;">
 <tr>
  <td valign=top style='text-align:left'>
本单位（或个人）承诺：<br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp;本申请书中所填写的内容和资料真实、有效，如存在弄虚作假和与事实相违背的内容，由本单位（个人）承担全部责任。
<br>
<br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp;申报单位（盖章）
<br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp;项目负责人签字：
<br>　　　　
<p align="right">年　　　　月　　　　日</p>
  </td>
 </tr>
</table>

<h2>十、县科技主管部门审查意见：</h2>
<table class='showTable' border=1 cellspacing=0  height=35% style="font-size:14px;">
 <tr>
  <td valign=top style='text-align:left'>
<br>
<br>
<br>  
<br> 
<br>
&nbsp;&nbsp;&nbsp;&nbsp;单位盖章<br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp;负责人签字：<br>
<br>
<p align="right">年　　　　月　　　　日</p>
  </td>
 </tr>
</table>
</center>
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
</div>
</html>
