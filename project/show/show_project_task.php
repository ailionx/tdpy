<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$user = $_SESSION[user];
	if ($_GET['project_id']){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from project_task where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		// 如果值为空就替换为空格，让表格显示更为美观
		foreach ($value as $key=>&$eachValue) {
			if ($eachValue === NULL) {								
				$eachValue = '&nbsp;';
			}
		}
		//不同用户进入此页面的权限
		permissionBlocker('show',$value[status]);
		
		//反序列化数组存入mysql的数据
		$arrayLong_list = array_field_inDB('project_task');
		foreach ($arrayLong_list as $arrayLong) {
			${$arrayLong} = unserialize($value[$arrayLong]);
		}
		if ($value['project_status']=='6') {
			$check_bg = 'url(../pic/check_passed_05.jpg)'; 
		} else {
			$check_bg = 'none'; 
		}
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
	
?>
<!doctype html5>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<title> </title>
<style type="text/css">
.check_passed {
	background-image:<?php echo $check_bg; ?>;
}
</style>
</head>
<div class="container check_passed">
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
<h1 style="font-family:黑体;font-weight:bold;font-size:26pt;line-height:40pt;text-align:center;margin:3cm 0 0 0;">龙 游 县 科 技 计 划 项 目<br>任 务 书</h1>
<br><br><br><br><br><br><br><br>
<div style="font-family:仿宋;font-size:16pt;text-align:left;margin: 0 3cm">
<h3>计 划 编 号：
<?php
$query_tmp = "select industry_type from project_application where project_id='$project_id'";
$result_tmp = mysql_query($query_tmp);
$industry_type = mysql_fetch_row($result_tmp);
$id_length = strlen($value['project_id']);
$add_length = 3-$id_length;
$add_string = "";
for ($i=0;$i<$add_length;$i++) {
	$add_string .= '0';
}
echo date('Y').$industry_type[0].$add_string.$value['project_id'];
?>
</h3>
<h3>项 目 名 称：&nbsp;<?php echo replaceText($value[project_name]); ?></h3>
<h3>计 划 类 别：
<?php
	$queryTemp = "select project_class from project_application where project_id='$project_id'";
	$rowTemp = mysql_fetch_array(mysql_query($queryTemp));
	switch ($rowTemp[project_class]) {
	  case (key_project):
	  	echo "重点科技计划项目";
	  	break;
	  case (general_project):
	  	echo "一般科技计划项目";
		break;
	  default:
	  	echo "计划类别未知";
  }
 ?></h3>
<h3>起 止 年 月：&nbsp;<?php echo replaceText($value[start_date]); ?>&nbsp;至&nbsp;<?php echo replaceText($value[finish_date]); ?></h3>
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<h3 class="paging" style="font:宋体;font-size:18pt;line-height:22pt;">龙 游 县 科 学 技 术 局<br>
2011年制</h3>

<h2>填 写 说 明</h2>
<ol class="paging" style="text-align:left;font-family:仿宋;font-size:15pt;">
<li>本任务书文本适用于县财政科技经费以“一般项目”立项资助的县级科技计划项目等。</li>
<li>“经济效益目标”、“项目成果”和人才引进培养情况应与“五、主要技术、经济指标”一致，是项目验收时考核的指标。</li>
<li>任务书中的“项目主要内容”，不同计划类别的项目要求不同。侧重于攻关的科技项目，“项目主要内容”应当重点突出项目研究开发内容和关键技术的研发；侧重于转化和产业化的科技项目，“项目主要内容”还应当包括转化和产业化过程中需要解决的技术问题。</li>
<li>任务书中的“技术、经济指标”，侧重于技术攻关的计划项目，主要应当反映技术指标、获得专利和人才培养等情况；侧重于科技成果转化和产业化项目，除技术指标外，还应反映经济指标，包括技术成果应用所形成的生产规模、经济效益、形成示范基地等指标。</li>
<li>项目经费中的“自筹”指乙方自行筹措、落实，与本项目相关的研究与开发投入的资金。</li>
<li>“项目经费预算”应按〈财政部 科技部关于印发《国家科技支撑计划专项经费管理》的通知〉（财教〔2006〕160号）规定的无偿资助性项目的经费开支范围填写。</li>
<li>根据《龙游县科技计划项目申报管理办法（试行）》（龙政办发〔2010〕62号）的规定，乙方完成本项目任务后，应及时申请验收或结题。</li>
<li>本任务书一式六份，分存甲方、乙方及有关单位。</li>
</ol>
<br>
<br>
<center>
<h2>一、项目基本情况</h2>
<table class='showTable paging' border=1 cellspacing=0 cellpadding=0 height="80%">
<tr>
<td colspan=2> 项 目 名 称</td>
<td colspan=12><?php echo replaceText($value[project_name]);?></td>
</tr>
<tr>
<td colspan=2> 项目管理领域</td>
<td colspan=12><?php echo replaceText($value[manager_domain]);?></td>
</tr>
<tr>
<td colspan=2> 项目技术来源</td>
<td colspan=12>
  <?php
    switch ($value[tech_resource]) {
	  case (independent_develop):
	  	echo "自主开发";
	  	break;
	  case (cooperation_develop):
	  	echo "合作开发";
		break;
	  case (chanxueyan_cooperation):
	  	echo "产学研联合开发";
		break;
	  case (internal_cooperation):
	  	echo "引进国内技术";
		break;
	  case (foreign_technology):
	  	echo "引进国外技术";
		break;
	  default:
	  	echo "未知项目技术来源";
  }
  ?>
</td>
</tr>
<tr>
<td colspan=2> 技术创新方式</td>
<td colspan=12>
  <?php
    switch ($value[innovation_format]) {
	  case (self_innovation):
	  	echo "A、自主创新";
	  	break;
	  case (integrate_innovation):
	  	echo "B、集成创新";
		break;
	  case (re_innovation):
	  	echo "C、在引进、消化吸收基础上的再创新";
		break;
	  case (tech_transform):
	  	echo "D、科技成果转化和产业化";
		break;
	  default:
	  	echo "未知项目技术来源";
  }
  ?>
</td>
</tr>
<tr>
<td colspan=2> 项目开始时间</td>
<td colspan=4> <?php echo replaceText($value[start_date]);?></td>
<td colspan=3> 项目完成时间</td>
<td colspan=5> <?php echo replaceText($value[finish_date]);?></td>
</tr>
<tr>
<td colspan=2 rowspan=3> 项目成果</td>
<td colspan=3 rowspan=2>新增产值（万元）</td>
<td rowspan=2>新增利税（万元）</td>
<td colspan=5> 专利</td>
<td colspan=3> 其中发明专利</td>
</tr>
<tr>
<td colspan=2> 申请数</td>
<td colspan=3> 授权数</td>
<td colspan=2> 申请数</td>
<td> 授权数</td>
</tr>
<tr>
<td colspan=3><?php echo replaceText($value[xinzengchanzhi]);?></td>
<td><?php echo replaceText($value[xinzenglishui]);?></td>
<td colspan=2><?php echo replaceText($value[zhuanlishenqing]);?></td>
<td colspan=3><?php echo replaceText($value[zhuanlishouquan]);?></td>
<td colspan=2><?php echo replaceText($value[famingzhuanlishenqing]);?></td>
<td><?php echo replaceText($value[famingzhuanlishouquan]);?></td>
</tr>
<tr>
<td colspan=2> 发表论文</td>
<td colspan=4><?php echo replaceText($value[fabiaolunwen]);?></td>
<td colspan=3> 人才引进和培养</td>
<td colspan=5><?php echo replaceText($value[rencaiyinjin]);?></td>
</tr>
<tr>
<td rowspan=5>承担单位</td>
<td colspan=3> 单位名称</td>
<td colspan=10><?php echo replaceText($value[danweimingcheng]);?></td>
</tr>
<tr>
<td colspan=3> 法人代码</td>
<td colspan=10><?php echo replaceText($value[farendaima]);?></td>
</tr>
<tr>
<td colspan=3> 详细地址</td>
<td colspan=3><?php echo replaceText($value[xiangxidizhi]);?></td>
<td colspan=3> 邮政编码</td>
<td colspan=4><?php echo replaceText($value[youzhengbianma]);?></td>
</tr>
<tr>
<td colspan=3> 联 系 人</td>
<td colspan=3><?php echo replaceText($value[diyidanweilianxiren]);?></td>
<td colspan=3> E-mail</td>
<td colspan=4><?php echo replaceText($value[diyidanweiemail]);?></td>
</tr>
<tr>
<td colspan=3> 电话/传真</td>
<td colspan=3><?php echo replaceText($value[dianhuachuanzhen]);?></td>
<td colspan=3> 手 机</td>
<td colspan=4><?php echo replaceText($value[shouji]);?></td>
</tr>
<tr>
<td rowspan=<?php $hezuo_tcol = $value[hezuodanweizongshu]+1; echo $hezuo_tcol; ?> >合作单位</td>
<td colspan=6> 单位名称</td>
<td colspan=5> 法人代码</td>
<td colspan=2> 职责</td>
</tr>
    <?php 
	$hezuo_col = $value[hezuodanweizongshu];
	for ($i=0;$i<$hezuo_col;$i++){
		$ii=$i+1;
		echo "<tr>
		<td>$ii.</td>
		<td colspan=5>$array_unit_danweimingcheng[$i]</td>
		<td colspan=5>$array_unit_farendaima[$i]</td>
		<td colspan=2>$array_unit_zhize[$i]</td>
		</tr>";
	}
	?>
</table>
<br>


<h2>二、项目负责人及项目组成员</h2>
<table  class='showTable paging' border=1 cellspacing=0 cellpadding=0 height="80%">
<tr>
<td rowspan=6>项目负责人</td>
<td colspan=2 valign=top> 姓 名</td>
<td colspan=3><?php echo replaceText($value[fuzeren_name]);?></td>
<td colspan=2 valign=top> 身份证号码</td>
<td colspan=2><?php echo replaceText($value[fuzeren_shenfenzheng]);?></td>
</tr>
<tr>
<td colspan=2 valign=top> 工作单位</td>
<td colspan=3><?php echo replaceText($value[fuzeren_danwei]);?></td>
<td colspan=2 valign=top> 法人代码</td>
<td colspan=2><?php echo replaceText($value[fuzeren_farendaima]);?></td>
</tr>
<tr>
<td colspan=2 valign=top> 详细地址</td>
<td colspan=3><?php echo replaceText($value[fuzeren_adress]);?></td>
<td colspan=2 valign=top> 邮政编码</td>
<td colspan=2><?php echo replaceText($value[fuzeren_zcode]);?></td>
</tr>
<tr>
<td colspan=2 valign=top> 移动电话</td>
<td colspan=3><?php echo replaceText($value[fuzeren_mobile]);?></td>
<td colspan=2 valign=top> E-mail</td>
<td colspan=2><?php echo replaceText($value[fuzeren_email]);?></td>
</tr>
<tr>
<td colspan=2 valign=top> 学 历</td>
<td colspan=2><?php echo replaceText($value[fuzeren_xueli]);?></td>
<td colspan=2> 学位</td>
<td colspan=3><?php echo replaceText($value[fuzeren_xuewei]);?></td>
</tr>
<tr>
<td colspan=2 valign=top> 职 称</td>
<td colspan=2><?php echo replaceText($value[fuzeren_zhicheng]);?></td>
<td colspan=2> 现从事专业</td>
<td colspan=3><?php echo replaceText($value[fuzeren_zhuanye]);?></td>
</tr>
<tr>
<td rowspan=<?php echo replaceText($value[member_number])+1; ?> >项目组成员</td>
<td> 姓 名</td>
<td colspan=2> 身份证号码</td>
<td> 所在单位</td>
<td> 职 称</td>
<td colspan=2> 从事专业</td>
<td> 在本项目中的分工</td>
<td> 年参加项目工作时间(月)</td>
</tr>
    <?php 
	for ($i=0;$i<$value[member_number];$i++){
		echo "<tr>
		<td>$array_member_name[$i]</td>
		<td colspan=2>$array_member_shenfenzheng[$i]</td>
		<td>$array_member_gongzuodanwei[$i]</td>
		<td>$array_member_zhicheng[$i]</td>
		<td colspan=2>$array_member_zhuanye[$i]</td>
		<td>$array_member_fengong[$i]</td>
		<td>$array_member_canjiashijian[$i]</td>
		</tr>";
	}
	?>
</table>

<h2>三、项目研发主要任务和关键技术经济指标</h2>

<table class="showTable" border=1 cellspacing=0 height=55%>
 <tr>
  <td valign=top height=100 style='text-align:left'>
  <?php echo replaceText($value[zhibiao]); ?>
  </td>
 </tr>
</table>

<h2>四、项目成果提供形式</h2>

<table class='showTable paging' border=1 cellspacing=0 height=30%>
 <tr>
  <td valign=top height=100 style='text-align:left'>
  <?php echo replaceText($value[chengguoxingshi]); ?>
  </td>
 </tr>
</table>


<h2>五、项目经费来源</h2>
本项目研发总经费<u><?php echo replaceText($value[zongjingfei]);?></u>万元，其中:甲方补助<u><?php echo replaceText($value[jiafangbuzhu]);?></u>万元，乙方自筹<u><?php echo replaceText($value[yifangzichou]);?></u>万元。
<br>
<h2>六、项目经费支出预算</h2>

<table class="showTable" border=1 cellspacing=0 cellpadding=0 height="45%">
<tr>
<td> 经费开支科目</td>
<td> 预算经费总额(万元)</td>
<td>
其中县财政经费
（万元）
</td>
</tr>
<tr>
<td> 1.设备费</td>
<td><?php echo $array_jingfei_yusuan[0];?></td>
<td><?php echo $array_jingfei_caizheng[0];?></td>
</tr>
<tr>
<td> 2.材料费</td>
<td><?php echo $array_jingfei_yusuan[1];?></td>
<td><?php echo $array_jingfei_caizheng[1];?></td>
</tr>
<tr>
<td> 3.测试化验加工费</td>
<td><?php echo $array_jingfei_yusuan[2];?></td>
<td><?php echo $array_jingfei_caizheng[2];?></td>
</tr>
<tr>
<td> 4.燃料动力费</td>
<td><?php echo $array_jingfei_yusuan[3];?></td>
<td><?php echo $array_jingfei_caizheng[3];?></td>
</tr>
<tr>
<td> 5.差旅费</td>
<td><?php echo $array_jingfei_yusuan[4];?></td>
<td><?php echo $array_jingfei_caizheng[4];?></td>
</tr>
<tr>
<td> 6.会议费</td>
<td><?php echo $array_jingfei_yusuan[5];?></td>
<td><?php echo $array_jingfei_caizheng[5];?></td>
</tr>
<tr>
<td> 7.合作、协作研究与交流费</td>
<td><?php echo $array_jingfei_yusuan[6];?></td>
<td><?php echo $array_jingfei_caizheng[6];?></td>
</tr>
<tr>
<td> 8.出版/文献/信息传播/知识产权事务费</td>
<td><?php echo $array_jingfei_yusuan[7];?></td>
<td><?php echo $array_jingfei_caizheng[7];?></td>
</tr>
<tr>
<td> 9.人员劳务费</td>
<td><?php echo $array_jingfei_yusuan[8];?></td>
<td><?php echo $array_jingfei_caizheng[8];?></td>
</tr>
<tr>
<td> 10.专家咨询费</td>
<td><?php echo $array_jingfei_yusuan[9];?></td>
<td><?php echo $array_jingfei_caizheng[9];?></td>
</tr>
<tr>
<td> 11.验收检查费</td>
<td><?php echo $array_jingfei_yusuan[10];?></td>
<td><?php echo $array_jingfei_caizheng[10];?></td>
</tr>
<tr>
<td> 12.管理费</td>
<td><?php echo $array_jingfei_yusuan[11];?></td>
<td><?php echo $array_jingfei_caizheng[11];?></td>
</tr>
<tr>
<td> 13．其他费用</td>
<td><?php echo $array_jingfei_yusuan[12];?></td>
<td><?php echo $array_jingfei_caizheng[12];?></td>
</tr>
</table>

<h2>七、需增添的仪器设备</h2>

<table class="paging showTable" border=1 cellspacing=0 cellpadding=0 height="25%">
<tr>
<td> 名称</td>
<td> 数 量</td>
<td> 单 价</td>
<td> 县财政拨款</td>
<td> 自筹或其他</td>
<td> 用途说明</td>
</tr>
<?php
$n=5;
for ($i=0;$i<$n;$i++) {
	echo "
	<tr>
	<td>$array_yiqi_name[$i]</td>
	<td>$array_yiqi_num[$i]</td>
	<td>$array_yiqi_price[$i]</td>
	<td>$array_yiqi_bokuan[$i]</td>
	<td>$array_yiqi_zichou[$i]</td>
	<td>$array_yiqi_yongtu[$i]</td>
	</tr>
	";
}
?>
</table>

<div style="line-height:30pt">
<div style="text-align:left;margin-top:5cm">
甲方(项目委托单位)：(盖章)<br>
单位负责人（签字）：<br>
<br>
</div>
<div style="text-align:right">
年 月  日
</div>
<br>
<br>
<br>
<div style="text-align:left">
乙方 (项目承担单位)：(盖章)<br>
项目（课题）负责人（签字）：<br>
单位负责人（签字）：<br>
财务负责人（签字）：<br>
开户银行及帐号：<br>
<br>
</div>
<div style="text-align:right">
年 月  日
</div>
</center>
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
</div>

</html>
