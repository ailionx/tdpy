<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$id = $_SESSION[id];
	$user = $_SESSION[user];
	if ($_GET['project_id']){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from tech_challenge where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		// 如果值为空就替换为空格，让表格显示更为美观
		foreach ($value as $key=>&$eachValue) {
			if ($eachValue === NULL) {								
				$eachValue = '&nbsp;';
			}
		}
		//不同用户进入此页面的权限
		permissionBlocker('show',$value[status]);
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
?>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<title>“企业难题招标、技术需求”工作单</title>
</head>

<div class="container">
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>

<center>
<h2>“企业难题招标、技术需求”工作单</h2>
编号：<?php echo sprintf('%03d',$value['project_id']); ?>
<table  class='showTable' height=75% border=1 cellspacing=0 cellpadding=8 >
 <tr>
  <td >
  企业名称
  （盖章）
  </td>
  <td  colspan=4>
  <?php echo replaceText($value[company_name]); ?>&nbsp;
  </td>
  <td  colspan=2>
  行 业
  </td>
  <td >
  <?php echo replaceText($value[industry_class]); ?>
  &nbsp;
  </td>
 </tr>
 <tr>
  <td >
  通讯地址
  </td>
  <td  colspan=4>
  <?php echo replaceText($value[company_address]); ?>&nbsp;
  </td>
  <td  colspan=2>
  邮 编
  </td>
  <td >
  <?php echo replaceText($value[post_code]); ?>
  &nbsp;
  </td>
 </tr>
 <tr>
  <td >
  网&nbsp; 址
  </td>
  <td  colspan=4>
  <?php echo replaceText($value[website]); ?>
  &nbsp;
  </td>
  <td  colspan=2>
  E-mail
  </td>
  <td >
  <?php echo replaceText($value[email]); ?>
  &nbsp;
  </td>
 </tr>
 <tr>
  <td >
  联 系 人
  </td>
  <td >
  <?php echo replaceText($value[contact_name]); ?>
  &nbsp;
  </td>
  <td >
  电 话
  </td>
  <td  colspan=2>
  <?php echo replaceText($value[contact_phone]); ?>
  &nbsp;
  </td>
  <td  colspan=2>
  传 真
  </td>
  <td >
  <?php echo replaceText($value[fax]); ?>
  &nbsp;
  </td>
 </tr>
 <tr>
  <td >
  技术难题名称
  （限100字内）
  </td>
  <td  colspan=7>
  <?php echo replaceText($value[project_name]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <td >
  主要内容和技术经济指标
  （限2000字内）
  </td>
  <td  colspan=7 valign=top>
  <?php echo replaceText($value[content]); ?>
  &nbsp;
  </td>
 </tr>
 <tr>
  <td >
  拟提供资金
  </td>
  <td  colspan=7>
  <?php echo replaceText($value[plan_fund]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <td >
  备&nbsp; 注
  （限500字内）
  </td>
  <td  colspan=7>
  <?php echo replaceText($value[comment]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <td >
  关 键 词
  （限100字内）
  </td>
  <td  colspan=7>
  <?php echo replaceText($value[key_words]); ?>
  </td>
 </tr>
 <tr>
  <td >
  起始时间
  </td>
  <td  colspan=3>
  <?php echo replaceText($value[start_time]); ?>
  </td>
  <td  colspan=2>
  截至时间
  </td>
  <td  colspan=2>
  <?php echo replaceText($value[finish_time]); ?>
  </td>
 </tr>
</table>
</center>
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
</div>
</html>
