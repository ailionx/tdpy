<?php
session_start();
include ("../../conf.php");
$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
$id = $_SESSION[id];
	$user = $_SESSION[user];
	if ($_GET['project_id']){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from tech_awards where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		
		//不同用户进入此页面的权限
		permissionBlocker('show',$value[status]);
		
		//反序列化数组存入的数据
		$arrayLong_list = array_field_inDB('tech_awards');
		foreach ($arrayLong_list as $arrayLong) {
			${$arrayLong} = unserialize($value[$arrayLong]);
		}
		
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
?>
<!DOCTYPE html5>
<html> 
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<style type="text/css">
/*@media screen {
	table.showTable {
		margin-bottom:80px;
		
	}
}*/
</style>
<title>浙江省科学技术奖项目推荐书</title>
</head>

<div class="container">
<center>
<div id='print_top'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
<p style="text-align:left">附件1：</p>
<h1>龙游县科学技术奖项目推荐书</h1>
<table border="0" class='showTable' style='margin-bottom:0px'>
<tr><td>
成果类别：<?php 
				$achievement_class_array = array('1'=>'基础研究类','2'=>'技术发明类','3'=>'技术开发类','4'=>'社会公益类','5'=>'重大工程类','6'=>'软科学类');
				foreach ($achievement_class_array as $arrKey=>$option) {
					if ($value['achievement_class']=== "$arrKey") {
						echo $option;
					}
				}
		?>
        </td><td>
行业评审组代码：<?php 
				$review_code_array = array('1'=>'机械、电力','2'=>'电子、通讯、信息','3'=>'化工、冶金、环保','4'=>'建设、建工、建材、交通、水工、地矿、煤炭','5'=>'轻工、纺织、食品','6'=>'农业','7'=>'畜牧、林业、渔业、气象、农田水利','8'=>'医学、卫生','9'=>'中医、医药、医疗器械（器材）','10'=>'软科学');
				foreach ($review_code_array as $arrKey=>$option) {
					if ($value['review_code']=== "$arrKey") {
						echo $option;
					}
				}
				?>
      </td><td>
编号：<?php echo $value['id_number'] ?></td>
</tr>
</table>
<table class='showTable paging' border=1 height=80%>
<tr >
<td  >
项目名称
</td>
<td  colspan=6 ><?php echo replaceText($value[project_name]);?></td> </tr>
<tr >
<td  >
主要完成人
</td>
<td  colspan=6 valign=top ><?php echo replaceText($value[primary_member]);?></td> </tr>
<tr >
<td  >
主要完成单位
</td>
<td  colspan=6 valign=bottom><?php echo replaceText($value[primary_unit]);?>
（省内第一完成单位盖印）
</td> </tr>
<tr >
<td  >
部门或单位推荐意见及建议奖励等级
</td>
<td  colspan=6 valign=bottom ><?php echo replaceText($value[unit_commemt]);?>
（盖章）
</td> </tr>
<tr >
<td  >
项目联系
人及电话
</td>
<td  colspan=2>
联系人：<?php echo replaceText($value[contact_name]);?>
<br>电话：<?php echo replaceText($value[contact_phone]);?>
</td>
<td  colspan=3 >
所属国民经济行业
</td>
<td  >
<?php 
				$financial_class_array = array('A'=>'农、林、牧、渔业','B'=>'采掘业','C'=>'制造业','D'=>'电力、煤气及水的生产和供应业','E'=>'建筑业','F'=>'地质勘察业、水利管理业','G'=>'交通运输、仓储及邮电通信业','H'=>'批发和零售贸易、餐饮业','I'=>'金融、保险业','J'=>'房地产业','K'=>'社会服务业','L'=>'卫生、体育和社会福利业','M'=>'教育、文化艺术和广播电影电视事业','N'=>'科学研究和综合技术服务业','O'=>'国家机关、党政机关和社会团体','P'=>'其它行业');
				foreach ($financial_class_array as $arrKey=>$option) {
					if ($value['financial_class']=== "$arrKey") {
						echo $option;
					}
				}
?>
</td> </tr>
<tr >
<td  >
任务来源
</td>
<td  >
<?php 
				$task_resource_array = array('A'=>'国家科技攻关','B'=>'国家“863计划”','C'=>'国家基础性研究重大项目计划','D'=>'国家科技型中小企业创新基金','E'=>'国家重点新产品计划','F'=>'国家其它计划','G'=>'省重大科研计划','H'=>'省重点科研计划','I'=>'省一般科研计划','J'=>'省科技型中小企业创新资金专项','K'=>'省自然科学基金','L'=>'省国际合作计划','M'=>'省新产品计划','N'=>'省其它科技计划','O'=>'其它部委计划','P'=>'其它单位委托','Q'=>'自选','R'=>'非职务');
				foreach ($task_resource_array as $arrKey=>$option) {
					if ($value['task_resource']=== "$arrKey") {
						echo $option;
					}
				}
?>
</td>
<td  colspan=3 >
成果水平
</td>
<td  colspan=2 >
<?php
			$achievement_level_array = array('international_lead'=>'国际领先','international_advanced'=>'国际先进','internal_lead'=>'国内领先','internal_advanced'=>'国内先进');
			foreach ($achievement_level_array as $arrKey=>$option) {
					if ($value['achievement_level']=== "$arrKey") {
						echo $option;
					}
				}
?>
</td> </tr>
<tr >
<td  >
主持评审单位
</td>
<td  ><?php echo replaceText($value[judgement_unit]);?></td>
<td  colspan=3 >
评审日期
</td>
<td  colspan=2 >
<?php echo replaceText($value[judgement_date]);?>
</td> </tr>
<tr >
<td  >
计划（基金）
名称和编号
</td>
<td  colspan=6 valign=top ><?php echo replaceText($value[fundation_name_num]);?></td> </tr>
<tr >
<td  >
项目起止时间
</td>
<td  colspan=3 ><?php echo replaceText($value[start_time]);?></td>
<td  colspan=3 ><?php echo replaceText($value[finish_time]);?></td> </tr>
</table>

<table class='showTable paging' border="1" height="80%">
<tr >
<td  colspan=7 valign=top style='text-align:left' height=300px>
项目简介（所属科学技术领域、主要内容、特点、推广应用、产业化情况）<br>
<?php echo replaceText($value[project_brief]);?>
</td> </tr>
<tr >
<td  valign=top  colspan=2 >创新类别</td>
<td  valign=top colspan=5> 
	<?php
			$innovate_class_array = array('original_innovate'=>'原始创新','integrate_innovate'=>'集成创新','introduce_innovate'=>'引进消化吸收再创新');
			foreach ($innovate_class_array as $arrKey=>$option) {
					if ($value['innovate_class']=== "$arrKey") {
						echo $option;
					}
				}
	?>
            </td> 
</tr>
<tr>
<td  colspan=7 valign=top style='text-align:left' height=300px>发现、发明及创新点（不超过400个汉字）：<br><?php echo replaceText($value[innovate_piont]);?><br></td> 
</tr>
<tr >
<td  colspan=7 valign=top  style='text-align:left' height=200px>
保密要点（不超过100个汉字）：<br><?php echo replaceText($value[classify_point]);?><br>

</td> </tr>
</table>

<table class='showTable paging' border=1 cellspacing=0 cellpadding=0 height=80%>
<tr >
<td  valign=top style='text-align:left' >
与当前国内外同类研究、同类技术的综合比较（不超过800个汉字）:
<br><?php echo replaceText($value[class_comparasion]);?><br>
</td> </tr>
</table>
<table class='showTable' border=1 cellspacing=0 cellpadding=0 valign=top style='text-align:left;margin-bottom:0;' height=80% >
<tr >
<td  valign=top style='text-align:left'>
推广应用情况（不超过800个汉字）
<br><?php 
echo replaceText($value[application_status]);
?><br>
</td> </tr>
</table>
<p style='margin-bottom:80px;margin-top:0px;text-align:left' class="paging">*应用情况栏目基础研究类项目填写研究论文、学术专著发表国内外引用情况</p>

<table class='showTable' border=1 style='margin-bottom:0px;' cellspacing=0 cellpadding=0  height=80%>
<tr>
<td  colspan=6 >
经济效益 （直接效益）&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
单位：万元（人民币）
</td> </tr>
<tr >
<td  >
项目总投资额
</td>
<td  colspan=5 ><?php echo replaceText($value[total_investment]);?></td> </tr>
<tr >
<td >
栏目
年份
</td>
<td  >
新增销售额
</td>
<td  >
新增利润
</td>
<td  >
新增税收
</td>
<td  >
创收外汇
（美元）
</td>
<td  >
节支总额
</td> </tr>
<?php
	for($i=0;$i<3;$i++) {
		echo "
	<tr>
		<td>$array_financial_year[$i] </td>
        <td>$array_financial_revenue[$i]</td>
        <td>$array_financial_benefit[$i]</td>
        <td>$array_financial_fax[$i]</td>
        <td>$array_financial_forex[$i]</td>
        <td>$array_financial_total[$i]</td>
    </tr>";
	}
?>
<tr >
<td  colspan=6 valign=top  style='text-align:left'>
各栏目的计算依据：
<br><?php echo replaceText($value[caculation_gist]);?><br>
</td> </tr>
<tr >
<td  colspan=6 valign=top  style='text-align:left'>
间接效益：
<br><?php echo replaceText($value[direct_benefit]);?><br>
</td> </tr>
<tr >
<td  colspan=6 valign=top  style='text-align:left'>
社会效益：
<br><?php echo replaceText($value[society_benefit]);?><br>
</td> </tr>
</table>
<p style='margin-bottom:80px;margin-top:0px;text-align:left' class="paging">*经济效益栏目基础研究类项目可不填写</p>

<h2>享有自主知识产权情况</h2>
<table class='showTable paging' border=1 cellspacing=0 cellpadding=0 height=80% >
<tr >
<td  >
国别
</td>
<td  >
申请号
</td>
<td  >
专利号
</td>
<td  >
项目名称
</td> </tr>
<?php
	for($i=0;$i<5;$i++) {
		echo "
	<tr>
        <td>$array_intellectual_country[$i]</td>
        <td>$array_intellectual_id[$i]</td>
        <td>$array_intellectual_number[$i]</td>
        <td>$array_intellectual_name[$i]</td>
    </tr>";
	}
?>
<tr >
<td  colspan=4 height=60% valign=top style='text-align:left'>
其他知识产权情况：（如著作权、软件登记、商标权、动植特新品种审定、药品、医疗器械、农药、食品或饲料添加剂、行业标准等证书）
<br><?php echo replaceText($value[other_property]);?><br>
</td> </tr>
</table>

<table class='showTable paging' border=1 cellspacing=0 cellpadding=0 height=80% >
<tr >
<td  colspan=4 valign=top style='text-align:left' >
评审（评审）意见：（全文）
<br><?php echo replaceText($value[judgement_opinion]);?><br>
</td> </tr>
</table>

<h2>评 审 委 员 会 名 单</h2>
<table class='showTable' border=1 cellspacing=0 cellpadding=0  height=80%>
<tr >
<td  >
序号
</td>
<td  >
评审会职务
</td>
<td  width='60px'>
姓 名
</td>
<td  >
工作单位
</td>
<td  >
所学专业
</td>
<td  >
现从事专业
</td>
<td  >
职称职务
</td>
<td  >
签 名
</td> </tr>
<?php
	for($i=0;$i<count($array_council_job);$i++) {
		$ii=$i+1;
		echo "
	<tr>
        <td>$ii</td>
        <td>$array_council_job[$i]</td>
        <td>$array_council_name[$i]</td>
        <td>$array_council_unit[$i]</td>
		<td>$array_council_speciality[$i]</td>
        <td>$array_council_career[$i]</td>
        <td>$array_council_title[$i]</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>";
	}
?>

</table>
<p style='margin-bottom:80px;margin-top:0px;text-align:left' class="paging">*请在纸质材料中附上原件复印件</p>
<h2>主 要 完 成 单 位 情 况</h2>
<table class='showTable' border=1 cellspacing=0 cellpadding=0 style='margin-bottom:0' height=70%>
<tr >
<td  >
序号
</td>
<td  colspan=2 >
完 成 单
位 名 称
</td>
<td  >
邮政编码
</td>
<td  >
详 细 通
信 地 址
</td>
<td  >
隶属部门
</td>
<td  >
单位
属性
</td> </tr>
<?php
	for($i=0;$i<count($array_primary_unit_name);$i++) {
		$ii=$i+1;
		echo "
	<tr id='tr_primary_unit_$i'>
        <td>$ii</td>
        <td colspan=2 >$array_primary_unit_name[$i]</td>
		<td>$array_primary_unit_subname[$i]</td>
        <td>$array_primary_unit_code[$i]</td>
        <td>$array_primary_unit_adress[$i]</td>
		<td>$array_primary_unit_apartment[$i]</td>
    </tr>";
	}
?>
</table>

<ol style='text-align:left;margin-bottom:80px'  class='paging'>注：
<li>完成单位序号超过9个可加附页。其顺序必须与评审证书封面上的完全一致。</li>
<li>完成单位名称必须填写全称，不得简化，与单位公章完全一致，并填入完成单位名称的第一栏中，其下属机构名称则填入第二栏中。</li>
<li>详细通信地址要写明县、街道和门牌号码。</li>
<li>隶属部门是指本单位和行政关系隶属哪一个主管部门。并将其名称填入表中。</li>
<li>单位属性是指本单位在  1.独立科研机构　2.大专院校  3.国有企业  4.民营企业  5.其他 五类性质中属于哪一类，在栏中选填1.2.3.4.5.既可。</li>
</ol>
<h2>主 要 完 成 人 员 情 况</h2>
<table class='showTable' border=1 cellspacing=0 style='margin-bottom:0px;' cellpadding=0 height=80% >
<tr >
<td  >
序号
</td>
<td  >
姓名
</td>
<td  >
性别
</td>
<td  >
出生年月
</td>
<td  >
技术职称
</td>
<td  >
文化程度
</td>
<td  >
工作单位
</td>
<td  >
对成果创造性贡献
</td>
<td  >
签名
</td> </tr>
<?php
	for($i=0;$i<count($array_primary_member_name);$i++) {
		$ii=$i+1;
		echo "
	<tr id='tr_primary_member_$i'>
        <td>$ii</td>
        <td>$array_primary_member_name[$i]</td>
        <td>$array_primary_member_sex[$i]</td>
        <td>$array_primary_member_birthday[$i]</td>
		<td>$array_primary_member_title[$i]</td>
        <td>$array_primary_member_education[$i]</td>
		<td>$array_primary_member_unit[$i]</td>
		<td>$array_primary_member_contribution[$i]</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>";
	}
?>
</table> 
<ol style='text-align:left;margin-bottom:80px;text-align:left' class="paging">注：
<li>主要完成人员顺序必须与评审证书上的顺序完全一致，并与推荐书首页主要完成人员的顺序一致。</li>
<li>本人签名栏不得代签。</li>
</ol>

<h2>第一主要完成人情况</h2>
<table class='showTable paging' border=1 cellspacing=0 cellpadding=0 height=80%>
<tr >
<td  >姓名</td>
<td  colspan=3 ><?php echo $array_mvp_info[name];?></td>
<td  colspan=3 >性别</td>
<td  colspan=2 ><?php echo $array_mvp_info[sex];?></td>
<td  colspan=2 >民族</td>
<td  ><?php echo $array_mvp_info[nation];?></td> 
</tr>
<tr >
<td colspan=2 >出生地</td>
<td  colspan=5 ><?php echo $array_mvp_info[birthplace];?></td>
<td  colspan=1 >出生日期</td>
<td  colspan=2 ><?php echo $array_mvp_info[birthday];?></td>
<td  colspan=1 >党派</td>
<td  ><?php echo $array_mvp_info[partygroup];?></td> 
</tr>
<tr >
<td  colspan=5>工作单位</td>
<td  colspan=5 ><?php echo $array_mvp_info[unit];?></td>
<td  colspan=1 >联系电话</td>
<td  colspan=1 ><?php echo $array_mvp_info[phone];?></td> </tr>
<tr >
<td  colspan=6>通讯地址及邮政编码</td>
<td  colspan=6 ><?php echo $array_mvp_info[adress];?></td> 
</tr>
<tr >
<td  colspan=5>家庭住址</td>
<td  colspan=4 ><?php echo $array_mvp_info[home];?></td>
<td  colspan=2 >住宅电话</td>
<td  colspan=1 ><?php echo $array_mvp_info[homenumber];?></td> </tr>
<tr >
<td colspan=5 >电子信箱</td>
<td  colspan=7 ><?php echo $array_mvp_info[email];?></td> </tr>
<tr >
<td  colspan=5>毕业学校</td>
<td  colspan=3 ><?php echo $array_mvp_info[school];?></td>
<td  colspan=1 >文化程度</td>
<td  colspan=1 ><?php echo $array_mvp_info[education];?></td>
<td  colspan=1 >学位</td>
<td  colspan=1 ><?php echo $array_mvp_info[degree];?></td> </tr>
<tr >
<td  colspan=5>职务、职称</td>
<td  colspan=3 ><?php echo $array_mvp_info[title];?></td>
<td  colspan=1 >专业、专长</td>
<td  colspan=1 ><?php echo $array_mvp_info[speciality];?></td>
<td  colspan=1 >毕业时间</td>
<td  colspan=1 ><?php echo $array_mvp_info[graduatedate];?></td> </tr>
<tr >
<td  colspan=4 >曾获奖励及荣誉称号情况：</td>
<td  colspan=8 style="text-align:left"><?php echo $array_mvp_info[awards];?></td> </tr>
<tr >
<td  colspan=4 >参加本项目的起止时间</td>
<td  colspan=8 >自<?php echo $array_mvp_info[syear];?>年<?php echo $array_mvp_info[smonth];?>月 至 于&nbsp;<?php echo $array_mvp_info[fyear];?>年<?php echo $array_mvp_info[fmonth];?>月</td>
 </tr>
<tr >
<td  >对本项目的主要学术（技术）贡献</td>
<td  colspan=11 >
<p style="text-align:left"><?php echo $array_mvp_info[contribution];?></p>
<p style="text-align:right">本人签名：&nbsp;&nbsp;&nbsp;&nbsp;
单位盖印：&nbsp;
年&nbsp;
月&nbsp;
日&nbsp;</p>
</td> </tr>
</table>
<h2>专 家 推 荐 意 见</h2>
（推荐类别仅限基础研究类项目或技术发明类项目）
<table class='showTable' border=1 cellspacing=0 cellpadding=0 style='margin-bottom:0' height=80%>
<tr >
<td  rowspan=4 >
推<br>
荐<br>
专<br>
家<br>
情<br>
况
</td>
<td >
姓名
</td>
<td ><?php echo $array_specialist_info[name];?></td>
<td  >
工作单位
</td>
<td  colspan=3 ><?php echo $array_specialist_info[unit];?></td> </tr>
<tr >
<td  colspan=2 >
通信
地址
</td>
<td  colspan=2 ><?php echo $array_specialist_info[adress];?></td>
<td  >
联系电话
</td>
<td  ><?php echo $array_specialist_info[phone];?></td> </tr>
<tr >
<td  colspan=2 >
专业、专长
</td>
<td  colspan=2 ><?php echo $array_specialist_info[speciality];?></td>
<td  rowspan=2 >
专家情况
（用√表示）
</td>
<td  rowspan=2 >
         <?php

			$array_specialist_info_prize_array = array('highest_prize'=>'最高科技奖得主','CASer'=>'中国科学院院士','CAEer'=>'中国工程院院士');
			foreach ($array_specialist_info_prize_array as $arrKey=>$option) {
					if ($array_specialist_info_prize[$arrKey]=== "$arrKey") {
						echo "$option<input type='checkbox' disabled name=array_specialist_info_prize[$arrKey] value=$arrKey checked><br>";
					} else {
						echo "$option<input type='checkbox' disabled name=array_specialist_info_prize[$arrKey] value=$arrKey><br>";
					}
				}
		?>
</td> </tr>
<tr >
<td  colspan=2 >
现从事的科学技术工作
</td>
<td  colspan=2 valign=top ><?php echo $array_specialist_info[job];?></td> </tr>
<tr >
<td  colspan=2 >
推<br>
荐<br>
意<br>
见<br>
</td>
<td  colspan=5 valign=top style="text-align:left" >（不超过500个汉字）
<br><?php echo $array_specialist_info[comment];?><br>
专家本人签字：
年 月 日
</td> </tr>
</table>
<p  style='margin-bottom:80;text-align:left' class="paging">(单位推荐的项目，此表不填)</p>

<h3>附件目录</h3>
<ol style="text-align:left">
<li>技术评价证明（原件）</li>
<li>试制工作总结、技术总结材料</li>
<li>检验报告</li>
<li>经济效益证明</li>
<li>应用证明</li>
<li>“享有自主知识产权情况”栏填写的是指国家专利证书及发明权利要求书，著作权、软件登记、商标权、动植特新品种审定、药品、医疗器械、农药、食品或饲料添加剂、行业标准等证书）</li>
<li>其他证明</li>
</ol>
<h4>填写说明</h4>
<ol style="text-align:left">
<li>成果类别：①基础研究类；②技术发明类；③技术开发类；④社会公益类；⑤重大工程类；⑥软科学类。</li>
<li>行业评审组代码：①机械、电力②电子、通讯、信息③化工、冶金、环保④建设、建工、建材、交通、水工、地矿、煤炭⑤轻工、纺织、食品⑥农业⑦畜牧、林业、渔业、气象、农田水利⑧医学、卫生⑨中医、医药、医疗器械（器材）⑩软科学</li>
<li>“所属国民经济行业”按推荐项目所属行业在相应字母上划“√”。</li>
国家标准《GB4754—94》国民经济行业分16个门类：（A）农、林、牧、渔业；（B）采掘业；（C）制造业；（D）电力、煤气及水的生产和供应业；（E）建筑业；（F）地质勘察业、水利管理业；（G）交通运输、仓储及邮电通信业；（H）批发和零售贸易、餐饮业；（I）金融、保险业；（J）房地产业；（K）社会服务业；（L）卫生、体育和社会福利业；（M）教育、文化艺术和广播电影电视事业；（N）科学研究和综合技术服务业；（O）国家机关、党政机关和社会团体；（P）其它行业。</li>
<li>“任务来源”按推荐项目所属计划在相应字母上划“√”。</li>
<ol type="A">
<li>国家科技攻关</li><li>国家“863计划”</li><li>国家基础性研究重大项目计划</li>
<li>国家科技型中小企业创新基金</li><li>国家重点新产品计划</li>
<li>国家其它计划</li><li>省重大科研计划</li><li>省重点科研计划</li>
<li>省一般科研计划</li><li>省科技型中小企业创新资金专项</li>
<li>省自然科学基金</li><li>省国际合作计划</li><li>省新产品计划</li>
<li>省其它科技计划</li><li>其它部委计划</li><li>其它单位委托</li>
<li>自选</li><li>非职务</li>
</ol>

<li>“经济效益”栏中填写的数字只填写在推荐前三年项目完成单位（包括以该项目技术出资入股或转让的相关企业）销售申报项目产品（或技术）所取得的直接效益，若申报一等奖（基础研究类、社会公益类、软科学项目除外）项目，要求提交销售发票复印件或专项审计报告。<br>
  各栏目的计算依据，应就生产或应用该项目后产生的直接累计净增效益以及提高产品的质量、提高劳动生产率等作用简要说明，并具体列出本表所填各项效益的计算方法和计算依据。</li>
<li>“间接效益”是指项目用户单位应用该项目（技术）后取得的效益。</li>
<li>“社会效益”栏填写的是指推荐项目在推动科学技术进步，保护自然资源或生态环境；提高国防能力；保障国家和社会安全；改善人民物质、文化、生活及健康水平等方面所起的作用，应扼要地做出说明。</li>
<li>中国科学院院士、中国工程院院士推荐的项目，应有3名院士以上共同推荐，“专家推荐意见”由推荐人独立填写、不打印、不代填后签名，不可联名推荐。</li>
</ol>
龙游县科学技术局制
二〇一〇年
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
</center>
</div>

</html>
