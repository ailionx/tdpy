<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
		$user = $_SESSION[user];
	if ($_GET['project_id']){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from tech_contract where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		//不同用户进入此页面的权限
		permissionBlocker('show',$value[status]);
		//反序列化数组存入的数据
		$arrayLong_list = array_field_inDB('tech_contract');
		foreach ($arrayLong_list as $arrayLong) {
			${$arrayLong} = unserialize($value[$arrayLong]);
		}
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
?>
<!DOCTYPE HTML5>
<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<TITLE>技 术 合 同 登 记 表</TITLE>
	<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
	<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
	<STYLE TYPE="text/css">
ul {
	list-style:none;
}

ul.check {
	text-align:left;
}
	</STYLE>
</HEAD>
<div class="container">
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
	<CENTER>
<h3>技术合同登记表</h3>
		<TABLE  class='showTable' height=75% CELLPADDING=3 CELLSPACING=0 border=1>
			<TR>
				<TD COLSPAN=4 >
					合同有效日期&nbsp;<?php echo replaceText($value[valid_start]); ?>&nbsp;至&nbsp;<?php echo replaceText($value[valid_end]); ?></TD>
			</TR><TR>
				<TD>合同编号</TD>
				<TD><?php echo replaceText($value[contract_code]); ?></TD>
				<TD>项目名称</TD>
				<TD><?php echo replaceText($value[project_name]); ?></TD>
			</TR><TR>
				<TD>买方名称</TD>
				<TD><?php echo replaceText($value[buyer_name]); ?></TD>
				<TD>合同签订日期</TD>
				<TD><?php echo replaceText($value[signed_data]); ?></TD>
			</TR><TR>
				<TD>负责人</TD>
				<TD><?php echo replaceText($value[manager_name]); ?></TD>
				<TD>合同类别</TD>
				<TD><?php echo replaceText($value[contract_class]); ?></TD>
			</TR><TR>
				<TD>买方类别</TD>
				<TD><?php echo replaceText($value[buyer_class]); ?></TD>
				<TD>计划类别</TD>
				<TD><?php echo replaceText($value[plan_class]); ?></TD>
			</TR><TR>
				<TD>卖方名称</TD>
				<TD><?php echo replaceText($value[seller_name]); ?></TD>
				<TD ROWSPAN=2>社会经济目标</TD>
				<TD ROWSPAN=2><?php echo replaceText($value[financal_target]); ?></TD>
			</TR><TR>
				<TD>卖方地区</TD>
				<TD><?php echo replaceText($value[seller_area]); ?></TD>
			</TR><TR>
				<TD>卖方类别</TD>
				<TD><?php echo replaceText($value[seller_class]); ?></TD>
				<TD>合同成交金额</TD>
				<TD><?php echo replaceText($value[transaction_amount]); ?>万元</TD>
			</TR><TR>
				<TD>卖方通讯地址</TD>
				<TD><?php echo replaceText($value[seller_adress]); ?></TD>
				<TD>其中:技术交易额</TD>
				<TD><?php echo replaceText($value[tech_amount]); ?>万元</TD>
			</TR><TR>
				<TD>卖方邮政编码</TD>
				<TD><?php echo replaceText($value[seller_zcode]); ?></TD>
				<TD>预计新增投入</TD>
				<TD><?php echo replaceText($value[additional_invest]); ?>万元</TD>
			</TR><TR>
				<TD>卖方联系电话</TD>
				<TD><?php echo replaceText($value[seller_phone]); ?></TD>
				<TD>预计新增产值</TD>
				<TD><?php echo replaceText($value[aditional_output]); ?>万元</TD>
			</TR><TR>
				<TD COLSPAN=4 >
					请选择合同公开项(注意：如果合同有公开项，则合同名称和编号将自动公开)

<ul class="check">
<?php
	$array_public_item_rule = array('name_code_p'=>'合同名称和编号',
								'party_name_p'=>'当事人名称',
								'signed_info_p'=>'签订的时间地点和有效期',
								'class_target_p'=>'合同类别和计划类别和社会经济目标',
								'contract_amount_p'=>'合同金额',
								);
	foreach ($array_public_item_rule as $public_item_key=>$public_item) {
		if ($array_public_item[$public_item_key]==$public_item) {
			echo "<li><input type='checkbox' name=array_public_item[$public_item_key] value=$public_item checked disabled>$public_item</li>";
		} else {
			echo "<li><input type='checkbox' name=array_public_item[$public_item_key] value=$public_item disabled>$public_item</li>";
		}
	}
?>
</ul></TD>
			</TR>
		</TABLE>
	</CENTER>
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
</div>

</BODY>
</HTML>
