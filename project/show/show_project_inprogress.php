<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$id = $_SESSION[id];
	$user = $_SESSION[user];
	if ($_GET['project_id']){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from project_inprogress where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		// 如果值为空就替换为空格，让表格显示更为美观
		foreach ($value as $key=>&$eachValue) {
			if ($eachValue === NULL) {								
				$eachValue = '&nbsp;';
			}
		}
		//不同用户进入此页面的权限
		permissionBlocker('show',$value[status]);
		
		//背景水印控制，如果项目是已审核或已立项，则背景有水印
		if ($value['project_status']=='6') {
			$check_bg = 'url(../pic/check_passed_05.jpg)'; 
		} else {
			$check_bg = 'none'; 
		}
		
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
?>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<title>项目执行情况中期检查表</title>
</head>

<center>
<h2>项目执行情况中期检查表</h2>

<br>
<h4>一、项目基本情况</h4>

<table border=1 cellspacing=0 cellpadding=8 width="756">
 <tr>
  <td>
  项目名称
  </td>
  <td colspan=3>
  <?php echo replaceText($value[project_name]); ?>
  &nbsp;
  </td>
 </tr>
 <tr>
  <td width="20%">
  计划编号
  </td>
  <td width="30%">
  <?php echo replaceText($value[project_code]); ?>
  &nbsp;
  </td>
  <td width="20%">
  开始日期
  </td>
  <td width="30%">
  <?php echo replaceText($value[start_date]); ?>
  &nbsp;
  </td>
 </tr>
 <tr>
  <td>
  承担单位
  </td>
  <td>
  <?php echo replaceText($value[assume_unit]); ?>
  &nbsp;
  </td>
  <td>
  结束日期
  </td>
  <td>
  <?php echo replaceText($value[finish_date]); ?>
  &nbsp;
  </td>
 </tr>
 <tr>
  <td>
  项目负责人
  </td>
  <td>
  <?php echo replaceText($value[project_manager]); ?>
  &nbsp;
  </td>
  <td>
  联系电话
  </td>
  <td>
  <?php echo replaceText($value[manager_phone]); ?>
  &nbsp;
  </td>
 </tr>
 <tr>
  <td>
  电子邮件
  </td>
  <td>
  <?php echo replaceText($value[manager_email]); ?>
  &nbsp;
  </td>
  <td>
  通讯地址
  </td>
  <td>
  <?php echo replaceText($value[adress]); ?>
  &nbsp;
  </td>
 </tr>
</table>
<br>
<h4>二、项目资金及其他配套条件落实情况</h4>

<table border=1 cellspacing=0 cellpadding=8 width="756">
 <tr>
  <td width="21%">&nbsp;
  
  </td>
  <td width="19%">
  项目总经费
  </td>
  <td width="19%">
  其中：自筹
  </td>
  <td width="19%">
  财政拨款
  </td>
  <td width="19%">
  银行贷款
  </td>
 </tr>
 <tr>
  <td>
  预算总额（按合同）
  </td>
  <td>
  <?php echo replaceText($value[total_budget]); ?>
  &nbsp;
  </td>
  <td>
  <?php echo replaceText($value[self_budget]); ?>
  &nbsp;
  </td>
  <td>
  <?php echo replaceText($value[fiscal_budget]); ?>
  &nbsp;
  </td>
  <td>
  <?php echo replaceText($value[loan_budget]); ?>
  &nbsp;
  </td>
 </tr>
 <tr>
  <td>
  已落实经费
  </td>
  <td>
  <?php echo replaceText($value[total_implemented]); ?>
  &nbsp;
  </td>
  <td>
  <?php echo replaceText($value[self_implemented]); ?>
  &nbsp;
  </td>
  <td>
  <?php echo replaceText($value[fiscal_implemented]); ?>
  &nbsp;
  </td>
  <td>
  <?php echo replaceText($value[loan_implemented]); ?>
  &nbsp;
  </td>
 </tr>
</table>

<br>
<h4>三、项目执行情况</h4>

<table border=1 cellspacing=0 cellpadding=8 width="756" height="200">
 <tr>
  <td valign=top>
  <?php echo replaceText($value[progress_status]); ?>
  </td>
 </tr>
</table>

</center>
<div id='edit_button'>
<?php
if ($value[user] == $_SESSION[user]){
	echo "<span class='print' style='font-size:9px'>如果您需要修改信息,<a href=../edit/edit_project_inprogress.php>点击这里</a></span><br>";
}
?>
</div>
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>

</html>
