<?php
session_start();
include ("../../conf.php");
$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
if ($_SESSION[pems]==1 || $_SESSION[pems]==11 || $_SESSION[pems]==12 || $_SESSION[pems]==13) {
	if ($_GET[company_name]){
		$company_name = $_GET[company_name];
	} else {
		echo "<script>alert('请指定企业名称')</script>";
		exit();
	}
} else {
	$company_name = $_SESSION[company_name];
}
$query = "select * from company_info where company_name='$company_name'";
$result = mysql_query($query);
$value = mysql_fetch_array($result);

// 如果值为空就替换为空格，让表格显示更为美观
		foreach ($value as $key=>&$eachValue) {
			if ($eachValue === NULL) {								
				$eachValue = '&nbsp;';
			}
		}

// 从mysql中读取checkbox的内容并显示
	$arrayLong_list = array_field_inDB('company_info');
	foreach ($arrayLong_list as $arrayLong) {
		${$arrayLong} = unserialize($value[$arrayLong]);
	}
	$array_company_class_list = array(hightech_country,hightech_city,shengchuangxinshifan,shengchuangxinxingshidian,shengkejixingzhongxiao,shengzhuanlishifan,shengnongyekeji,shizhuanlishifan,shichuangxinxing,xiankejixing,class_other);
	foreach ($array_company_class_list as $cla) {
		if ($array_company_class[$cla]) {
			${$cla}= $cla." checked";
		} else {
			${$cla}= $cla;
		}
	}
	$array_reserch_class_list = array( shengjiqiyeyanjiuyuan,shengjigaoxinjishuqiyefazhanzhongxin,shengjinongyeqiyekejiyanfazhongxin,shengjiquyukejichuangxinfuwuzhongxin,shengjidayuanmingxiaogongjianchuangxinzaiti,shijigongchengjishuyanjiukaifazhongxin,qiyeshuyanfajigou,rd_other);
	foreach ($array_reserch_class_list as $cla) {
		if ($array_reserch_class[$cla]) {
			${$cla}= $cla." checked";
		} else {
			${$cla}= $cla;
		}
	}
?>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<style type="text/css">
@media screen {
	.container {
		position:relative;
		top:33px;
	}
	table.showCompany td,table.showCompany th { 
	font-size:12px;
	}
	table.showCompany { 
	border-collapse:collapse; 
	border-spacing:0; 
	}
	
}
</style>
<title>科技型企业基本信息表</title>
</head>
<body>
<div class='container'>
<h3 class='print_show'>科技型企业基本信息表</h3>

<table id='company_info_id' class="showCompany" border=1 cellspacing=0 height='80%' width='96%' align="center" style="font-size:12px">
 <tr>
  <td >
  企业名称
  </td>
  <td colspan=5 >
  <?php echo replaceText($value[company_name]) ?>
  </td>
  <td colspan=5 >
  成立时间
  </td>
  <td colspan=2 >
  <?php echo replaceText($value[establish_time]) ?>
  </td>
 </tr>
 <tr>
  <td >
  企业地址
  </td>
  <td colspan=5 >
  <?php echo replaceText($value[company_address]) ?>
  </td>
  <td colspan=5 >
  行业类别
  </td>
  <td colspan=2>
  <?php echo replaceText($value[industry_class]) ?>
  </td>
 </tr>
 <tr>
  <td >
  注册时间
  </td>
  <td colspan=4 >
  注册资本
  </td>
  <td colspan=8 >
  组织机构代码
  </td>
 </tr>
 <tr>
  <td >
  <?php echo replaceText($value[register_time]) ?>
  </td>
  <td colspan=4 >
  <?php echo replaceText($value[registered_capital]) ?>
  </td>
  <td colspan=8 >
  <?php echo replaceText($value[company_code]) ?>
  </td>
 </tr>
 <tr>
  <td >
  法人代表
  </td>
  <td colspan=2 >
  <?php echo replaceText($value[corporate_rep]) ?>
  </td>
  <td colspan=2 >
  联系方式
  </td>
  <td colspan=4 >
  <?php echo replaceText($value[rep_phone]) ?>
  </td>
  <td colspan=3 >
  职务/职称
  </td>
  <td >
  <?php echo replaceText($value[rep_title]) ?>
  </td>
 </tr>
 <tr>
  <td >
  联系人
  </td>
  <td colspan=2 >
  <?php echo replaceText($value[contact]) ?>
  </td>
  <td colspan=2 >
  联系方式
  </td>
  <td colspan=4 >
  <?php echo replaceText($value[contact_phone]) ?>
  </td>
  <td colspan=3 >
  职务/职称
  </td>
  <td >
  <?php echo replaceText($value[contact_title]) ?>
  </td>
 </tr>
 <tr>
  <td >
  传真
  </td>
  <td colspan=4 >
  网址
  </td>
  <td colspan=8 >
  电子邮箱
  </td>
 </tr>
 <tr>
  <td >
  <?php echo replaceText($value[fax]) ?>
  </td>
  <td colspan=4 >
  <?php echo replaceText($value[website]) ?>
  </td>
  <td colspan=8 >
  <?php echo replaceText($value[email]) ?>
  </td>
 </tr>
 <tr>
  <td rowspan=4 >
  企业规模
  和经营情况
  </td>
  <td colspan=2 >
  占地面积
  </td>
  <td colspan=2 >
  营业收入
  </td>
  <td colspan=2 >
  职工总数
  </td>
  <td colspan=6 >
  大专以上职工数
  </td>
 </tr>
 <tr>
  <td colspan=2 >
  <?php echo replaceText($value[floor_space]) ?>
  </td>
  <td colspan=2 >
  <?php echo replaceText($value[revenue]) ?>
  </td>
  <td colspan=2 >
  <?php echo replaceText($value[work_force]) ?>
  </td>
  <td colspan=6 >
  <?php echo replaceText($value[high_work_force]) ?>
  </td>
 </tr>
 <tr>
  <td colspan=2 >
  利税收入
  </td>
  <td colspan=2 >
  上缴税额
  </td>
  <td colspan=8 >
  高新技术产品、新产品等销售收入
  </td>
 </tr>
 <tr>
  <td colspan=2 >
  <?php echo replaceText($value[tax_revenue]) ?>
  </td>
  <td colspan=2 >
  <?php echo replaceText($value[pay_tax]) ?>
  </td>
  <td colspan=8 >
  <?php echo replaceText($value[hightech_revenue]) ?>
  </td>
 </tr>
 </tr>
 <tr >
  <td >
  经营范围
  </td>
  <td colspan=12 >
  <?php echo replaceText($value[business_scope]) ?>
  </td>
 </tr>
 <tr >
  <td >
  企业类别
  </td>
  <td style='text-align:left' colspan=12 >
		高新技术企业 <input disabled type="checkbox" name="array_company_class[hightech_country]" value=<?php echo $hightech_country; ?> >国家  <input disabled type="checkbox" name="array_company_class[hightech_city]" value=<?php echo $hightech_city; ?> >市&nbsp;&nbsp;
        <span style='text-align:right'><input disabled type="checkbox" name="array_company_class[shengchuangxinshifan]" value=<?php echo $shengchuangxinshifan; ?> >省级创新型示范企业</span><br>
    	<input disabled type="checkbox" name="array_company_class[shengchuangxinxingshidian]" value=<?php echo $shengchuangxinxingshidian; ?> >省级创新型试点企业&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input disabled type="checkbox" name="array_company_class[shengkejixingzhongxiao]" value=<?php echo $shengkejixingzhongxiao; ?> >省级科技型中小企业<br>
    	<input disabled type="checkbox" name="array_company_class[shengzhuanlishifan]" value=<?php echo $shengzhuanlishifan; ?> >省级专利示范企业&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input disabled type="checkbox" name="array_company_class[shengnongyekeji]" value=<?php echo $shengnongyekeji; ?> >省级农业科技型企业<br>
    	<input disabled type="checkbox" name="array_company_class[shizhuanlishifan]" value=<?php echo $shizhuanlishifan; ?> >市级专利示范企业&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input disabled type="checkbox" name="array_company_class[shichuangxinxing]" value=<?php echo $shichuangxinxing; ?> >市级创新型企业<br>
    	<input disabled type="checkbox" name="array_company_class[xiankejixing]" value=<?php echo $xiankejixing; ?> >县级科技型企业&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input disabled type="checkbox" name="array_company_class[class_other]" value=<?php echo $class_other; ?> >其他
  </td>
 </tr>
 <tr >
  <td >
  研发中心类别
  </td>
  <td style='text-align:left' colspan=12 >
		<input disabled type="checkbox" name="array_reserch_class[shengjiqiyeyanjiuyuan]"  value=<?php echo $shengjiqiyeyanjiuyuan; ?> >省级企业研究院&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input disabled type="checkbox" name="array_reserch_class[shengjigaoxinjishuqiyefazhanzhongxin]"  value=<?php echo $shengjigaoxinjishuqiyefazhanzhongxin; ?> >省级高新技术企业研发中心<br>
    	<input disabled type="checkbox" name="array_reserch_class[shengjinongyeqiyekejiyanfazhongxin]"  value=<?php echo $shengjinongyeqiyekejiyanfazhongxin; ?> >省级农业企业科技研发中心
        <input disabled type="checkbox" name="array_reserch_class[shengjiquyukejichuangxinfuwuzhongxin]"  value=<?php echo $shengjiquyukejichuangxinfuwuzhongxin; ?> >省级区域科技创新服务中心<br>
    	<input disabled type="checkbox" name="array_reserch_class[shengjidayuanmingxiaogongjianchuangxinzaiti]"  value=<?php echo $shengjidayuanmingxiaogongjianchuangxinzaiti; ?> >省级大院名校共建创新载体
        <input disabled type="checkbox" name="array_reserch_class[shijigongchengjishuyanjiukaifazhongxin]"  value=<?php echo $shijigongchengjishuyanjiukaifazhongxin; ?> >市级工程技术研究开发中心<br>
    	<input disabled type="checkbox" name="array_reserch_class[qiyeshuyanfajigou]"  value=<?php echo $qiyeshuyanfajigou; ?> >企业所属研发机构&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input disabled type="checkbox" name="array_reserch_class[rd_other]"  value=<?php echo $rd_other; ?> >其他
  </td>
 </tr>
 <tr>
  <td rowspan=6 >
  科研活动情况
  </td>
  <td colspan=5>
  科技人员数
  </td>
  <td colspan=5 >
  技术依托单位
  </td>
  <td colspan=5 >
  研发费占销售收入比
  </td>
 </tr>
 <tr>
  <td  colspan=5>
  <?php echo replaceText($value[tech_member]) ?>
  </td>
  <td colspan=5 >
  <?php echo replaceText($value[jishuyituodanwei]) ?>
  </td>
  <td colspan=5 >
  <?php echo replaceText($value[rd_sale_ratio]) ?>
  </td>
 </tr>
 <tr>
  <td colspan=4>
  发明专利数
  </td>
  <td colspan=4 >
  实用新型专利
  </td>
  <td colspan=4 >
  外观设计专利数
  </td>
 </tr><tr>
 <td>当年</td><td><?php echo replaceText($value[invent_patent]) ?></td><td>累计</td> <td><?php echo replaceText($value[invent_patent_all]) ?></td>
 <td>当年</td><td><?php echo replaceText($value[practical_patent]) ?></td><td>累计</td><td><?php echo replaceText($value[practical_patent_all]) ?></td>
 <td>当年</td><td><?php echo replaceText($value[facade_patent]) ?></td><td>累计</td><td><?php echo replaceText($value[facade_patent_all]) ?></td>
 </tr><tr>
 <td colspan=4 >
 科技成果登记及获奖情况
  </td>
  <td colspan=8 >
  <?php echo replaceText($value[tech_awards]) ?>
  </td>
 </tr>
 <tr >
 <td colspan=4 >
 高新技术产品和新产品
  </td>
  <td colspan=8 >
  <?php echo replaceText($value[high_product]) ?>
  </td>
 </tr>
 <tr>
  <td rowspan=5 >科技创新项目意向</td>
  <td colspan=4 >专利申报</td>
  <td colspan=4 >研发中心建设</td>
  <td colspan=4 >科技型企业升级</td>
 </tr>
  <tr>
  <td>发明</td>
  <td colspan=2 >实用新型</td>
  <td>外观设计</td>
  <td>国家级</td>
  <td>省级</td>
  <td>市级</td>
  <td>县级</td>
  <td>国家级</td>
  <td>省级</td>
  <td>市级</td>
  <td colspan=2 >县级</td>
 </tr>
 <tr >
  <td><?php echo replaceText($value[patent_invent]) ?></td>
  <td colspan=2 ><?php echo replaceText($value[patent_practical]) ?></td>
  <td><?php echo replaceText($value[patent_appear]) ?></td>

 <?php
$research_construction_arr = array('construction_country'=>'国家级','construction_province'=>'省级','construction_city'=>'市级','construction_county'=>'县级');
foreach ($research_construction_arr as $key=>$option) {
	if ($array_research_construction[$key] === '是') {
		echo "<td>是</td>";
	} else {
		echo "<td>否</td>";
	}
}
?>

<?php
$tech_upgrade_arr = array('upgrade_country'=>'国家级','upgrade_province'=>'省级','upgrade_city'=>'市级','upgrade_county'=>'县级');
foreach ($tech_upgrade_arr as $key=>$option) {
	if ($array_tech_upgrade[$key] === '是') {
		echo "<td>是</td>";
	} else {
		echo "<td>否</td>";
	}
}
?>

 </tr>
 <tr>
  <td colspan=4 >
  技术、产品开发引进
  </td>
  <td colspan=4 >
  科企合作
  </td>
  <td colspan=4 >
  其他
  </td>
 </tr>
 <tr >
  <td colspan=4 >
  <?php echo replaceText($value[tech_introduce]) ?>
  </td>
  <td colspan=4 >
  <?php echo replaceText($value[tech_coorperation]) ?>
  </td>
  <td colspan=4 >
  <?php echo replaceText($value[orientation_other]) ?>
  </td>
 </tr>
</table>
<div id='edit_button'>
<?php

//if ($value[user] == $_SESSION[user]){
//	echo "<span class='print' style='font-size:12px'>如果您需要修改企业信息,<a href=../edit/edit_company_info.php>点击这里</a></span><br>";
//}
?>
<span class='print' style='font-size:12px'>如果您需要修改企业信息,<a href=../edit/edit_company_info.php>点击这里</a></span><br>
</div>
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
</div>
</body>
</html>
