<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$user = $_SESSION[user];
	if ($_GET['project_id']){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from project_contract where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		// 如果值为空就替换为空格，让表格显示更为美观
		foreach ($value as $key=>&$eachValue) {
			if ($eachValue === NULL) {								
				$eachValue = '&nbsp;';
			}
		}
		//不同用户进入此页面的权限
		permissionBlocker('show',$value[status]);
		
		//反序列化数组存入mysql的数据
		$arrayLong_list = array_field_inDB('project_contract');
		foreach ($arrayLong_list as $arrayLong) {
			${$arrayLong} = unserialize($value[$arrayLong]);
		}
		//背景水印控制，如果项目是已审核或已立项，则背景有水印
		if ($value['project_status']=='6' || $value['project_status']=='7') {
			$check_bg = 'url(../pic/check_passed_05.jpg)'; 
		} else {
			$check_bg = 'none'; 
		}
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
	
?>
<!doctype html5>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<title>龙游县科技计划项目合同书</title>
<style type="text/css">
.check_passed {
	background-image:<?php echo $check_bg; ?>;
}
</style>
</head>
<div class="container check_passed">
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
<h1 style="font-family:黑体;font-weight:bold;font-size:26pt;line-height:40pt;text-align:center;margin:3cm 0 0 0;">龙 游 县 科 技 计 划 项 目<br>
合 同 书</h1>
<br><br><br><br><br><br><br><br>
<div style="font-family:仿宋;font-size:16pt;text-align:left;margin: 0 3cm">
<h3>计 划 编 号：
<?php
$id_length = strlen($value['project_id']);
$add_length = 3-$id_length;
$add_string = "";
for ($i=0;$i<$add_length;$i++) {
	$add_string .= '0';
}
$plan_code = date('Y').$value['industry_type'].$add_string.$value['project_id'];
echo $plan_code;
?>
</h3>
<h3>项 目 名 称：&nbsp;<?php echo replaceText($value[project_name]); ?></h3>
<h3>计 划 类 别：
<?php
    switch ($value[project_class]) {
	  case (key_industry):
	  	echo "重点工业项目";
	  	break;
	  case (key_agriculture):
	  	echo "重点农业项目";
		break;
	  case (key_society):
	  	echo "重点社会发展项目";
		break;
	  default:
	  	echo "未知项目技术来源";
  }
 ?></h3>
<h3>起 止 年 月：&nbsp;<?php echo replaceText($value[start_date]); ?>&nbsp;至&nbsp;<?php echo replaceText($value[finish_date]); ?></h3>
</div>
<br><br><br><br><br><br><br><br><br><br><br>
<h3 class="paging" style="font:宋体;font-size:18pt;line-height:22pt;">龙 游 县 科 学 技 术 局<br>
2011年制</h3>

<h2>填 写 说 明</h2>
<ol class="paging" style="text-align:left;font-family:仿宋;font-size:15pt;">
<li>本合同文本适用于县财政科技经费以“重点项目”立项资助的各类县级科技计划项目。</li>
<li>本合同所列内容应实事求是填写，表达要明确、严谨。</li>
<li>本合同与项目申请书构成一套立项的原始资料，乙方承担项目（课题）任务以合同内容为依据。</li>
<li>合同中的“经济效益目标”是指项目完成时预计能达到的目标。“项目成果”中的“论文数”和“专利数”是指项目完成时预计能发表的论文和申请或授权的专利。“引进和培养人才”是指项目执行过程中，能够引进和培养人才的数量。“经济效益目标”、“项目成果”和人才引进培养情况应与“五、主要技术、经济指标”一致，是项目验收时考核的指标。</li>
<li>合同中的“项目主要内容”，不同计划类别的项目要求不同。侧重于攻关的科技项目，“项目主要内容”应当重点突出项目研究开发内容和关键技术的研发；侧重于转化和产业化的科技项目，“项目主要内容”还应当包括转化和产业化过程中需要解决的技术问题。</li>
<li>合同中的“技术、经济指标”，不同计划类别的项目要求不同。侧重于技术攻关的计划项目，主要应当反映技术指标、获得专利和人才培养等情况；侧重于科技成果转化和产业化项目，除技术指标外，还应反映经济指标，包括技术成果应用所形成的生产规模、经济效益、形成示范基地等指标。</li>
<li>合同中的“分年度(阶段)进度安排”，要根据项目主要任务和目标合理安排。各阶段的任务目标是项目年度(中期)检查和安排项目结转经费的依据。</li>
<li>项目经费中的“自筹”指乙方自行筹措、落实，与本项目相关的研究与开发投入的资金。</li>
<li>“项目经费预算”应按〈财政部科技部关于印发《国家科技支撑计划专项经费管理》的通知〉（财教[2006]160号）规定的无偿资助性项目的经费开支范围填写。</li>
</ol>

<center>
<h2>一、项目基本情况</h2>
<table class='showTable paging' border=1 cellspacing=0 cellpadding=0 height=80%>
<tr>
<td  >项 目 名 称</td>
<td colspan=14 ><?php echo replaceText($value[project_name]);?></td>
</tr>
<tr>
<td>项目计划类别</td>
<td colspan=14>
  <?php
    switch ($value[project_class]) {
	  case (key_industry):
	  	echo "重点工业项目";
	  	break;
	  case (key_agriculture):
	  	echo "重点农业项目";
		break;
	  case (key_society):
	  	echo "重点社会发展项目";
		break;
	  default:
	  	echo "未知项目技术来源";
  }
  ?>
 </td>
</tr>
<tr >
<td  >项目行业分类</td>
<td colspan=14 ><?php echo replaceText($value[industry_type]);?></td>
</tr>
<tr >
<td  >项目学科领域</td>
<td colspan=14 ><?php echo replaceText($value[science_domain]);?></td>
</tr>
<tr >
<td  >项目管理领域</td>
<td colspan=14 ><?php echo replaceText($value[manager_domain]);?></td>
</tr>
<tr >
<td  >项目技术来源</td>
<td colspan=14>
  <?php
    switch ($value[tech_resource]) {
	  case (independent_develop):
	  	echo "自主开发";
	  	break;
	  case (cooperation_develop):
	  	echo "合作开发";
		break;
	  case (chanxueyan_cooperation):
	  	echo "产学研联合开发";
		break;
	  case (internal_cooperation):
	  	echo "引进国内技术";
		break;
	  case (foreign_technology):
	  	echo "引进国外技术";
		break;
	  default:
	  	echo "未知项目技术来源";
  }
  ?>
</td>
</tr>
<tr >
<td  >技术创新方式</td>
<td colspan=14>
  <?php
    switch ($value[innovation_format]) {
	  case (self_innovation):
	  	echo "A、自主创新";
	  	break;
	  case (integrate_innovation):
	  	echo "B、集成创新";
		break;
	  case (re_innovation):
	  	echo "C、在引进、消化吸收基础上的再创新";
		break;
	  case (tech_transform):
	  	echo "D、科技成果转化和产业化";
		break;
	  default:
	  	echo "未知项目技术来源";
  }
  ?>
</td>
</tr>
<tr >
<td  >项目开始时间</td>
<td colspan=4  ><?php echo replaceText($value[start_date]);?></td>
<td colspan=4  >项目完成时间</td>
<td colspan=6  ><?php echo replaceText($value[finish_date]);?></td>
</tr>
<tr >
<td rowspan=2>经济效益目标</td>
<td>年增产值（万元）</td>
<td colspan=3>年增利润（万元）</td>
<td colspan=2>年增税金（万元）</td>
<td colspan=5>年创汇（万美元）</td>
<td colspan=3>年节汇（万美元）</td>
</tr>
<tr >
<td ><?php echo replaceText($value['nianzengchanzhi']);?></td>
<td colspan=3 ><?php echo replaceText($value['nianzenglirun']);?></td>
<td colspan=2 ><?php echo replaceText($value[nianzengshuijin]);?></td>
<td colspan=5 ><?php echo replaceText($value[nianjiehui]);?></td>
<td colspan=3 ><?php echo replaceText($value[nianchuanghui]);?></td>
</tr>
<tr >
<td rowspan=3>项目成果</td>
<td colspan=3 rowspan=2>论文数</td>
<td colspan=6>专利</td>
<td colspan=5>其中发明专利</td>
</tr>
<tr >
<td colspan=2>申请数</td>
<td colspan=4>授权数</td>
<td colspan=3>申请数</td>
<td colspan=2>授权数</td>
</tr>
<tr >
<td colspan=3 ><?php echo replaceText($value[lunwenshu]);?></td>
<td colspan=2 ><?php echo replaceText($value[zhuanlishenqing]);?></td>
<td colspan=4 ><?php echo replaceText($value[zhuanlishouquan]);?></td>
<td colspan=3 ><?php echo replaceText($value[famingzhuanlishenqing]);?></td>
<td colspan=2 ><?php echo replaceText($value[famingzhuanlishouquan]);?></td>
</tr>
<tr >
<td  >人才引进和培养</td>
<td colspan=2>研究生培养</td>
<td ><?php echo replaceText($value[yanjiusheng]);?></td>
<td colspan=4>中级及以下</td>
<td colspan=3 ><?php echo replaceText($value[zhongjiyixia]);?></td>
<td colspan=3>副高级及以上</td>
<td ><?php echo replaceText($value[fugaoyishang]);?></td>
</tr>
<tr >
<td  >备 注</td>
<td colspan=14   ><?php echo replaceText($value[beizhu]);?></td>
</tr>
</table>

<h2>二、项目承担单位</h2>
<table class='showTable paging' border=1 cellspacing=0 cellpadding=0 height="80%">
<tr >
<td rowspan=7>第一承担单位</td>
<td colspan=2>单位名称</td>
<td colspan=6 ><?php echo replaceText($value[danweimingcheng]);?></td>
</tr>
<tr >
<td colspan=2>法人代码</td>
<td colspan=6 ><?php echo replaceText($value[farendaima]);?></td>
</tr>
<tr >
<td colspan=2  >单位类型</td>
<td colspan=6 ><?php echo replaceText($value[danweileixing]);?></td>
</tr>
<tr >
<td colspan=2  >详细地址</td>
<td colspan=2 ><?php echo replaceText($value[xiangxidizhi]);?></td>
<td colspan=2  >邮政编码</td>
<td colspan=2 ><?php echo replaceText($value[youzhengbianma]);?></td>
</tr>
<tr >
<td colspan=2>联 系 人</td>
<td colspan=2 ><?php echo replaceText($value[diyidanweilianxiren]);?></td>
<td colspan=2>E-mail</td>
<td colspan=2 ><?php echo replaceText($value[diyidanweiemail]);?></td>
</tr>
<tr >
<td colspan=2  >电话/传真</td>
<td colspan=2 ><?php echo replaceText($value[dianhuachuanzhen]);?></td>
<td colspan=2  >手 机</td>
<td colspan=2 ><?php echo replaceText($value[shouji]);?></td>
</tr>
<tr >
<td colspan=2  >开户银行</td>
<td colspan=2 ><?php echo replaceText($value[kaihuyinhang]);?></td>
<td colspan=2  >银行帐号</td>
<td colspan=2 ><?php echo replaceText($value[yinhangzhanghao]);?></td>
</tr>
<tr >
<td rowspan=5 >合作单位</td>
<td colspan=4  >单位名称</td>
<td colspan=3  >法人代码</td>
<td  >职责*</td>
</tr>
    <?php 
	$hezuo_col = $value[hezuodanweizongshu]-1;
	for ($i=0;$i<$hezuo_col;$i++){
		$ii=$i+1;
		echo "<tr>
		<td>$ii.</td>
		<td colspan=3>$array_unit_danweimingcheng[$i]</td>
		<td colspan=3>$array_unit_farendaima[$i]</td>
		<td colspan=2>$array_unit_zhize[$i]</td>
		</tr>";
	}
	?>
<td rowspan=2>合作单位
总数</td>
<td colspan=3 rowspan=2 ><?php echo replaceText($value[hezuodanweizongshu]);?></td>
<td colspan=2  >承担单位数</td>
<td colspan=3  >参加单位数</td>
</tr>
<tr >
<td colspan=2 ><?php echo replaceText($value[chengdandanweishu]);?></td>
<td colspan=3 ><?php echo replaceText($value[canjiadanweishu]);?></td>
</tr>
</table>

注：职责分为 ： 0-承担，1-参加；合作单位总数 ：包含第一承担单位


<h2>三、项目负责人及项目组成员</h2>
<table class='showTable paging' border=1 cellspacing=0 cellpadding=0 height="80%">
<tr >
<td rowspan=6>项目负责人</td>
<td colspan=2  >姓 名</td>
<td colspan=3 ><?php echo replaceText($value[fuzeren_name]);?></td>
<td colspan=2  >身份证号码</td>
<td colspan=2 ><?php echo replaceText($value[fuzeren_shenfenzheng]);?></td>
</tr>
<tr >
<td colspan=2  >工作单位</td>
<td colspan=3 ><?php echo replaceText($value[fuzeren_danwei]);?></td>
<td colspan=2  >法人代码</td>
<td colspan=2 ><?php echo replaceText($value[fuzeren_farendaima]);?></td>
</tr>
<tr >
<td colspan=2  >详细地址</td>
<td colspan=3 ><?php echo replaceText($value[fuzeren_adress]);?></td>
<td colspan=2  >邮政编码</td>
<td colspan=2 ><?php echo replaceText($value[fuzeren_zcode]);?></td>
</tr>
<tr >
<td colspan=2  >移动电话</td>
<td colspan=3 ><?php echo replaceText($value[fuzeren_mobile]);?></td>
<td colspan=2  >E-mail</td>
<td colspan=2 ><?php echo replaceText($value[fuzeren_email]);?></td>
</tr>
<tr >
<td colspan=2  >学 历</td>
<td colspan=2 ><?php echo replaceText($value[fuzeren_xueli]);?></td>
<td colspan=2>学位</td>
<td colspan=3 ><?php echo replaceText($value[fuzeren_xuewei]);?></td>
</tr>
<tr >
<td colspan=2  >职 称</td>
<td colspan=2 ><?php echo replaceText($value[fuzeren_zhicheng]);?></td>
<td colspan=2>现从事专业</td>
<td colspan=3 ><?php echo replaceText($value[fuzeren_zhuanye]);?></td>
</tr>
<tr >
<td rowspan=<?php echo replaceText($value[member_number])+1; ?>>项目组成员</td>
<td>姓 名</td>
<td colspan=2>身份证号码</td>
<td>所在单位</td>
<td>职 称</td>
<td colspan=2>从事专业</td>
<td>在本项目中的分工</td>
<td>年参加项目工作时间(月)</td>
</tr>
    <?php 
	for ($i=0;$i<$value[member_number];$i++){
		echo "<tr>
		<td>$array_member_name[$i]</td>
		<td colspan=2>$array_member_shenfenzheng[$i]</td>
		<td>$array_member_gongzuodanwei[$i]</td>
		<td>$array_member_zhicheng[$i]</td>
		<td colspan=2>$array_member_zhuanye[$i]</td>
		<td>$array_member_fengong[$i]</td>
		<td>$array_member_canjiashijian[$i]</td>
		</tr>";
	}
	?>
</table>
<br
clear=all >
<h2>四、项目主要内容（关键技术）</h2>

<table class='showTable paging' border=1 cellspacing=0  height=75%>
 <tr>
  <td valign=top  height=100 style='text-align:left'>
  <?php echo replaceText($value[xiangmuneirong]); ?>
  </td>
 </tr>
</table>

<br clear=all
>
<h2>五、项目主要技术、经济指标</h2>

<table class='showTable paging' border=1 cellspacing=0  height=75%>
 <tr>
  <td valign=top  height=100 style='text-align:left'>
  <?php echo replaceText($value[zhibiao]); ?>
  </td>
 </tr>
</table>
<h2>六、项目成果提供形式</h2>

<table class='showTable paging' border=1 cellspacing=0  height=75%>
 <tr>
  <td valign=top  height=100 style='text-align:left'>
  <?php echo replaceText($value[chengguoxingshi]); ?>
  </td>
 </tr>
</table>

<h2>七、项目分年度（阶段）进度安排</h2>
<table class='showTable' border=1 cellspacing=0 cellpadding=0>
<tr >
<td>起止年月</td>
<td>计划进度</td>
<td>投入经费（万元）</td>
</tr>
 <?php
	$n=count($array_progress_start);
	for($i=0;$i<$n;$i++){
		echo "<tr id='tr_$i'><td width=200>
			$array_progress_start[$i] 至 $array_progress_end[$i]</td>
			<td>$array_progress_content[$i]</td>
			<td>$array_progress_fund[$i]</td>
			</tr>";
	}
 ?>
</table>

<h2>八、项目经费来源</h2>
<ol style="text-align:left">
<li>本项目研发总经费<u><?php echo replaceText($value[zongjingfei]);?></u>万元，其中:甲方补助<u><?php echo replaceText($value[jiafangbuzhu]);?></u>万元，乙方自筹<u><?php echo replaceText($value[yifangzichou]);?></u>万元。</li>
<li>甲方经费拨付计划：
<table class='showTable' border=1 cellspacing=0 cellpadding=0 style="">
<tr >
<td>拨款时间</td>
<td>合 计</td>
<?php 
$n = 4;
for( $i=0;$i<$n; $i++) {
	echo "<td>$array_bokuan_nian[$i]年$array_bokuan_yue[$i]月</td>";
}
?>
</tr>
<tr >
<td>金额(万元)</td>
<?php 
$n = 5;
for( $i=0;$i<$n; $i++) {
	echo "<td>$array_bokuan_qian[$i]</td>";
}
?>
</tr>
</table>
<br>
甲方首期拨款后，其余经费根据项目执行情况，分期拨给乙方。</li>
<li>乙方自筹经费到位计划：
<table class='showTable paging' border=1 cellspacing=0 cellpadding=0 style="">
<tr >
<td ><?php echo replaceText($value[placeholder]);?></td>
<td>总金额（万元）</td>
<?php 
$n = 3;
for( $i=0;$i<$n; $i++) {
	echo "<td>$array_zichou_nian[$i]年$array_zichou_yue[$i]月</td>";
}
?>
</tr>
<tr >
<td>乙方自筹</td>
<?php 
$n = 4;
for( $i=0;$i<$n; $i++) {
	echo "<td>$array_zichou_qian[$i]</td>";
}
?>
</tr>
</table></li>
</ol>

<h2>九、项目经费支出预算</h2>
<table class='showTable' border=1 cellspacing=0 cellpadding=0
>
<tr >
<td>经费开支科目</td>
<td>预算经费总额(万元)</td>
<td>其中县财政经费（万元）</td>
</tr>
<tr >
<td> 1.设备费</td>
<td><?php echo $array_jingfei_yusuan[0];?></td>
<td><?php echo $array_jingfei_caizheng[0];?></td>
</tr>
<tr>
<td> 2.材料费</td>
<td><?php echo $array_jingfei_yusuan[1];?></td>
<td><?php echo $array_jingfei_caizheng[1];?></td>
</tr>
<tr>
<td> 3.测试化验加工费</td>
<td><?php echo $array_jingfei_yusuan[2];?></td>
<td><?php echo $array_jingfei_caizheng[2];?></td>
</tr>
<tr>
<td> 4.燃料动力费</td>
<td><?php echo $array_jingfei_yusuan[3];?></td>
<td><?php echo $array_jingfei_caizheng[3];?></td>
</tr>
<tr>
<td> 5.差旅费</td>
<td><?php echo $array_jingfei_yusuan[4];?></td>
<td><?php echo $array_jingfei_caizheng[4];?></td>
</tr>
<tr>
<td> 6.会议费</td>
<td><?php echo $array_jingfei_yusuan[5];?></td>
<td><?php echo $array_jingfei_caizheng[5];?></td>
</tr>
<tr>
<td> 7.合作、协作研究与交流费</td>
<td><?php echo $array_jingfei_yusuan[6];?></td>
<td><?php echo $array_jingfei_caizheng[6];?></td>
</tr>
<tr>
<td> 8.出版/文献/信息传播/知识产权事务费</td>
<td><?php echo $array_jingfei_yusuan[7];?></td>
<td><?php echo $array_jingfei_caizheng[7];?></td>
</tr>
<tr>
<td> 9.人员劳务费</td>
<td><?php echo $array_jingfei_yusuan[8];?></td>
<td><?php echo $array_jingfei_caizheng[8];?></td>
</tr>
<tr>
<td> 10.专家咨询费</td>
<td><?php echo $array_jingfei_yusuan[9];?></td>
<td><?php echo $array_jingfei_caizheng[9];?></td>
</tr>
<tr>
<td> 11.管理费</td>
<td><?php echo $array_jingfei_yusuan[10];?></td>
<td><?php echo $array_jingfei_caizheng[10];?></td>
</tr>
<tr>
<td> 12．其他费用</td>
<td><?php echo $array_jingfei_yusuan[11];?></td>
<td><?php echo $array_jingfei_caizheng[11];?></td>
</tr>
</table>

<h2>十、需增添的仪器设备</h2>
<table class='showTable paging' border=1 cellspacing=0 cellpadding=0
>
<tr >
<td>名称</td>
<td>数 量</td>
<td>单 价</td>
<td>县财政拨款</td>
<td>自筹或其他</td>
<td>用途说明</td>
</tr>
<?php
$n=6;
for ($i=0;$i<$n;$i++) {
	echo "
	<tr>
	<td>$array_yiqi_name[$i]</td>
	<td>$array_yiqi_num[$i]</td>
	<td>$array_yiqi_price[$i]</td>
	<td>$array_yiqi_bokuan[$i]</td>
	<td>$array_yiqi_zichou[$i]</td>
	<td>$array_yiqi_yongtu[$i]</td>
	</tr>
	";
}
?>
</table>

<br clear=all>
<h2>十一、合同其他条款</h2>
<ol class="paging" style="text-align:left;font-family:仿宋;font-size:15pt;">
<li>各方应严格遵守本合同的各项条款。因合同执行过程中出现的客观原因，任何一方认为有必要变更合同条款内容的，需经协商一致。</li>
<li>甲方有权按照合同的要求，监督、检查乙方项目进展和经费使用情况，乙方应予以配合。乙方应按要求向甲方报送项目执行和经费使用情况。</li>
<li>乙方有权按照合同的要求组织实施项目、使用项目经费。乙方未按本合同落实自筹经费，或未按规定使用项目经费的，甲方有权暂停拨款直至解除合同，并收回已投入的经费。</li>
<li>甲方由多家单位组成的(如联合招标项目)，各方的出资数额、方式、时间以及其它相关权利和义务需单独订立协议，作为本合同的附件，视作本合同的组成部分。</li>
<li>乙方与合作单位的合作协议（包括各方承担的任务、经费分配、研究成果的归属等），作为本合同的附件，视作本合同的组成部分。</li>
<li>根据《龙游县科技计划项目申报管理办法（试行）》（龙政办发〔2010〕62号）的规定，乙方完成本项目任务后，应及时申请验收或结题。</li>
<li>乙方在合同执行过程中，确因技术、市场等不可抗拒原因，必须调整合同相关任务或指标的，应及时书面提出申请，甲方审批同意后，按批准后合同任务和指标执行。未经批准的，按原合同任务和指标要求完成。</li>
<li>成果的权属和保密。本项目研究取得的技术成果，其知识产权归属及成果转化，按国家和本省的有关规定执行。项目技术成果涉及国家利益的，乙方在有偿转让之前，应经过甲方的审查批准；涉及国家机密的，按国家《中华人民共和国保守国家秘密法》和《科学技术保密规定》有关规定执行。</li>
<li>本合同文本一式六份，分存甲方、乙方及有关单位。</li>
</ol>

<div style="line-height:30pt">
<div style="text-align:left;margin-top:5cm">
甲方(项目委托单位)：(盖章)<br>
单位负责人（签字）：<br>
<br>
</div>
<div style="text-align:right">
年 月  日
</div>
<br>
<br>
<br>
<div style="text-align:left">
乙方 (项目承担单位)：(盖章)<br>
项目（课题）负责人（签字）：<br>
单位负责人（签字）：<br>
财务负责人（签字）：<br>
开户银行及帐号：<br>
<br>
</div>
<div style="text-align:right">
年 月  日
</div>
</center>
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
</div>

</html>
