<?php
	session_start();
	include ("../../conf.php");
	$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
	$id = $_SESSION[id];
	$user = $_SESSION[user];
	if ($_GET['project_id']){
		//查找项目id，如果不属于当前用户，而且当前用户也不是管理员，则访问被拒绝
		$project_id = $_GET['project_id'];
		$query = "select * from project_conclusion where project_id='$project_id'";
		$result = mysql_query($query);
		$value = mysql_fetch_array($result);
		// 如果值为空就替换为空格，让表格显示更为美观
		foreach ($value as $key=>&$eachValue) {
			if ($eachValue === NULL) {								
				$eachValue = '&nbsp;';
			}
		}
		//不同用户进入此页面的权限
		permissionBlocker('show',$value[status]);
		
		//背景水印控制，如果项目是已审核或已立项，则背景有水印
		if ($value['project_status']=='6') {
			$check_bg = 'url(../pic/check_passed_05.jpg)'; 
		} else {
			$check_bg = 'none'; 
		}
		
	} else {
		echo "<script>alert('未指定项目')</script>";
		exit();
	}
?>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="../css/screenstyle.css" media="screen" />
<link type="text/css" rel="stylesheet" href="../css/printstyle.css" media="print" />
<style type="text/css">
.check_passed {
	background-image:<?php echo $check_bg; ?>;
}
@media screen {
	table.wideTable th,table.wideTable td { 
		text-align:center;
		font-size:13pt;
		padding: 0px; 
	} 

}
</style>
</head>
<div class="check_passed container">
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
<center>
<h2>龙游县科技计划项目结题书</h2>
<table height=80%  class='showTable wideTable paging' border=1 cellspacing=0 >
 <tr>
  <th  colspan=2>
  项目名称
  </th>
  <td  colspan=11>
  <?php echo replaceText($value[project_name]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th  colspan=2>
  项目承担<br />
  单　　位
  </th>
  <td  colspan=11>
  <?php echo replaceText($value[assume_unit]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th  colspan=2>
  主要完成<br />
  人　　员
  </th>
  <td  colspan=11>
  <?php echo replaceText($value[primary_member]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th  colspan=2>
  项目协作<br />
  单　　位
  </th>
  <td  colspan=11>
  <?php echo replaceText($value[cooperating_unit]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th  colspan=2>
  计划编号
  </th>
  <td  colspan=5>
  <?php echo replaceText($value[plan_code]); ?>&nbsp;
  </td>
  <th  colspan=2>
  项目起止年月&nbsp;&nbsp; 
  </th>
  <td  colspan=4>
  <?php echo replaceText($value[start_finish]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th rowspan=2>
  投入经费及来源（万元）
  </th>
  <th colspan=3>
  县财政拨款
  </th>
  <th>
  贷款
  </th>
  <th>
  自筹
  </th>
  <th rowspan=2>
  经费
  支出
  (万元)
  </th>
  <th>
  调研咨询　费
  </th>
  <th>
  材料　、　仪器费
  </th>
  <th>
  设计试验　费
  </th>
  <th>
  管理费
  </th>
  <th colspan=2>
  其 它<br />
  费 用
  </th>
 </tr>
 <tr>
  <td  colspan=3>
  <?php echo replaceText($value[fiscal_final]); ?>&nbsp;
  </td>
  <td >
  <?php echo replaceText($value[loan_final]); ?>&nbsp;
  </td>
  <td >
  <?php echo replaceText($value[self_final]); ?>&nbsp;
  </td>
  <td >
  <?php echo replaceText($value[consult_cost]); ?>&nbsp;
  </td>
  <td >
  <?php echo replaceText($value[instrument_cost]); ?>&nbsp;
  </td>
  <td >
  <?php echo replaceText($value[experiment_cost]); ?>&nbsp;
  </td>
  <td >
  <?php echo replaceText($value[management_cost]); ?>&nbsp;
  </td>
  <td  colspan=2>
  <?php echo replaceText($value[other_cost]); ?>&nbsp;
  </td>
 </tr>
 </table>
 
 <table height=80%  class='showTable paging' border=1 cellspacing=0 >
 <tr>
  <td  colspan=13 valign='top' height="300">
  一、项目简介（包括项目研究开发的主要内容，与国内外同类技术的比较等）：<br />
  <p><?php echo replaceText($value[project_brief]); ?></p>
  </td>
 </tr>
</table>
 
 <table height=80%  class='showTable paging' border=1 cellspacing=0 >
 <tr>
  <td  colspan=13 valign='top' height="300">
  二、项目合同规定的主要内容、技术经济指标及完成情况：<br />
  <p><?php echo replaceText($value[exceptation_completion]); ?></p>
  </td>
 </tr>
 </table>
 
 <table height=80%  class='showTable paging' border=1 cellspacing=0 >
 <tr>
  <td  colspan=13 valign='top' height="300">
  三、关键技术及创新点、获自主知识产权情况、成果应用和产业化情况：<br />
  <p><?php echo replaceText($value[inovation_point]); ?></p>
  </td>
 </tr>
 <tr>
  <th  colspan=3>
  提供的技术<br />
  资料目录
  </th>
  <td  colspan=10>
  <?php echo replaceText($value[material_catalog]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th  colspan=3>
  项目承担单位<br />
  对项目成果的自我评价
  </th>
  <td  colspan=10>
  <?php echo replaceText($value[self_evaluation]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th  colspan=3>
  县科技局、财政局结题意见
  </th>
  <td  colspan=10>
  <?php echo replaceText($value[goverment_opinion]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th  colspan=3 valign='top' height="50">
  备&nbsp;&nbsp;&nbsp; 注
  </th>
  <td  colspan=10>
  <?php echo replaceText($value[comment]); ?>&nbsp;
  </td>
 </tr>
</table>

<h2>龙游县科技计划项目实施效果统计表</h2>
<br>
单位（盖章）：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 单位：万元
<table height=80%  class='showTable' border=1 cellspacing=0 cellpadding=8>
 <tr>
  <th >
  项目编号
  </th>
  <td  colspan=3>
  <?php echo replaceText($value[project_code]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th >
  项目名称
  </th>
  <td  colspan=3>
  <?php echo replaceText($value[project_name]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th >
  项目总投资
  </th>
  <td  colspan=3>
  <?php echo replaceText($value[investment_total]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th >
  研发经费投入
  </th>
  <td  colspan=3>
  <?php echo replaceText($value[reserch_expense]); ?>&nbsp;
  </td>
 </tr>
 <tr>
  <th >
  县财政经费
  </th>
  <td  colspan=3>
  <?php echo replaceText($value[fiscal_expenditure]); ?>&nbsp;
  </td>
 </tr>
 	<tr>
    	<th>&nbsp;</th>	
		<th>销售收入</th>	
		<th>税　　收</th>
		<th>利　　润</th>
    </tr>
<?php 
	for($i=0;$i<3;$i++){
		$ii=2010+$i;
		echo "
	<tr>
	<th>$ii 年</th>
	<td>$array_income[$i]</td>	
	<td>$array_tax[$i]</td>
	<td>$array_profit[$i]</td>
	</tr>";
	}
?>
  <th >
  备　　注
  </th>
  <td  colspan=3 valign=top height="300">
  项目获得专利、成果、社会效益说明：<br>
  <p><?php echo replaceText($value[comment_result]); ?></p>
  </td>
 </tr>
</table>
</center>
<div id='edit_button'>
<?php
if ($value[user] == $_SESSION[user]){
	echo "<span class='print' style='font-size:9px'>如果您需要修改信息,<a href=../edit/edit_project_conclusion.php?project_id=$project_id>点击这里</a></span><br>";
}
?>
</div>
<div id='print_button'><input class='print' type='button' value=" 打 印 " onClick="window.print()"></div>
</div>
</html>