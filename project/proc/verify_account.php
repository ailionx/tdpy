<?php
session_start();
include ("../../conf.php");
$ident = identify_user($_SESSION[id],$_SESSION[cookshell]);
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<style type="text/css">

body {
	margin:50px auto;
}

table {
	border-collapse:collapse;
	font-size:14px;
}

td,th {
	text-align:center;
	padding:8px 15px;
	margin:0;
}
</style>
<title>审核账号</title>
</head>

<body>
<?php
class showAccount {
	public $pems;
	public $company_name;
	public $show_array;
	public $query;

	function __construct ($pems,$company_name) {
		$this->pems = $pems;
		$this->company_name = $company_name; 
		$this->show_array = array('person_name'=>'申请人姓名','user'=>'用户名','id_card'=>'身份证号','person_phone'=>'申请人电话','personal_email'=>'申请人email','register_date'=>'申请时间');
		$this->query = "select * from account where company_name='$this->company_name' and pems='$this->pems' order by register_date asc";
	}
	
	function showAccountToVerify(){	
		$result = mysql_query($this->query);
		echo "<center>";
		if (mysql_num_rows($result)==0) {
			echo "当前没有需要审核的账号";
		} else {
	//		echo $this->query;
	//		print_r($row = mysql_fetch_array($result));
				echo "<table border=1>";
				echo "<thead>";
				foreach ($this->show_array as $show_key=>$show_item) {
					echo "<th>$show_item</th>";
				}
				echo "<th colspan=2>操作</th>";
				echo "</thead>";
				while ($row = mysql_fetch_array($result)) {
					echo "<tr>";
					foreach ($this->show_array as $show_key=>$show_item) {
						echo "<td>$row[$show_key]</td>";
					}
					echo "<td><a href='./change_account_status.php?action=pass&user=$row[user]&pems=$this->pems' onclick=\"return confirm('确定审核通过吗？')\">审核通过</a></td>";
					echo "<td><a href='./change_account_status.php?action=delete&user=$row[user]&pems=$this->pems' onclick=\"return confirm('确定删除这个账号的所有信息吗？')\">删除</a></td>";
					echo "</tr>";
				}
				echo "</table></center>";
		}
	}
}

if ($_GET['type']) {
	$type = $_GET['type'];
	switch ($type) {
		case ('person'):
			if($_SESSION[pems]=='2') {
				$company_name = $ident['company_name'];
				$verify_person_account = new showAccount('30',$company_name);
				$verify_person_account->showAccountToVerify();
			} else {
				echo "<script>alert('非法操作！')</script>";
				exit();
			}
			break;
		case ('company'):
			if($_SESSION[pems]=='1') {
				$verify_company_account = new showAccount('20','');
				$verify_company_account->query = "select * from account where pems='20' order by register_date asc";
				$verify_company_account->show_array = array('company_name'=>'申请单位','user'=>'用户名','company_phone'=>'企业电话','company_email'=>'企业email','register_date'=>'申请时间');
				$verify_company_account->showAccountToVerify();
			} else {
				echo "<script>alert('非法操作！')</script>";
				exit();
			}
			break;
		default:
			echo "<script>alert('未知的操作项目！')</script>";
			exit();			
	}
} else {
	echo "<script>alert('未知的执行项目')</script>";
	exit();
}
?>

</body>
</html>