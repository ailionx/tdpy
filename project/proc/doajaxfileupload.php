<?php
	$error = "";
	$msg = "";
	$fileElementName = 'fileToUpload';
	if(!empty($_FILES[$fileElementName]['error']))
	{
		switch($_FILES[$fileElementName]['error'])
		{

			case '1':
				$error = '文件大小超过php.ini定义的限制';
				break;
			case '2':
				$error = '文件大小超过HTML定义的限制';
				break;
			case '3':
				$error = '文件只有部分被上传';
				break;
			case '4':
				$error = '没有文件上传';
				break;

			case '6':
				$error = '缺少临时目录';
				break;
			case '7':
				$error = '写入文件到磁盘出错';
				break;
			case '8':
				$error = '文件上传意外停止';
				break;
			case '999':
			default:
				$error = '未知的错误信息！';
		}
	}elseif(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none')
	{
		$error = '无上传文件！';
	}else 
	{
		
		//	$msg .= " 文件名: " . $_FILES['fileToUpload']['name'] . ", ";
		//	$msg .= " 文件大小: " . @filesize($_FILES['fileToUpload']['tmp_name']);

		$uploaddir = "../files/";//设置文件保存目录 注意包含/       
	   //判断文件类型
		if($_FILES['fileToUpload']['type']!=='application/msword') {  
			$error = "您只能上传word文件: ";

		} else {   //生成目标文件的文件名
			$filename=explode(".",$_FILES['fileToUpload']['name']);     
			$filename[0]= $_GET['project_name']."__可行性报告"; // 设置带项目名称的文件名
			$filename[0]=iconv("utf-8","gb2312",$filename[0]); 
			$name=implode(".",$filename);   
			$uploadfile=$uploaddir.$name;  
			if(is_uploaded_file($_FILES['fileToUpload']['tmp_name'])){
				if(move_uploaded_file($_FILES['fileToUpload']['tmp_name'],$uploadfile)) {
					$msg .= "您的文件已经上传完毕";
				} else {
					$msg .= " 上传失败！";
				}	
			} else {
				$msg .= " 文件错误！";
			}
		}   	
	}		
	echo "{";
	echo				"error: '" . $error . "',\n";
	echo				"msg: '" . $msg . "'\n";
	echo "}";
?>