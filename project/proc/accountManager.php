<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of passwordManager
 *
 * @author slin
 */
class accountManager {
    //put your code here
    public static function get_accounts(){
        $sql = 'SELECT * FROM account WHERE 1';
        $query = mysql_query($sql);
        
        $accounts = array();
        while ($row = mysql_fetch_object($query)) {
            $accounts[] = $row;
        }
        
        return $accounts;
        
    }
    
    /**
     * Function is to show account manager table
     * @return string
     */
    public function showAccounts(){
        $accounts = self::get_accounts();
        
        $html = '<table id="manage-account">'
                . '<thead>'
                . '<tr>'
                . '<th>公司名</th>'
                . '<th>账户名</th>'
                . '<th>修改密码</th>'
                . '</tr>'
                . '</thead>'
                . '</tbody>';
        foreach($accounts as $account) {
            $html .= "<tr>"
                    . "<td class='company-name'>$account->company_name</td>"
                    . "<td class='user-name'>$account->user</td>"
                    . "<td><a href='' account-id='$account->id' class='manage-change-password'>修改密码</a></td>"
                    . "</tr>";
        }
        
        $html .= '</tbody>'
                . '</table>';
        
        return $html;
    }
    
}
